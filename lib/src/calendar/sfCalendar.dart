part of calendar;

/// Signature for callback that reports that a current view or current visible
/// dates changes.
///
/// The visible dates collection visible on view when the view changes available
/// in the [ViewChangedDetails].
///
/// Used by [SfCalendar.onViewChanged].
typedef ViewChangedCallback = void Function(
    ViewChangedDetails viewChangedDetails);

/// Signature for callback that reports that a calendar element tapped on view.
///
/// The tapped date, appointments, and element details when the tap action
///  performed on element available in the [CalendarTapDetails].
///
/// Used by[SfCalendar.onTap].
typedef CalendarTapCallback = void Function(
    CalendarTapDetails calendarTapDetails);

/// Signature for callback that reports that a calendar element long pressed
/// on view.
///
/// The tapped date, appointments, and element details when the  long press
///  action performed on element available in the [CalendarLongPressDetails].
///
/// Used by[SfCalendar.onLongPress].
typedef CalendarLongPressCallback = void Function(
    CalendarLongPressDetails calendarLongPressDetails);

typedef _UpdateCalendarState = void Function(
    _UpdateCalendarStateDetails _updateCalendarStateDetails);

/// A material design calendar.
///
/// Used to scheduling and managing events.
///
/// The [SfCalendar] has built-in configurable views that provide basic
/// functionalities for scheduling and representing [Appointment]'s or events
/// efficiently. It supports [minDate] and [maxDate] to restrict the date selection.
///
/// By default it displays [CalendarView.day] view with current date visible.
///
/// To navigate to different views set [view] property with a desired [CalendarView].
///
/// Available view types is followed by:
/// * [CalendarView.day]
/// * [CalendarView.week]
/// * [CalendarView.workWeek]
/// * [CalendarView.month]
/// * [CalendarView.timelineDay]
/// * [CalendarView.timelineWeek]
/// * [CalendarView.timelineWorkWeek]
/// * [CalendarView.schedule]
///
/// ![different views in calendar](https://help.syncfusion.com/flutter/calendar/images/overview/multiple_calenda_views.png)
///
/// To restrict the date navigation and selection interaction use [minDate],
/// [maxDate], the dates beyond this will be restricted.
///
/// Set the [Appointment]'s or custom events collection to [dataSource] property
/// by using the [CalendarDataSource].
///
/// When the visible view changes the widget calls the [onViewChanged] callback
/// with the current view visible dates.
///
/// When an any of [CalendarElement] tapped the widget calls the [onTap] callback
/// with selected date, appointments and selected calendar element details.
///
/// _Note:_ The calendar widget allows to customize its appearance using [SfCalendarThemeData]
/// available from [SfCalendarTheme] widget or the [SfTheme.calendarTheme] widget.
/// It can also be customized using the properties available in
/// [CalendarHeaderStyle][ViewHeaderStyle][MonthViewSettings][TimeSlotViewSettings]
/// [MonthCellStyle], [AgendaStyle].
///
/// See also:
/// [SfCalendarThemeData]
/// [CalendarHeaderStyle]
/// [ViewHeaderStyle]
/// [MonthViewSettings]
/// [TimeSlotViewSettings]
/// [MonthCellStyle]
/// [AgendaStyle].
///
///
/// ```dart
///Widget build(BuildContext context) {
///   return Container(
///      child: SfCalendar(
///        view: CalendarView.day,
///        dataSource: _getCalendarDataSource(),
///      ),
///    );
///  }
///
/// class DataSource extends CalendarDataSource {
///  DataSource(List<Appointment> source) {
///    appointments = source;
///  }
/// }
///
///  DataSource _getCalendarDataSource() {
///    List<Appointment> appointments = <Appointment>[];
///    appointments.add(
///        Appointment(
///          startTime: DateTime.now(),
///          endTime: DateTime.now().add(
///              Duration(hours: 2)),
///          isAllDay: true,
///          subject: 'Meeting',
///          color: Colors.blue,
///          startTimeZone: '',
///          endTimeZone: '',
///        ));
///
///    return DataSource(appointments);
///  }
///  ```
class SfCalendar extends StatefulWidget {
  /// Creates a [SfCalendar] widget, which used to scheduling and managing
  /// events.
  ///
  /// By default it displays [CalendarView.day] view with current date visible.
  ///
  /// To navigate to different views set [view] property with a desired
  /// [CalendarView].
  ///
  /// Use [DataSource] property to set the appointments to the scheduler.
  SfCalendar({
    Key key,
    CalendarView view,
    this.firstDayOfWeek = 7,
    this.headerHeight = 40,
    this.viewHeaderHeight = -1,
    this.todayHighlightColor,
    this.cellBorderColor,
    this.backgroundColor,
    this.dataSource,
    this.timeZone,
    this.selectionDecoration,
    this.onViewChanged,
    this.onTap,
    this.onLongPress,
    this.controller,
    this.appointmentTimeTextFormat,
    CalendarHeaderStyle headerStyle,
    ViewHeaderStyle viewHeaderStyle,
    TimeSlotViewSettings timeSlotViewSettings,
    MonthViewSettings monthViewSettings,
    DateTime initialDisplayDate,
    DateTime initialSelectedDate,
    ScheduleViewSettings scheduleViewSettings,
    DateTime minDate,
    DateTime maxDate,
    TextStyle appointmentTextStyle,
    bool showNavigationArrow,
    this.specialRegions,
  })  : view = view ?? CalendarView.day,
        appointmentTextStyle = appointmentTextStyle ??
            const TextStyle(
                color: Colors.white,
                fontSize: 10,
                fontWeight: FontWeight.w500,
                fontFamily: 'Roboto'),
        headerStyle = headerStyle ?? CalendarHeaderStyle(),
        viewHeaderStyle = viewHeaderStyle ?? ViewHeaderStyle(),
        timeSlotViewSettings = timeSlotViewSettings ?? TimeSlotViewSettings(),
        scheduleViewSettings =
            scheduleViewSettings ?? const ScheduleViewSettings(),
        monthViewSettings = monthViewSettings ?? MonthViewSettings(),
        initialSelectedDate =
            controller != null && controller.selectedDate != null
                ? controller.selectedDate
                : initialSelectedDate,
        initialDisplayDate =
            controller != null && controller.displayDate != null
                ? controller.displayDate
                : initialDisplayDate ??
                    DateTime(DateTime.now().year, DateTime.now().month,
                        DateTime.now().day, 08, 45, 0),
        minDate = minDate ?? DateTime(0001, 01, 01),
        maxDate = maxDate ?? DateTime(9999, 12, 31),
        showNavigationArrow = showNavigationArrow ?? false,
        super(key: key);

  /// Defines the view for the [SfCalendar].
  ///
  /// Defaults to `CalendarView.day`.
  ///
  /// Also refer: [CalendarView].
  ///
  /// ``` dart
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.day,
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final CalendarView view;

  /// The minimum date as much as the [SfCalendar] will navigate.
  ///
  /// The [SfCalendar] widget will navigate as minimum as to the given date,
  /// and the dates before that date will be disabled for interaction and navigation
  /// to those dates were restricted.
  ///
  /// Defaults to `1st  January of 0001`.
  ///
  /// _Note:_ If the [initialDisplayDate] property set with the date prior to this
  /// date, the [SfCalendar] will take this date as a display date and render
  /// dates based on the date set to this property.
  ///
  /// See also:
  /// [initialDisplayDate].
  /// [maxDate].
  ///
  /// ``` dart
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        minDate: new DateTime(2019, 12, 14, 9, 0, 0),
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final DateTime minDate;

  /// The maximum date as much as the [SfCalendar]  will navigate.
  ///
  /// The [SfCalendar] widget will navigate as maximum as to the given date,
  /// and the dates after that date will be disabled for interaction and navigation
  /// to those dates were restricted.
  ///
  /// Defaults to `31st December of 9999`.
  ///
  /// _Note:_ If the [initialDisplayDate] property set with the date after to this
  /// date, the [SfCalendar] will take this date as a display date and render
  /// dates based on the date set to this property.
  ///
  /// See also:
  /// [initialDisplayDate].
  /// [minDate].
  ///
  /// ``` dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        maxDate: new DateTime(2020, 01, 15, 9, 0, 0),
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final DateTime maxDate;

  /// The first day of the week in the [SfCalendar].
  ///
  /// Allows to change the first day of week in all possible views in calendar,
  /// every view's week will start from the date set to this property.
  ///
  /// Defaults to `7` which indicates `DateTime.sunday`.
  ///
  /// ``` dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        firstDayOfWeek: 3,
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final int firstDayOfWeek;

  /// Defines the time format for appointment view text in [SfCalendar]
  /// month agenda view and schedule view.
  ///
  /// The specified time format applicable when [view] of [SfCalendar] is
  /// [CalendarView.schedule] or [CalendarView.month].
  ///
  /// The time formats specified in the below link are supported
  /// Ref: https://api.flutter.dev/flutter/intl/DateFormat-class.html
  ///
  /// Defaults to null.
  ///
  /// ``` dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.month,
  ///        monthViewSettings: MonthViewSettings(
  ///            showAgenda: true,
  ///            navigationDirection: MonthNavigationDirection.horizontal),
  ///        appointmentTimeTextFormat: 'hh:mm a',
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final String appointmentTimeTextFormat;

  /// The color which fills the border of every calendar cells in [SfCalendar].
  ///
  /// Defaults to null.
  ///
  /// Using a [SfCalendarTheme] gives more fine-grained control over the
  /// appearance of various components of the calendar.
  ///
  /// ``` dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.day,
  ///        cellBorderColor: Colors.grey,
  ///      ),
  ///    );
  ///  }
  ///
  ///```
  final Color cellBorderColor;

  /// The settings have properties which allow to customize the schedule view of
  /// the [SfCalendar].
  ///
  /// Allows to customize the [ScheduleViewSettings.monthHeaderSettings],
  /// [ScheduleViewSettings.weekHeaderSettings], [ScheduleViewSettings.dayHeaderSettings],
  /// [ScheduleViewSettings.appointmentTextStyle], [ScheduleViewSettings.appointmentItemHeight]
  /// and [ScheduleViewSettings.hideEmptyScheduleWeek] in schedule view of calendar.
  ///
  /// ``` dart
  ///
  /// @override
  ///  Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.schedule,
  ///        scheduleViewSettings: ScheduleViewSettings(
  ///            appointmentItemHeight: 60,
  ///            weekLabelSettings: WeekLabelSettings(
  ///              height: 40,
  ///              textAlign: TextAlign.center,
  ///            )),
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final ScheduleViewSettings scheduleViewSettings;

  /// Sets the style for customizing the [SfCalendar] header view.
  ///
  /// Allows to customize the [CalendarHeaderStyle.textStyle], [CalendarHeaderStyle.textAlign]
  /// and [CalendarHeaderStyle.backgroundColor] in header view of calendar.
  ///
  /// ![header with different style in calendar](https://help.syncfusion.com/flutter/calendar/images/headers/header-style.png)
  ///
  /// See also: [CalendarHeaderStyle].
  ///
  /// ```dart
  ///Widget build(BuildContext context) {
  ///  return Container(
  ///  child: SfCalendar(
  ///      view: CalendarView.week,
  ///      headerStyle: CalendarHeaderStyle(
  ///          textStyle: TextStyle(color: Colors.red, fontSize: 20),
  ///          textAlign: TextAlign.center,
  ///          backgroundColor: Colors.blue),
  ///    ),
  ///  );
  ///}
  /// ```
  final CalendarHeaderStyle headerStyle;

  /// Sets the style to customize [SfCalendar] view header.
  ///
  /// Allows to customize the [ViewHeaderStyle.backgroundColor],
  /// [ViewHeaderStyle.dayTextStyle] and [ViewHeaderStyle.dateTextStyle] in view
  /// header of calendar.
  ///
  /// ![view header with different style in calendar](https://help.syncfusion.com/flutter/calendar/images/headers/viewheader-style.png)
  ///
  /// See also: [ViewHeaderStyle].
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        viewHeaderStyle: ViewHeaderStyle(
  ///            backgroundColor: Colors.blue,
  ///            dayTextStyle: TextStyle(color: Colors.grey, fontSize: 20),
  ///            dateTextStyle: TextStyle(color: Colors.grey, fontSize: 25)),
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final ViewHeaderStyle viewHeaderStyle;

  /// The height for header view to layout within this in calendar.
  ///
  /// Defaults to `40`.
  ///
  /// ![header height as 100 in calendar](https://help.syncfusion.com/flutter/calendar/images/headers/header-height.png)
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        headerHeight: 100,
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final double headerHeight;

  /// The text style for the text in the [Appointment] view in [SfCalendar].
  ///
  /// Defaults to null.
  ///
  /// Using a [SfCalendarTheme] gives more fine-grained control over the
  /// appearance of various components of the calendar.
  ///
  /// _Note:_ This style doesn't apply for the appointment's in the agenda view
  /// of month view, for agenda view appointments styling can be achieved by
  /// using the [MonthViewSettings.agendaStyle.appointmentTextStyle].
  ///
  /// See also: [AgendaStyle].
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.workWeek,
  ///        appointmentTextStyle: TextStyle(
  ///                fontSize: 12,
  ///                fontWeight: FontWeight.w500,
  ///                color: Colors.blue,
  ///                fontStyle: FontStyle.italic)
  ///      ),
  ///    );
  ///  }
  ///
  ///  ```
  final TextStyle appointmentTextStyle;

  /// The height of the view header to the layout within this in [SfCalendar].
  ///
  /// Defaults to `-1`.
  ///
  /// ![view header height as 100 in calendar](https://help.syncfusion.com/flutter/calendar/images/headers/viewheader-height.png)
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        viewHeaderHeight: 100,
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final double viewHeaderHeight;

  /// Color that highlights the today cell in [SfCalendar].
  ///
  /// Allows to change the color that highlights the today cell in month view,
  /// and view header of day/week/workweek, timeline view and highlights the date
  /// in month agenda view in [SfCalendar].
  ///
  /// Defaults to null.
  ///
  /// Using a [SfCalendarTheme] gives more fine-grained control over the
  /// appearance of various components of the calendar.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        todayHighlightColor: Colors.red,
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final Color todayHighlightColor;

  /// The background color to fill the background of the [SfCalendar].
  ///
  /// Defaults to null.
  ///
  /// Using a [SfCalendarTheme] gives more fine-grained control over the
  /// appearance of various components of the calendar.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        backgroundColor: Colors.transparent,
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final Color backgroundColor;

  /// Displays the navigation arrows on the header view of [SfCalendar].
  ///
  /// If this property set as [true] the header view of [SfCalendar] will
  /// display the navigation arrows which used to navigate to the previous/next
  /// views through the navigation icon buttons.
  ///
  /// defaults to `false`.
  ///
  /// _Note:_ Header view does not show arrow when [view] as [CalendarView.schedule]
  ///
  /// ``` dart
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.day,
  ///        showNavigationArrow: true,
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final bool showNavigationArrow;

  /// The settings have properties which allow to customize the time slot views
  /// of the [SfCalendar].
  ///
  /// Allows to customize the [TimeSlotViewSettings.startHour],
  /// [TimeSlotViewSettings.endHour], [TimeSlotViewSettings.nonWorkingDays],
  /// [TimeSlotViewSettings.timeInterval], [TimeSlotViewSettings.timeIntervalHeight],
  /// [TimeSlotViewSettings.timeFormat], [TimeSlotViewSettings.dateFormat],
  /// [TimeSlotViewSettings.dayFormat], and [TimeSlotViewSettings.timeRulerSize]
  /// in time slot views of calendar.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.workWeek,
  ///        timeSlotViewSettings: TimeSlotViewSettings(
  ///            startHour: 10,
  ///            endHour: 20,
  ///            nonWorkingDays: <int>[
  ///              DateTime.saturday,
  ///              DateTime.sunday,
  ///              DateTime.friday
  ///            ],
  ///            timeInterval: Duration(minutes: 120),
  ///            timeIntervalHeight: 80,
  ///            timeFormat: 'h:mm',
  ///            dateFormat: 'd',
  ///            dayFormat: 'EEE',
  ///            timeRulerSize: 70),
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final TimeSlotViewSettings timeSlotViewSettings;

  /// The settings have properties which allow to customize the month view of
  /// the [SfCalendar].
  ///
  /// Allows to customize the [MonthViewSettings.dayFormat],
  /// [MonthViewSettings.numberOfWeeksInView], [MonthViewSettings.appointmentDisplayMode],
  /// [MonthViewSettings.showAgenda], [MonthViewSettings.appointmentDisplayCount],
  /// and [MonthViewSettings.navigationDirection] in month view of calendar.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.Month,
  ///        monthViewSettings: MonthViewSettings(
  ///            dayFormat: 'EEE',
  ///            numberOfWeeksInView: 4,
  ///            appointmentDisplayCount: 2,
  ///            appointmentDisplayMode: MonthAppointmentDisplayMode.appointment,
  ///            showAgenda: false,
  ///            navigationDirection: MonthNavigationDirection.horizontal),
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final MonthViewSettings monthViewSettings;

  /// The decoration for the selection cells in [SfCalendar].
  ///
  /// Defaults to null.
  ///
  /// Using a [SfCalendarTheme] gives more fine-grained control over the
  /// appearance of various components of the calendar.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.Month,
  ///        selectionDecoration: BoxDecoration(
  ///           color: Colors.transparent,
  ///             border:
  ///               Border.all(color: const Color.fromARGB(255, 68, 140, 255), width: 2),
  ///             borderRadius: const BorderRadius.all(Radius.circular(4)),
  ///             shape: BoxShape.rectangle,
  ///         );,
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final Decoration selectionDecoration;

  /// The initial date to show on the [SfCalendar].
  ///
  /// The [SfCalendar] will display the dates based on the date set to this
  /// property.
  ///
  /// Defaults to `DateTime(DateTime.now().year, DateTime.now().month,
  /// DateTime.now().day, 08, 45, 0)`.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        initialDisplayDate: DateTime(2020, 02, 05, 10, 0, 0),
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final DateTime initialDisplayDate;

  /// The time zone for [SfCalendar] to function.
  ///
  /// If the [Appointment.startTimeZone] and [Appointment.endTimeZone] set as
  /// [null] the appointments will be displayed in UTC time based on the
  /// time zone set to this property.
  ///
  /// If the [Appointment.startTimeZone] and [Appointment.endTimeZone] set as not
  /// [null] the appointments will be displayed based by calculating the
  /// appointment's startTimeZone and endTimeZone based on the time zone set to
  /// this property.
  ///
  /// Defaults to null.
  ///
  /// See also:
  /// [Appointment.startTimeZone].
  /// [Appointment.endTimeZone].
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        timeZone: 'Atlantic Standard Time',
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final String timeZone;

  /// The date to initially select on the [SfCalendar].
  ///
  /// The [SfCalendar] will select the date that set to this property.
  ///
  /// Defaults to null.
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        initialSelectedDate: DateTime(2019, 12, 12, 11, 0, 0),
  ///      ),
  ///   );
  ///  }
  ///
  /// ```
  final DateTime initialSelectedDate;

  /// Called when the current visible date changes in [SfCalendar].
  ///
  /// Called in the following scenarios when the visible dates were changed
  /// 1. When calendar loaded the visible dates initially.
  /// 2. When calendar view swiped to previous/next view.
  /// 3. When calendar view changed, i.e: Month to day, etc..,
  /// 4. When navigated to a specific date programmatically by using the
  /// [controller.displayDate].
  /// 5. When navigated programmatically using [controller.forward] and
  /// [controller.backward].
  ///
  /// The visible dates collection visible on view when the view changes available
  /// in the [ViewChangedDetails].
  ///
  /// See also: [ViewChangedDetails].
  ///
  /// ```dart
  ///
  ///Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        onViewChanged: (ViewChangedDetails details){
  ///          List<DateTime> dates = details.visibleDates;
  ///        },
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final ViewChangedCallback onViewChanged;

  /// Called whenever the [SfCalendar] elements tapped on view.
  ///
  /// The tapped date, appointments, and element details when the tap action
  /// performed on element available in the [CalendarTapDetails].
  ///
  /// see also:
  /// [CalendarTapDetails].
  /// [CalendarElement]
  ///
  /// ```dart
  ///
  ///return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        onTap: (CalendarTapDetails details){
  ///          DateTime date = details.date;
  ///          dynamic appointments = details.appointments;
  ///          CalendarElement view = details.targetElement;
  ///        },
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final CalendarTapCallback onTap;

  /// Called whenever the [SfCalendar] elements long pressed on view.
  ///
  /// The long-pressed date, appointments, and element details when the long-press action
  /// performed on element available in the [CalendarLongPressDetails].
  ///
  /// see also:
  /// [CalendarLongPressDetails].
  /// [CalendarElement]
  ///
  /// ```dart
  ///
  ///return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        onLongPress: (CalendarLongPressDetails details){
  ///          DateTime date = details.date;
  ///          dynamic appointments = details.appointments;
  ///          CalendarElement view = details.targetElement;
  ///        },
  ///      ),
  ///    );
  ///  }
  ///
  /// ```
  final CalendarLongPressCallback onLongPress;

  /// Used to set the [Appointment] or custom event collection through the
  /// [CalendarDataSource] class.
  ///
  /// If it is not [null] the collection of appointments set to the
  /// [CalendarDataSource.appointments] property will be set to [SfCalendar] and
  /// rendered on view.
  ///
  /// Defaults to null.
  ///
  /// see also: [CalendarDataSource]
  ///
  /// ```dart
  ///
  /// Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        dataSource: _getCalendarDataSource(),
  ///        timeSlotViewSettings: TimeSlotViewSettings(
  ///            appointmentTextStyle: TextStyle(
  ///                fontSize: 12,
  ///                fontWeight: FontWeight.w500,
  ///                color: Colors.blue,
  ///                fontStyle: FontStyle.italic)
  ///        ),
  ///      ),
  ///    );
  ///  }
  ///
  /// class DataSource extends CalendarDataSource {
  ///  DataSource(List<Appointment> source) {
  ///    appointments = source;
  ///  }
  /// }
  ///
  ///  DataSource _getCalendarDataSource() {
  ///    List<Appointment> appointments = <Appointment>[];
  ///    appointments.add(Appointment(
  ///      startTime: DateTime.now(),
  ///      endTime: DateTime.now().add(Duration(hours: 2)),
  ///      isAllDay: true,
  ///      subject: 'Meeting',
  ///      color: Colors.blue,
  ///      startTimeZone: '',
  ///      endTimeZone: '',
  ///    ));
  ///
  ///    return DataSource(appointments);
  ///  }
  ///
  /// ```
  final CalendarDataSource dataSource;

  /// Defines the collection of special [TimeRegion] for [SfCalendar].
  ///
  /// It is used to highlight time slots on day, week, work week
  /// and timeline views based on [TimeRegion] start and end time.
  ///
  /// It also used to restrict interaction on time slots.
  ///
  /// ``` dart
  ///  Widget build(BuildContext context) {
  ///    return Container(
  ///      child: SfCalendar(
  ///        view: CalendarView.week,
  ///        specialRegions: _getTimeRegions(),
  ///      ),
  ///    );
  ///  }
  ///
  ///  List<TimeRegion> _getTimeRegions() {
  ///    final List<TimeRegion> regions = <TimeRegion>[];
  ///    regions.add(TimeRegion(
  ///        startTime: DateTime.now(),
  ///        endTime: DateTime.now().add(Duration(hours: 1)),
  ///        enablePointerInteraction: false,
  ///        color: Colors.grey.withOpacity(0.2),
  ///        text: 'Break'));
  ///
  ///    return regions;
  ///  }
  ///
  ///  ```
  final List<TimeRegion> specialRegions;

  /// An object that used for programmatic date navigation and date selection
  /// in [SfCalendar].
  ///
  /// A [CalendarController] served for several purposes. It can be used
  /// to selected dates programmatically on [SfCalendar] by using the
  /// [controller.selectedDate]. It can be used to navigate to specific date
  /// by using the [controller.displayDate] property.
  ///
  /// ## Listening to property changes:
  /// The [CalendarController] is a listenable. It notifies it's listeners
  /// whenever any of attached [SfCalendar]`s selected date, display date
  /// changed (i.e: selecting a different date, swiping to next/previous
  /// view] in in [SfCalendar].
  ///
  /// ## Navigates to different view:
  /// In [SfCalendar] the visible view can be navigated programmatically by
  /// using the [controller.forward] and [controller.backward] method.
  ///
  /// ## Programmatic selection:
  /// In [SfCalendar] selecting dates programmatically can be achieved by
  /// using the [controller.selectedDate] which allows to select date on
  /// [SfCalendar] on initial load and in run time.
  ///
  /// The [CalendarController] can be listened by adding a listener to the
  /// controller, the listener will listen and notify whenever the selected date,
  /// display date changed in the [SfCalendar].
  ///
  /// See also: [CalendarController].
  ///
  /// Defaults to null.
  ///
  /// This example demonstrates how to use the [CalendarController] for [SfCalendar].
  ///
  /// ```dart
  ///
  /// class MyAppState extends State<MyApp>{
  ///
  ///  CalendarController _calendarController;
  ///  @override
  ///  initState(){
  ///    _calendarController = CalendarController();
  ///    _calendarController.selectedDate = DateTime(2022, 02, 05);
  ///    _calendarController.displayDate = DateTime(2022, 02, 05);
  ///    super.initState();
  ///  }
  ///
  ///  @override
  ///  Widget build(BuildContext context) {
  ///    return MaterialApp(
  ///      home: Scaffold(
  ///        body: SfCalendar(
  ///          view: CalendarView.month,
  ///          controller: _calendarController,
  ///        ),
  ///      ),
  ///    );
  ///  }
  ///}
  /// ```
  final CalendarController controller;

  /// Returns the date time collection at which the recurrence appointment will
  /// recur
  ///
  /// Using this method the recurrence appointments occurrence date time collection
  /// can be obtained.
  ///
  /// * rRule - required - the recurrence rule of the appointment
  /// * recurrenceStartDate - required - the start date in which the recurrence starts.
  /// * specificStartDate - optional - the specific start date, used to get the
  /// date collection for a specific interval of dates.
  /// * specificEndDate - optional - the specific end date, used to get the date
  /// collection for a specific interval of dates.
  ///
  ///
  /// return `List<DateTime>`
  ///
  ///```dart
  ///
  /// DateTime dateTime = DateTime(2020, 03, 15);
  /// List<DateTime> dateCollection = SfCalendar.getRecurrenceDateTimeCollection(
  ///    'FREQ=DAILY;INTERVAL=1;COUNT=3', dateTime);
  ///
  /// ```
  static List<DateTime> getRecurrenceDateTimeCollection(
      String rRule, DateTime recurrenceStartDate,
      {DateTime specificStartDate, DateTime specificEndDate}) {
    return _getRecurrenceDateTimeCollection(rRule, recurrenceStartDate,
        specificStartDate: specificStartDate, specificEndDate: specificEndDate);
  }

  /// Returns the recurrence properties based on the given recurrence rule and
  /// the recurrence start date.
  ///
  /// Used to get the recurrence properties from the given recurrence rule.
  ///
  /// * rRule - optional - recurrence rule for the properties required
  /// * recStartDate - optional - start date of the recurrence rule for which the
  /// properties required.
  ///
  /// returns `RecurrenceProperties`.
  ///
  /// ```dart
  ///
  /// DateTime dateTime = DateTime(2020, 03, 15);
  /// RecurrenceProperties recurrenceProperties =
  ///    SfCalendar.parseRRule('FREQ=DAILY;INTERVAL=1;COUNT=1', dateTime);
  ///
  /// ```
  static RecurrenceProperties parseRRule(String rRule, DateTime recStartDate) {
    return _parseRRule(rRule, recStartDate);
  }

  /// Generates the recurrence rule based on the given recurrence properties and
  /// the start date and end date of the recurrence appointment.
  ///
  /// Used to generate recurrence rule based on the recurrence properties.
  ///
  /// * recurrenceProperties - required - the recurrence properties to generate
  /// the recurrence rule.
  /// * appStartTime - required - the recurrence appointment start time.
  /// * appEndTime - required - the recurrence appointment end time.
  ///
  /// returns `String`.
  ///
  /// ```dart
  ///
  /// RecurrenceProperties recurrenceProperties = RecurrenceProperties();
  //recurrenceProperties.recurrenceType = RecurrenceType.daily;
  //recurrenceProperties.recurrenceRange = RecurrenceRange.count;
  //recurrenceProperties.interval = 2;
  //recurrenceProperties.recurrenceCount = 10;
  //
  ///Appointment appointment = Appointment(
  ///    startTime: DateTime(2019, 12, 16, 10),
  ///    endTime: DateTime(2019, 12, 16, 12),
  ///    subject: 'Meeting',
  ///    color: Colors.blue,
  ///    recurrenceRule: SfCalendar.generateRRule(recurrenceProperties,
  ///        DateTime(2019, 12, 16, 10), DateTime(2019, 12, 16, 12)));
  ///
  /// ```
  static String generateRRule(RecurrenceProperties recurrenceProperties,
      DateTime appStartTime, DateTime appEndTime) {
    return _generateRRule(recurrenceProperties, appStartTime, appEndTime);
  }

  @override
  _SfCalendarState createState() => _SfCalendarState();
}

class _SfCalendarState extends State<SfCalendar> {
  List<DateTime> _currentViewVisibleDates;
  DateTime _currentDate, _selectedDate;
  List<Appointment> _visibleAppointments;
  List<_AppointmentView> _allDayAppointmentViewCollection =
      <_AppointmentView>[];
  double _allDayPanelHeight = 0;
  ScrollController _agendaScrollController;
  ValueNotifier<DateTime> _agendaSelectedDate, _headerUpdateNotifier;
  String _locale;
  SfLocalizations _localizations;
  double _minWidth, _minHeight;
  SfCalendarThemeData _calendarTheme;
  ValueNotifier<Offset> _headerHoverNotifier;
  ValueNotifier<_ScheduleViewHoveringDetails> _agendaDateNotifier,
      _agendaViewNotifier;

  /// Used to assign the forward list as center of scroll view.
  Key _scheduleViewKey;

  /// Used to create the new scroll view for schedule calendar view.
  Key _scrollKey;

  /// Used to get the scrolled position to update the header value.
  ScrollController _scheduleViewController;

  /// Used to store the visible dates before the display date
  List<DateTime> previousDates;

  /// Used to store the visible dates after the display date
  List<DateTime> nextDates;

  /// Used to store the height of each views generated by next dates.
  Map<int, _ScheduleViewDetails> _forwardWidgetHeights;

  /// Used to store the height of each views generated by previous dates.
  Map<int, _ScheduleViewDetails> _backwardWidgetHeights;

  /// Used to store the max and min visible date.
  DateTime _minDate, _maxDate;

  //// Used to notify the time zone data base loaded or not.
  //// Example, initially appointment added on visible date changed callback then
  //// data source changed listener perform operation but the time zone data base
  //// not initialized, so it makes error.
  bool _timeZoneLoaded = false;
  List<Appointment> _appointments;
  CalendarController _controller;

  /// loads the time zone data base to handle the time zone for calendar
  Future<bool> _loadDataBase() async {
    final dynamic byteData =
        await rootBundle.load('packages/timezone/data/2020a.tzf');
    initializeDatabase(byteData.buffer.asUint8List());
    _timeZoneLoaded = true;
    return true;
  }

  /// Generates the calendar appointments from the given data source, and
  /// time zone details
  void _getAppointment() {
    _appointments = _generateCalendarAppointments(widget.dataSource, widget);
    _updateVisibleAppointments();
  }

  /// Updates the visible appointments for the calendar
  // ignore: avoid_void_async
  void _updateVisibleAppointments() async {
    if (widget.view != CalendarView.schedule) {
      // ignore: await_only_futures
      _visibleAppointments = await _getVisibleAppointments(
          _currentViewVisibleDates[0],
          _currentViewVisibleDates[_currentViewVisibleDates.length - 1],
          _appointments,
          widget.timeZone,
          widget.view == CalendarView.month || _isTimelineView(widget.view));
      //// Update all day appointment related implementation in calendar, because time label view needs the top position.
      _updateAllDayAppointment();
    }

    //// mounted property in state return false when the state disposed,
    //// restrict the async method set state after the state disposed.
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    if (_agendaScrollController != null) {
      _agendaScrollController.dispose();
      _agendaScrollController = null;
    }

    _controller.removePropertyChangedListener(_calendarValueChangedListener);
    super.dispose();
  }

  void _updateViewHeaderHover() {
    if (!mounted) {
      return;
    }

    setState(() {});
  }

  @override
  void initState() {
    _timeZoneLoaded = false;
    initializeDateFormatting();
    _loadDataBase().then((dynamic value) => _getAppointment());
    _agendaDateNotifier = ValueNotifier<_ScheduleViewHoveringDetails>(null);
    _agendaViewNotifier = ValueNotifier<_ScheduleViewHoveringDetails>(null);
    _headerHoverNotifier = ValueNotifier<Offset>(null)
      ..addListener(_updateViewHeaderHover);
    _controller = widget.controller ?? CalendarController();
    _controller.selectedDate = widget.initialSelectedDate;
    _selectedDate = widget.initialSelectedDate;
    _agendaSelectedDate = ValueNotifier<DateTime>(widget.initialSelectedDate);
    _agendaSelectedDate.addListener(_agendaSelectedDateListener);
    _currentDate =
        getValidDate(widget.minDate, widget.maxDate, widget.initialDisplayDate);
    _controller.displayDate = _currentDate;
    _updateCurrentVisibleDates();
    widget.dataSource?.addListener(_dataSourceChangedListener);
    if (widget.view == CalendarView.month &&
        widget.monthViewSettings.showAgenda) {
      _agendaScrollController =
          ScrollController(initialScrollOffset: 0, keepScrollOffset: true);
    }

    _controller.addPropertyChangedListener(_calendarValueChangedListener);
    if (widget.view == CalendarView.schedule &&
        _shouldRaiseViewChangedCallback(widget.onViewChanged)) {
      _raiseViewChangedCallback(widget,
          visibleDates: <DateTime>[]..add(_controller.displayDate));
    }

    _initScheduleViewProperties();
    super.initState();
  }

  void _initScheduleViewProperties() {
    _scheduleViewKey ??= UniqueKey();
    _scrollKey = UniqueKey();
    nextDates = <DateTime>[];
    previousDates = <DateTime>[];
    _headerUpdateNotifier = ValueNotifier<DateTime>(_controller.displayDate);
    _forwardWidgetHeights = <int, _ScheduleViewDetails>{};
    _backwardWidgetHeights = <int, _ScheduleViewDetails>{};

    /// Add listener for scroll view to handle the scroll view scroll position changes.
    _scheduleViewController = ScrollController()
      ..addListener(_handleScheduleViewScrolled);
  }

  /// Handle the scroll view scroll changes to update header date value.
  void _handleScheduleViewScrolled() {
    final double _scrolledPosition = _scheduleViewController.position.pixels;
    double widgetPosition = 0;

    /// Scrolled position greater than zero then it moves to forward views.
    if (_scrolledPosition >= 0) {
      for (int i = 0; i < _forwardWidgetHeights.length; i++) {
        final _ScheduleViewDetails details =
            _forwardWidgetHeights.containsKey(i)
                ? _forwardWidgetHeights[i]
                : null;
        final double widgetHeight = details == null ? 0 : details._height;
        final double interSectionPoint =
            details == null ? 0 : details._intersectPoint;

        /// Check the scrolled position in between the view position
        if (_scrolledPosition >= widgetPosition &&
            _scrolledPosition < widgetHeight) {
          DateTime date = nextDates[i];

          /// Check the view have intersection point, because intersection point
          /// tells the view does not have similar month dates. If it reaches the
          /// intersection point then it moves to another month date so
          /// update the header view date with latest date.
          if (interSectionPoint != 0 && _scrolledPosition > interSectionPoint) {
            date = addDuration(date, const Duration(days: 6));
          }

          if (date.month != _headerUpdateNotifier.value.month ||
              date.year != _headerUpdateNotifier.value.year) {
            _headerUpdateNotifier.value = date;
          }

          break;
        }

        widgetPosition = widgetHeight;
      }
    } else {
      /// Scrolled position less than zero then it moves to backward views.
      for (int i = 0; i < _backwardWidgetHeights.length; i++) {
        final _ScheduleViewDetails details =
            _backwardWidgetHeights.containsKey(i)
                ? _backwardWidgetHeights[i]
                : null;
        final double widgetHeight = details == null ? 0 : details._height;
        final double interSectionPoint =
            details == null ? 0 : details._intersectPoint;

        /// Check the scrolled position in between the view position
        if (-_scrolledPosition > widgetPosition &&
            -_scrolledPosition <= widgetHeight) {
          DateTime date = previousDates[i];

          /// Check the view have intersection point, because intersection point
          /// tells the view does not have similar month dates. If it reaches the
          /// intersection point then it moves to another month date so
          /// update the header view date with latest date.
          if (interSectionPoint != 0 &&
              -_scrolledPosition <= interSectionPoint) {
            date = addDuration(date, const Duration(days: 6));
          }

          if (date.month != _headerUpdateNotifier.value.month ||
              date.year != _headerUpdateNotifier.value.year) {
            _headerUpdateNotifier.value = date;
          }

          break;
        }

        widgetPosition = widgetHeight;
      }
    }
  }

  void _calendarValueChangedListener(String property) {
    if (property == 'selectedDate') {
      if (isSameDate(_selectedDate, _controller.selectedDate) &&
          _isSameTimeSlot(_selectedDate, _controller.selectedDate)) {
        return;
      }

      setState(() {
        _selectedDate = _controller.selectedDate;
      });
    } else if (property == 'displayDate') {
      if (isSameDate(_currentDate, _controller.displayDate) ||
          isDateWithInDateRange(
              _currentViewVisibleDates[0],
              _currentViewVisibleDates[_currentViewVisibleDates.length - 1],
              _controller.displayDate)) {
        _currentDate = _controller.displayDate;
        return;
      }

      setState(() {
        _currentDate = _controller.displayDate;
        _updateCurrentVisibleDates();
        if (widget.view == CalendarView.schedule) {
          _scheduleViewController?.removeListener(_handleScheduleViewScrolled);
          _initScheduleViewProperties();
        }
      });
    }
  }

  void _updateCurrentVisibleDates() {
    final DateTime currentDate = _currentDate;
    final List<int> _nonWorkingDays = (widget.view == CalendarView.workWeek ||
            widget.view == CalendarView.timelineWorkWeek)
        ? widget.timeSlotViewSettings.nonWorkingDays
        : null;

    _currentViewVisibleDates = getVisibleDates(
        currentDate,
        _nonWorkingDays,
        widget.firstDayOfWeek,
        _getViewDatesCount(widget.view,
            widget.monthViewSettings.numberOfWeeksInView, currentDate));
  }

  //// Perform action while data source changed based on data source action.
  void _dataSourceChangedListener(
      CalendarDataSourceAction type, List<dynamic> data) {
    if (!_timeZoneLoaded || !mounted) {
      return;
    }

    if (type == CalendarDataSourceAction.reset) {
      _getAppointment();
    } else {
      setState(() {
        final List<Appointment> visibleAppointmentCollection = <Appointment>[];
        //// Clone the visible appointments because if we add visible appointment directly then
        //// calendar view visible appointment also updated so it does not perform to paint, So
        //// clone the visible appointment and added newly added appointment and set the value.
        if (_visibleAppointments != null) {
          for (int i = 0; i < _visibleAppointments.length; i++) {
            visibleAppointmentCollection.add(_visibleAppointments[i]);
          }
        }

        _appointments ??= <Appointment>[];
        if (type == CalendarDataSourceAction.add) {
          final List<Appointment> collection =
              _generateCalendarAppointments(widget.dataSource, widget, data);
          final List<Appointment> visibleCollection = widget.view ==
                  CalendarView.schedule
              ? <Appointment>[]
              : _getVisibleAppointments(
                  _currentViewVisibleDates[0],
                  _currentViewVisibleDates[_currentViewVisibleDates.length - 1],
                  collection,
                  widget.timeZone,
                  widget.view == CalendarView.month ||
                      _isTimelineView(widget.view));

          for (int i = 0; i < collection.length; i++) {
            _appointments.add(collection[i]);
          }

          for (int i = 0; i < visibleCollection.length; i++) {
            visibleAppointmentCollection.add(visibleCollection[i]);
          }
        } else if (type == CalendarDataSourceAction.remove) {
          for (int i = 0; i < data.length; i++) {
            final Object appointment = data[i];
            for (int j = 0; j < _appointments.length; j++) {
              if (_appointments[j]._data == appointment) {
                _appointments.removeAt(j);
                j--;
              }
            }
          }

          for (int i = 0; i < data.length; i++) {
            final Object appointment = data[i];
            for (int j = 0; j < visibleAppointmentCollection.length; j++) {
              if (visibleAppointmentCollection[j]._data == appointment) {
                visibleAppointmentCollection.removeAt(j);
                j--;
              }
            }
          }
        }

        _visibleAppointments = visibleAppointmentCollection;
        //// Update all day appointment related implementation in calendar, because time label view needs the top position.
        _updateAllDayAppointment();
      });
    }
  }

  void _agendaSelectedDateListener() {
    if (widget.view != CalendarView.month ||
        !widget.monthViewSettings.showAgenda) {
      return;
    }

    setState(() {});
  }

  @override
  void didUpdateWidget(SfCalendar oldWidget) {
    if (oldWidget.controller != widget.controller) {
      oldWidget.controller
          ?.removePropertyChangedListener(_calendarValueChangedListener);
      _controller = widget.controller ?? CalendarController();
      if (widget.controller != null) {
        _controller.selectedDate = widget.controller.selectedDate;
        _controller.displayDate = widget.controller.displayDate ?? _currentDate;
      } else {
        _controller.selectedDate = widget.initialSelectedDate;
        _currentDate = getValidDate(
            widget.minDate, widget.maxDate, widget.initialDisplayDate);
        _controller.displayDate = _currentDate;
      }
      _selectedDate = widget.controller.selectedDate;
      _controller.addPropertyChangedListener(_calendarValueChangedListener);
    }

    if (oldWidget.controller == widget.controller &&
        widget.controller != null &&
        oldWidget.controller.selectedDate != widget.controller.selectedDate) {
      _selectedDate = _controller.selectedDate;
      _agendaSelectedDate.value = _controller.selectedDate;
    }

    if (oldWidget.scheduleViewSettings.hideEmptyScheduleWeek !=
        widget.scheduleViewSettings.hideEmptyScheduleWeek) {
      previousDates.clear();
      nextDates.clear();
      _backwardWidgetHeights.clear();
      _forwardWidgetHeights.clear();
      WidgetsBinding.instance.addPostFrameCallback((Duration timeStamp) {
        _handleScheduleViewScrolled();
      });
    }

    if (oldWidget.controller == widget.controller &&
        widget.controller != null &&
        oldWidget.controller.displayDate != widget.controller.displayDate) {
      if (_controller.displayDate != null) {
        _currentDate = getValidDate(
            widget.minDate, widget.maxDate, _controller.displayDate);
      }
      _currentDate = _currentDate ??
          getValidDate(
              widget.minDate, widget.maxDate, widget.initialDisplayDate);

      _controller.displayDate = _currentDate;
    }

    if (_agendaSelectedDate.value != _selectedDate) {
      _agendaSelectedDate.value = _selectedDate;
    }

    if (oldWidget.timeZone != widget.timeZone) {
      _updateVisibleAppointments();
    }

    if (oldWidget.view != widget.view ||
        widget.monthViewSettings.numberOfWeeksInView !=
            oldWidget.monthViewSettings.numberOfWeeksInView) {
      _currentDate = getValidDate(
          widget.minDate, widget.maxDate, _updateCurrentDate(oldWidget));
      _controller.displayDate = _currentDate;
      if (widget.view == CalendarView.schedule) {
        if (_shouldRaiseViewChangedCallback(widget.onViewChanged)) {
          _raiseViewChangedCallback(widget,
              visibleDates: <DateTime>[]..add(_controller.displayDate));
        }

        _scheduleViewController?.removeListener(_handleScheduleViewScrolled);
        _initScheduleViewProperties();
      }
    }

    if (oldWidget.dataSource != widget.dataSource) {
      _getAppointment();
      oldWidget.dataSource?.removeListener(_dataSourceChangedListener);
      widget.dataSource?.addListener(_dataSourceChangedListener);
    }

    if ((oldWidget.minDate != widget.minDate && widget.minDate != null) ||
        (oldWidget.maxDate != widget.maxDate && widget.maxDate != null)) {
      _currentDate = getValidDate(
          widget.minDate, widget.maxDate, widget.initialDisplayDate);
      if (widget.view == CalendarView.schedule) {
        _minDate = null;
        _maxDate = null;
      }
    }

    if (widget.view == CalendarView.month &&
        widget.monthViewSettings.showAgenda &&
        _agendaScrollController == null) {
      _agendaScrollController =
          ScrollController(initialScrollOffset: 0, keepScrollOffset: true);
    }

    super.didUpdateWidget(oldWidget);
  }

  DateTime _updateCurrentDate(SfCalendar _oldWidget) {
    // condition added to updated the current visible date while switching the calendar views
    // if any date selected in the current view then, while switching the view the view move based the selected date
    // if no date selected and the current view has the today date, then switching the view will move based on the today date
    // if no date selected and today date doesn't falls in current view, then switching the view will move based the first day of current view

    if (_oldWidget.view == CalendarView.schedule) {
      return _headerUpdateNotifier.value ?? _controller.displayDate;
    }

    if (_selectedDate != null &&
        isDateWithInDateRange(
            _currentViewVisibleDates[0],
            _currentViewVisibleDates[_currentViewVisibleDates.length - 1],
            _selectedDate)) {
      if (_oldWidget.view == CalendarView.month) {
        return DateTime(
            _selectedDate.year,
            _selectedDate.month,
            _selectedDate.day,
            _controller.displayDate.hour,
            _controller.displayDate.minute,
            _controller.displayDate.second);
      } else {
        return _selectedDate;
      }
    } else if (isDateWithInDateRange(
        _currentViewVisibleDates[0],
        _currentViewVisibleDates[_currentViewVisibleDates.length - 1],
        DateTime.now())) {
      final DateTime _date = DateTime.now();
      return DateTime(
          _date.year,
          _date.month,
          _date.day,
          _controller.displayDate.hour,
          _controller.displayDate.minute,
          _controller.displayDate.second);
    } else {
      if (_oldWidget.view == CalendarView.month) {
        if (widget.monthViewSettings.numberOfWeeksInView > 0 &&
            widget.monthViewSettings.numberOfWeeksInView < 6) {
          return _currentViewVisibleDates[0];
        }
        return DateTime(
            _currentDate.year,
            _currentDate.month,
            1,
            _controller.displayDate.hour,
            _controller.displayDate.minute,
            _controller.displayDate.second);
      } else {
        final DateTime _date = _currentViewVisibleDates[0];
        return DateTime(
            _date.year,
            _date.month,
            _date.day,
            _controller.displayDate.hour,
            _controller.displayDate.minute,
            _controller.displayDate.second);
      }
    }
  }

  _AppointmentView _getAppointmentView(Appointment appointment) {
    _AppointmentView appointmentRenderer;
    for (int i = 0; i < _allDayAppointmentViewCollection.length; i++) {
      final _AppointmentView view = _allDayAppointmentViewCollection[i];
      if (view.appointment == null) {
        appointmentRenderer = view;
        break;
      }
    }

    if (appointmentRenderer == null) {
      appointmentRenderer = _AppointmentView();
      appointmentRenderer.appointment = appointment;
      _allDayAppointmentViewCollection.add(appointmentRenderer);
    }

    appointmentRenderer.appointment = appointment;
    appointmentRenderer.canReuse = false;
    return appointmentRenderer;
  }

  @override
  void didChangeDependencies() {
    // default width value will be device width when the widget placed inside a infinity width widget
    _minWidth = MediaQuery.of(context).size.width;
    // default height for the widget when the widget placed inside a infinity height widget
    _minHeight = 300;
    final SfCalendarThemeData calendarThemeData = SfCalendarTheme.of(context);
    final ThemeData themeData = Theme.of(context);
    _calendarTheme = calendarThemeData.copyWith(
        todayHighlightColor:
            calendarThemeData.todayHighlightColor ?? themeData.accentColor,
        selectionBorderColor:
            calendarThemeData.selectionBorderColor ?? themeData.accentColor);
    //// localeOf(context) returns the locale from material app when SfCalendar locale value as null
    _locale = Localizations.localeOf(context).toString();
    _localizations = SfLocalizations.of(context);
    super.didChangeDependencies();
  }

  void _updateAllDayAppointment() {
    _allDayPanelHeight = 0;
    if (widget.view == CalendarView.week ||
        widget.view == CalendarView.workWeek ||
        widget.view == CalendarView.day) {
      _allDayAppointmentViewCollection =
          _allDayAppointmentViewCollection ?? <_AppointmentView>[];

      //// Remove the existing appointment related details.
      for (int i = 0; i < _allDayAppointmentViewCollection.length; i++) {
        final _AppointmentView obj = _allDayAppointmentViewCollection[i];
        obj.canReuse = true;
        obj.appointment = null;
        obj.startIndex = -1;
        obj.endIndex = -1;
        obj.position = -1;
        obj.maxPositions = -1;
      }

      if (_visibleAppointments == null || _visibleAppointments.isEmpty) {
        return;
      }

      //// Calculate the visible all day appointment collection.
      final List<Appointment> allDayAppointments = <Appointment>[];
      for (final Appointment apppointment in _visibleAppointments) {
        if (apppointment.isAllDay ||
            apppointment._actualEndTime
                    .difference(apppointment._actualStartTime)
                    .inDays >
                0) {
          allDayAppointments.add(apppointment);
        }
      }

      //// Update the appointment view collection with visible appointments.
      for (int i = 0; i < allDayAppointments.length; i++) {
        _AppointmentView _appointmentView;
        if (_allDayAppointmentViewCollection.length > i) {
          _appointmentView = _allDayAppointmentViewCollection[i];
        } else {
          _appointmentView = _AppointmentView();
          _allDayAppointmentViewCollection.add(_appointmentView);
        }

        _appointmentView.appointment = allDayAppointments[i];
        _appointmentView.canReuse = false;
      }

      //// Calculate the appointment view position.
      for (final _AppointmentView _appointmentView
          in _allDayAppointmentViewCollection) {
        if (_appointmentView.appointment == null) {
          continue;
        }

        final int startIndex = _getIndex(_currentViewVisibleDates,
            _appointmentView.appointment._actualStartTime);
        final int endIndex = _getIndex(_currentViewVisibleDates,
                _appointmentView.appointment._actualEndTime) +
            1;
        if (startIndex == -1 && endIndex == 0) {
          _appointmentView.appointment = null;
          continue;
        }

        _appointmentView.startIndex = startIndex;
        _appointmentView.endIndex = endIndex;
      }

      //// Sort the appointment view based on appointment view width.
      _allDayAppointmentViewCollection
          .sort((_AppointmentView app1, _AppointmentView app2) {
        if (app1.appointment != null && app2.appointment != null) {
          return (app2.endIndex - app2.startIndex) >
                  (app1.endIndex - app1.startIndex)
              ? 1
              : 0;
        }

        return 0;
      });

      //// Sort the appointment view based on appointment view start position.
      _allDayAppointmentViewCollection
          .sort((_AppointmentView app1, _AppointmentView app2) {
        if (app1.appointment != null && app2.appointment != null) {
          return app1.startIndex.compareTo(app2.startIndex);
        }

        return 0;
      });

      final List<List<_AppointmentView>> _allDayAppointmentView =
          <List<_AppointmentView>>[];
      //// Calculate the intersecting appointment view collection.
      for (int i = 0; i < _currentViewVisibleDates.length; i++) {
        final List<_AppointmentView> intersectingAppointments =
            <_AppointmentView>[];
        for (int j = 0; j < _allDayAppointmentViewCollection.length; j++) {
          final _AppointmentView _currentView =
              _allDayAppointmentViewCollection[j];
          if (_currentView.appointment == null) {
            continue;
          }

          if (_currentView.startIndex <= i && _currentView.endIndex >= i + 1) {
            intersectingAppointments.add(_currentView);
          }
        }

        _allDayAppointmentView.add(intersectingAppointments);
      }

      //// Calculate the appointment view position and max position.
      for (int i = 0; i < _allDayAppointmentView.length; i++) {
        final List<_AppointmentView> intersectingAppointments =
            _allDayAppointmentView[i];
        for (int j = 0; j < intersectingAppointments.length; j++) {
          final _AppointmentView _currentView = intersectingAppointments[j];
          if (_currentView.position == -1) {
            _currentView.position = 0;
            for (int k = 0; k < j; k++) {
              final _AppointmentView _intersectView = _getAppointmentOnPosition(
                  _currentView, intersectingAppointments);
              if (_intersectView != null) {
                _currentView.position++;
              } else {
                break;
              }
            }
          }
        }

        if (intersectingAppointments.isNotEmpty) {
          final int maxPosition = intersectingAppointments
              .reduce((_AppointmentView currentAppview,
                      _AppointmentView nextAppview) =>
                  currentAppview.position > nextAppview.position
                      ? currentAppview
                      : nextAppview)
              .position;
          for (int j = 0; j < intersectingAppointments.length; j++) {
            intersectingAppointments[j].maxPositions = maxPosition + 1;
          }
        }
      }

      int maxPosition = 0;
      if (_allDayAppointmentViewCollection.isNotEmpty) {
        maxPosition = _allDayAppointmentViewCollection
            .reduce((_AppointmentView currentAppview,
                    _AppointmentView nextAppview) =>
                currentAppview.maxPositions > nextAppview.maxPositions
                    ? currentAppview
                    : nextAppview)
            .maxPositions;
      }

      if (maxPosition == -1) {
        maxPosition = 0;
      }

      _allDayPanelHeight = (maxPosition * _kAllDayAppointmentHeight).toDouble();
    }
  }

  // method to check and update the calendar width and height.
  double _updateHeight(double _height) {
    return _height -= widget.headerHeight;
  }

  @override
  Widget build(BuildContext context) {
    SyncfusionLicense.validateLicense(context);
    double _top = 0, _width, _height;
    final bool _isRTL = _isRTLLayout(context);
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      _minWidth = constraints.maxWidth == double.infinity
          ? _minWidth
          : constraints.maxWidth;
      _minHeight = constraints.maxHeight == double.infinity
          ? _minHeight
          : constraints.maxHeight;

      _width = _minWidth;
      _height = _minHeight;

      _height = _updateHeight(_height);
      _top = widget.headerHeight;
      double agendaHeight = widget.view == CalendarView.month &&
              widget.monthViewSettings.showAgenda
          ? widget.monthViewSettings.agendaViewHeight
          : 0;
      if (agendaHeight == null || agendaHeight == -1) {
        agendaHeight = (_height +
                widget.headerHeight +
                _getViewHeaderHeight(widget.viewHeaderHeight, widget.view)) /
            3;
      }

      return Container(
        width: _minWidth,
        height: _minHeight,
        color: widget.backgroundColor ?? _calendarTheme.backgroundColor,
        child: widget.view == CalendarView.schedule
            ? addAgenda(_top, _height, _isRTL)
            : _addChildren(_top, agendaHeight, _height, _width, _isRTL),
      );
    });
  }

  void _updateMouseHoverPosition(dynamic event,
      [bool isRTL,
      DateTime currentDate,
      double startPosition,
      double padding = 0]) {
    final RenderBox box = context.findRenderObject();
    final Offset localPosition = box.globalToLocal(event.position);
    if (localPosition.dy < widget.headerHeight) {
      if (_agendaDateNotifier.value != null) {
        _agendaDateNotifier.value = null;
      }

      if (_agendaViewNotifier.value != null) {
        _agendaViewNotifier.value = null;
      }

      _headerHoverNotifier.value = Offset(localPosition.dx, localPosition.dy);
    } else {
      if (widget.view != CalendarView.month &&
          widget.view != CalendarView.schedule) {
        return;
      }

      double _yPosition = localPosition.dy;
      double _xPosition = localPosition.dx;
      double dateViewWidth =
          _getAgendaViewDayLabelWidth(widget.scheduleViewSettings, _minWidth);
      if (widget.view == CalendarView.month) {
        currentDate = _selectedDate;
        final double _agendaHeight =
            widget.monthViewSettings.agendaViewHeight == -1
                ? _minHeight / 3
                : widget.monthViewSettings.agendaViewHeight;
        _yPosition -= _minHeight - _agendaHeight;
        dateViewWidth = _minWidth * 0.15;
        if (_selectedDate == null) {
          dateViewWidth = 0;
        }

        if (dateViewWidth > 60 && kIsWeb) {
          dateViewWidth = 60;
        }
      } else {
        _yPosition = (_scheduleViewController.offset + localPosition.dy) -
            startPosition -
            widget.headerHeight;
      }

      if ((isRTL && localPosition.dx > (_minWidth - dateViewWidth)) ||
          (!isRTL && localPosition.dx < dateViewWidth)) {
        if (_headerHoverNotifier.value != null) {
          _headerHoverNotifier.value = null;
        }

        if (_agendaViewNotifier.value != null) {
          _agendaViewNotifier.value = null;
        }

        _xPosition = isRTL ? _minWidth - _xPosition : _xPosition;

        _agendaDateNotifier.value = _ScheduleViewHoveringDetails(
            currentDate, Offset(_xPosition, _yPosition));
      } else {
        /// padding value used to specify the view top padding on agenda view.
        /// padding value is assigned when the agenda view has top padding
        /// eg., if the agenda view holds one all day appointment in
        /// schedule view then it have top padding because the agenda view
        /// minimum panel height as appointment height specified in setting.
        _yPosition -= padding;
        if (_headerHoverNotifier.value != null) {
          _headerHoverNotifier.value = null;
        }

        if (_agendaDateNotifier.value != null) {
          _agendaDateNotifier.value = null;
        }

        _agendaViewNotifier.value = _ScheduleViewHoveringDetails(
            currentDate, Offset(_xPosition, _yPosition));
      }
    }
  }

  dynamic _pointerEnterEvent(PointerEnterEvent event,
      [bool isRTL,
      DateTime currentDate,
      double startPosition,
      double padding = 0]) {
    _updateMouseHoverPosition(
        event, isRTL, currentDate, startPosition, padding);
  }

  dynamic _pointerHoverEvent(PointerHoverEvent event,
      [bool isRTL,
      DateTime currentDate,
      double startPosition,
      double padding = 0]) {
    _updateMouseHoverPosition(
        event, isRTL, currentDate, startPosition, padding);
  }

  dynamic _pointerExitEvent(PointerExitEvent event) {
    _headerHoverNotifier.value = null;
    _agendaDateNotifier.value = null;
    _agendaViewNotifier.value = null;
  }

  /// Return the all day appointment count from appointment collection.
  int _getAllDayCount(List<Appointment> appointmentCollection) {
    int allDayCount = 0;
    for (int i = 0; i < appointmentCollection.length; i++) {
      final Appointment appointment = appointmentCollection[i];
      if (_isAllDayAppointmentView(appointment)) {
        allDayCount += 1;
      }
    }

    return allDayCount;
  }

  /// Return the collection of appointment collection listed by
  /// start date of the appointment.
  Map<DateTime, List<Appointment>> _getAppointmentCollectionOnDateBasis(
      List<Appointment> appointmentCollection,
      DateTime startDate,
      DateTime endDate) {
    final Map<DateTime, List<Appointment>> dateAppointments =
        <DateTime, List<Appointment>>{};
    DateTime date = startDate;
    while (date.isBefore(endDate) || isSameDate(endDate, date)) {
      final List<Appointment> _appointmentList = <Appointment>[];
      for (int i = 0; i < appointmentCollection.length; i++) {
        final Appointment appointment = appointmentCollection[i];
        if (!isDateWithInDateRange(
            appointment._actualStartTime, appointment._actualEndTime, date)) {
          continue;
        }

        _appointmentList.add(appointment);
      }

      if (_appointmentList.isNotEmpty) {
        dateAppointments[date] = _appointmentList;
      }

      date = addDuration(date, const Duration(days: 1));
    }

    return dateAppointments;
  }

  /// Return the widget to scroll view based on index.
  Widget _getItem(BuildContext context, int index, bool _isRTL) {
    /// Get the minimum date of schedule view when it value as null
    /// It return min date user assigned when the [hideEmptyScheduleWeek]
    /// in [ScheduleViewSettings] disabled else it return min
    /// start date of the appointment collection.
    _minDate ??= _getMinAppointmentDate(_appointments, widget.timeZone,
        widget.minDate, widget.scheduleViewSettings, _minWidth);
    _minDate = _minDate.isBefore(widget.minDate) ? widget.minDate : _minDate;

    final DateTime _viewMinDate =
        _minDate.add(Duration(days: -_minDate.weekday));

    /// Get the maximum date of schedule view when it value as null
    /// It return max date user assigned when the [hideEmptyScheduleWeek]
    /// in [ScheduleViewSettings] disabled else it return max
    /// end date of the appointment collection.
    _maxDate ??= _getMaxAppointmentDate(_appointments, widget.timeZone,
        widget.maxDate, widget.scheduleViewSettings, _minWidth);
    _maxDate = _maxDate.isAfter(widget.maxDate) ? widget.maxDate : _maxDate;

    final bool _isWebView = kIsWeb && _minWidth > _kMobileViewWidth;

    if (index > 0) {
      /// Add next 100 dates to next dates collection when index
      /// reaches next dates collection end.
      if (nextDates.isNotEmpty && index > nextDates.length - 20) {
        DateTime date = nextDates[nextDates.length - 1];
        int count = 0;
        final bool _hideEmptyAgendaDays =
            widget.scheduleViewSettings.hideEmptyScheduleWeek || _isWebView;

        /// Using while for calculate dates because if [hideEmptyAgendaDays] as
        /// enabled, then it hides the weeks when it does not have appointments.
        while (count < 20) {
          for (int i = 1; i <= 100; i++) {
            final DateTime _date = addDuration(date, Duration(days: i * 7));

            /// Skip the weeks after the max date.
            if (!isSameOrBeforeDate(_maxDate, _date)) {
              count = 20;
              break;
            }

            /// Skip the week date when it does not have appointments
            /// when [hideEmptyAgendaDays] as enabled.
            if (_hideEmptyAgendaDays &&
                !_isAppointmentBetweenDates(
                    _appointments,
                    _date,
                    addDuration(_date, const Duration(days: 6)),
                    widget.timeZone)) {
              continue;
            }

            nextDates.add(_date);
            count++;
          }

          date = addDuration(date, const Duration(days: 700));
        }
      }
    } else {
      /// Add previous 100 dates to previous dates collection when index
      /// reaches previous dates collection end.
      if (previousDates.isNotEmpty && -index > previousDates.length - 20) {
        DateTime date = previousDates[previousDates.length - 1];
        int count = 0;
        final bool _hideEmptyAgendaDays =
            widget.scheduleViewSettings.hideEmptyScheduleWeek || _isWebView;

        /// Using while for calculate dates because if [hideEmptyAgendaDays] as
        /// enabled, then it hides the weeks when it does not have appointments.
        while (count < 20) {
          for (int i = 1; i <= 100; i++) {
            final DateTime _date = addDuration(date, Duration(days: -i * 7));

            /// Skip the weeks before the min date.
            if (!isSameOrAfterDate(_viewMinDate, _date)) {
              count = 20;
              break;
            }

            /// Skip the week date when it does not have appointments
            /// when [hideEmptyAgendaDays] as enabled.
            if (_hideEmptyAgendaDays &&
                !_isAppointmentBetweenDates(
                    _appointments,
                    _date,
                    addDuration(_date, const Duration(days: 6)),
                    widget.timeZone)) {
              continue;
            }
            previousDates.add(_date);
            count++;
          }

          date = addDuration(date, const Duration(days: -700));
        }
      }
    }

    final int _currentIndex = index;

    /// Return null when the index reached the date collection end.
    if (index >= 0
        ? nextDates.length <= index
        : previousDates.length <= -index - 1) {
      return null;
    }

    final DateTime startDate =
        index >= 0 ? nextDates[index] : previousDates[-index - 1];

    /// Set previous date form it date collection if index is first index of
    /// next dates collection then get the start date from previous dates.
    /// If the index as last index of previous dates collection then calculate
    /// by subtract the 7 days to get previous date.
    final DateTime prevDate = index == 0
        ? previousDates.isEmpty
            ? addDuration(startDate, const Duration(days: -7))
            : previousDates[0]
        : (index > 0
            ? nextDates[index - 1]
            : -index >= previousDates.length - 1
                ? addDuration(startDate, const Duration(days: -7))
                : previousDates[-index]);
    final DateTime prevEndDate = addDuration(prevDate, const Duration(days: 6));
    final DateTime endDate = addDuration(startDate, const Duration(days: 6));

    /// Get the visible week appointment and split the appointments based on date.
    final List<Appointment> appointmentCollection = _getVisibleAppointments(
        isSameOrAfterDate(_minDate, startDate) ? startDate : _minDate,
        isSameOrBeforeDate(_maxDate, endDate) ? endDate : _maxDate,
        _appointments,
        widget.timeZone,
        false,
        canCreateNewAppointment: false);
    appointmentCollection.sort((Appointment app1, Appointment app2) =>
        app1._actualStartTime.compareTo(app2._actualStartTime));

    /// Get the collection of appointment collection listed by date.
    final Map<DateTime, List<Appointment>> dateAppointments =
        _getAppointmentCollectionOnDateBasis(
            appointmentCollection, startDate, endDate);
    const double padding = 5;

    /// calculate the day label(eg., May 25) width based on schedule setting.
    final double _viewPadding =
        _getAgendaViewDayLabelWidth(widget.scheduleViewSettings, _minWidth);

    /// Calculate the padding between the month and week label.
    final double _viewTopPadding = !_isWebView ? padding : 0;

    /// calculate the total height using height variable
    /// web view does not have week label.
    double height =
        _isWebView ? 0 : widget.scheduleViewSettings.weekHeaderSettings.height;

    /// It is used to current view top position inside the collection of views.
    double _topHeight = 0;

    /// Check the week date needs month header at first or before of appointment view.
    final bool isNeedMonthBuilder = _isWebView
        ? false
        : prevEndDate.month != startDate.month ||
            prevEndDate.year != startDate.year;

    /// Web view does not have month label.
    height += isNeedMonthBuilder
        ? widget.scheduleViewSettings.monthHeaderSettings.height
        : 0;
    final double _appointmentViewHeight =
        _getScheduleAppointmentHeight(null, widget.scheduleViewSettings);
    final double _allDayAppointmentHeight =
        _getScheduleAllDayAppointmentHeight(null, widget.scheduleViewSettings);

    final List<DateTime> dateAppointmentKeys = dateAppointments.keys.toList();
    int numberOfEvents = 0;

    double appointmentHeight = 0;

    /// Calculate the total height of appointment views of week.
    for (int i = 0; i < dateAppointmentKeys.length; i++) {
      final List<Appointment> _currentDateAppointment =
          dateAppointments[dateAppointmentKeys[i]];
      final int eventsCount = _currentDateAppointment.length;
      int allDayEventCount = 0;

      /// Web view does not differentiate all day and normal appointment.
      if (!_isWebView) {
        allDayEventCount = _getAllDayCount(_currentDateAppointment);
      }

      double panelHeight =
          ((eventsCount - allDayEventCount) * _appointmentViewHeight) +
              (allDayEventCount * _allDayAppointmentHeight);
      panelHeight = panelHeight > _appointmentViewHeight
          ? panelHeight
          : _appointmentViewHeight;
      appointmentHeight += panelHeight;
      numberOfEvents += eventsCount;
    }

    /// Add the padding height to the appointment height
    /// Each of the appointment view have top padding in agenda view and
    /// end agenda view have end padding, so count as (numberOfEvents + 1).
    /// value 1 as padding between the  agenda view and end appointment view.
    /// each of the agenda view in the week have padding add the existing
    /// value with date appointment keys length.
    appointmentHeight +=
        (numberOfEvents + dateAppointmentKeys.length) * padding;

    /// Add appointment height and week view end padding to height.
    height += appointmentHeight + (_isWebView ? 0 : padding);

    /// Create the generated view details to store the view height
    /// and its intersection point.
    final _ScheduleViewDetails _scheduleViewDetails = _ScheduleViewDetails();
    _scheduleViewDetails._intersectPoint = 0;
    double _previousHeight = 0;

    /// Get the previous view end position used to find the next view end position.
    if (_currentIndex >= 0) {
      _previousHeight = _currentIndex == 0
          ? 0
          : _forwardWidgetHeights[_currentIndex - 1]._height;
    } else {
      _previousHeight = _currentIndex == -1
          ? 0
          : _backwardWidgetHeights[-_currentIndex - 2]._height;
    }

    final List<Widget> widgets = <Widget>[];

    /// Web view does not have month label.
    if (!_isWebView) {
      if (isNeedMonthBuilder) {
        /// Add the height of month label to total height of view.
        _topHeight += widget.scheduleViewSettings.monthHeaderSettings.height;
        widgets.add(_getMonthOrWeekHeader(startDate, endDate, _isRTL, true));

        /// Add the week label padding value to top position and total height.
        _topHeight += _viewTopPadding;
        height += _viewTopPadding;
      }

      widgets.add(_getMonthOrWeekHeader(startDate, endDate, _isRTL, false,
          viewPadding: _viewPadding, isNeedTopPadding: isNeedMonthBuilder));

      /// Add the height of week label to update the top position of next view.
      _topHeight += widget.scheduleViewSettings.weekHeaderSettings.height;
    }

    /// Calculate the day label(May, 25) height based on appointment height and
    /// assign the label maximum height as 60.
    double _appointmentViewHeaderHeight =
        _appointmentViewHeight + (2 * padding);
    if (!_isWebView) {
      _appointmentViewHeaderHeight =
          _appointmentViewHeaderHeight > 60 ? 60 : _appointmentViewHeaderHeight;
    }
    double _interSectPoint = _topHeight;

    /// Check the week date needs month header at in between the appointment views.
    bool isNeedInBetweenMonthBuilder = _isWebView
        ? false
        : startDate.month !=
            (isSameOrBeforeDate(_maxDate, endDate) ? endDate : _maxDate).month;

    /// Check the end date month have appointments or not.
    bool _isNextMonthHasNoAppointment = false;
    if (isNeedInBetweenMonthBuilder) {
      final DateTime _lastAppointmentDate = dateAppointmentKeys.isNotEmpty
          ? dateAppointmentKeys[dateAppointmentKeys.length - 1]
          : null;
      final DateTime nextWeekDate = index == -1
          ? nextDates[0]
          : (index < 0
              ? previousDates[-index - 2]
              : index >= nextDates.length - 1 ? null : nextDates[index + 1]);

      /// Check the following scenarios for rendering month label at last when
      /// the week holds different month dates
      /// 1. If the week does not have an appointments.
      /// 2. If the week have appointments but next month dates does not have
      /// an appointments
      /// 3. If the week have appointments but next month dates does not have
      /// an appointments but [hideEmptyScheduleWeek] enabled so the next view
      /// date month as different with current week end date week.
      _isNextMonthHasNoAppointment = _lastAppointmentDate == null ||
          (_lastAppointmentDate != null &&
              _lastAppointmentDate.month != endDate.month &&
              nextWeekDate != null &&
              nextWeekDate.month == endDate.month &&
              nextWeekDate.year == endDate.year);

      /// Check the in between header placed in between appointment views
      /// or end of the week view.
      isNeedInBetweenMonthBuilder = _isNextMonthHasNoAppointment ||
          (_lastAppointmentDate != null &&
              _lastAppointmentDate.month != startDate.month);
    }

    /// Add the in between month label height to total height when
    /// next month dates have appointments(!_isNextMonthHasNoAppointment) or
    /// next month dates does not have appointments and is before max date.
    if (isNeedInBetweenMonthBuilder &&
        (!_isNextMonthHasNoAppointment ||
            isSameOrBeforeDate(_maxDate, endDate))) {
      /// Add the height of month label to total height of view and
      /// Add the month header top padding value to height when in between
      /// week needs month header
      height += widget.scheduleViewSettings.monthHeaderSettings.height +
          _viewTopPadding;
    }

    /// Generate views on week days that have appointments.
    for (int i = 0; i < dateAppointmentKeys.length; i++) {
      final DateTime _currentDate = dateAppointmentKeys[i];
      final List<Appointment> currentAppointments =
          dateAppointments[_currentDate];
      final int eventsCount = currentAppointments.length;
      int allDayEventCount = 0;

      /// Web view does not differentiate all day and normal appointment.
      if (!_isWebView) {
        allDayEventCount = _getAllDayCount(currentAppointments);
      }

      /// Check if the view intersection point not set and the current week date
      /// month differ from the week start date then assign the intersection point.
      if (_scheduleViewDetails._intersectPoint == 0 &&
          (startDate.month != _currentDate.month ||
              startDate.year != _currentDate.year)) {
        /// Assign the intersection point based on previous view end position.
        _scheduleViewDetails._intersectPoint = _currentIndex >= 0
            ? _previousHeight + _interSectPoint + _viewTopPadding
            : _previousHeight + height - _interSectPoint - _viewTopPadding;

        /// Web view does not have month label;
        if (!_isWebView) {
          _interSectPoint +=
              widget.scheduleViewSettings.monthHeaderSettings.height +
                  _viewTopPadding;
          widgets.add(_getMonthOrWeekHeader(_currentDate, null, _isRTL, true,
              isNeedTopPadding: true));
        }
      }

      final double totalPadding = (eventsCount + 1) * padding;
      final double panelHeight = totalPadding +
          ((eventsCount - allDayEventCount) * _appointmentViewHeight) +
          (allDayEventCount * _allDayAppointmentHeight);
      double _appointmentViewTopPadding = 0;
      double _appointmntViewPadding = 0;
      if (panelHeight < _appointmentViewHeaderHeight) {
        _appointmntViewPadding = _appointmentViewHeaderHeight - panelHeight;
        _appointmentViewTopPadding = _appointmntViewPadding / 2;
      }

      final double _viewStartPosition = _currentIndex >= 0
          ? _previousHeight + _interSectPoint
          : -(_previousHeight + height - _interSectPoint);

      _interSectPoint += _appointmntViewPadding;

      /// Add appointment view to the current views collection.
      widgets.add(MouseRegion(
          onEnter: (PointerEnterEvent event) {
            _pointerEnterEvent(event, _isRTL, _currentDate, _viewStartPosition,
                _appointmentViewTopPadding);
          },
          onExit: _pointerExitEvent,
          onHover: (PointerHoverEvent event) {
            _pointerHoverEvent(event, _isRTL, _currentDate, _viewStartPosition,
                _appointmentViewTopPadding);
          },
          child: GestureDetector(
            child: _ScheduleAppointmentView(
                header: Container(
                    child: CustomPaint(
                        painter: _AgendaDateTimePainter(
                            _currentDate,
                            null,
                            widget.scheduleViewSettings,
                            widget.todayHighlightColor ??
                                _calendarTheme.todayHighlightColor,
                            _locale,
                            _calendarTheme,
                            _agendaDateNotifier,
                            _minWidth,
                            _isRTL),
                        size:
                            Size(_viewPadding, _appointmentViewHeaderHeight))),
                content: Container(
                  padding: EdgeInsets.fromLTRB(
                      _isRTL ? 0 : _viewPadding,
                      _appointmentViewTopPadding,
                      _isRTL ? _viewPadding : 0,
                      _appointmentViewTopPadding),
                  child: CustomPaint(
                      painter: _AgendaViewPainter(
                        null,
                        widget.scheduleViewSettings,
                        _currentDate,
                        currentAppointments,
                        _isRTL,
                        _locale,
                        _localizations,
                        _calendarTheme,
                        widget.appointmentTimeTextFormat,
                        _agendaViewNotifier,
                        _viewPadding,
                      ),
                      size: Size(_minWidth - _viewPadding, panelHeight)),
                )),
            onTapUp: (TapUpDetails details) {
              if (!_shouldRaiseCalendarTapCallback(widget.onTap)) {
                return;
              }

              _raiseCallbackForScheduleView(_currentDate, details.localPosition,
                  currentAppointments, _viewPadding, padding, true);
            },
            onLongPressStart: (LongPressStartDetails details) {
              if (!_shouldRaiseCalendarLongPressCallback(widget.onLongPress)) {
                return;
              }

              _raiseCallbackForScheduleView(_currentDate, details.localPosition,
                  currentAppointments, _viewPadding, padding, false);
            },
          )));

      _interSectPoint += panelHeight;

      /// Add divider at end of each of the week days in web view.
      if (_isWebView) {
        final Color _dividerColor =
            widget.cellBorderColor ?? _calendarTheme.cellBorderColor;
        widgets.add(Divider(
          height: 1,
          thickness: 1,
          color: _dividerColor.withOpacity(_dividerColor.opacity * 0.5),
        ));
        height += 1;
      }
    }

    /// Web view does not have month label.
    /// Add Month label at end of the view when the week start and end date
    /// month different and week does not have appointments or week have
    /// appointments but end date month dates does not have an appointment
    if (!_isWebView &&
        isNeedInBetweenMonthBuilder &&
        _isNextMonthHasNoAppointment &&
        isSameOrBeforeDate(_maxDate, endDate)) {
      /// Calculate and assign the intersection point because the current
      /// view holds next month label. if scrolling reaches this position
      /// then we update the header date so add the location to intersecting point.
      _scheduleViewDetails._intersectPoint = _currentIndex >= 0
          ? _previousHeight + _topHeight + appointmentHeight + _viewTopPadding
          : _previousHeight +
              height -
              _topHeight -
              appointmentHeight -
              _viewTopPadding;
      _topHeight += widget.scheduleViewSettings.monthHeaderSettings.height +
          _viewTopPadding;
      widgets.add(_getMonthOrWeekHeader(endDate, endDate, _isRTL, true,
          isNeedTopPadding: true));
    }

    /// Update the current view end position based previous view
    /// end position and current view height.
    _scheduleViewDetails._height = _previousHeight + height;
    if (_currentIndex >= 0) {
      _forwardWidgetHeights[_currentIndex] = _scheduleViewDetails;
    } else {
      _backwardWidgetHeights[-_currentIndex - 1] = _scheduleViewDetails;
    }

    return Container(height: height, child: Column(children: widgets));
  }

  Widget _getMonthOrWeekHeader(
      DateTime startDate, DateTime endDate, bool _isRTL, bool _isMonthLabel,
      {double viewPadding = 0, bool isNeedTopPadding = false}) {
    const double padding = 5;
    return GestureDetector(
        child: Container(
            padding: _isMonthLabel
                ? EdgeInsets.fromLTRB(0, isNeedTopPadding ? padding : 0, 0, 0)
                : EdgeInsets.fromLTRB(
                    _isRTL ? 0 : viewPadding,
                    isNeedTopPadding ? padding : 0,
                    _isRTL ? viewPadding : 0,
                    0),
            child: RepaintBoundary(
                child: CustomPaint(
              painter: _ScheduleLabelPainter(startDate, endDate,
                  widget.scheduleViewSettings, _isMonthLabel, _isRTL, _locale),
              size: _isMonthLabel
                  ? Size(_minWidth,
                      widget.scheduleViewSettings.monthHeaderSettings.height)
                  : Size(_minWidth - viewPadding - (2 * padding),
                      widget.scheduleViewSettings.weekHeaderSettings.height),
            ))),
        onTapUp: (TapUpDetails details) {
          if (!_shouldRaiseCalendarTapCallback(widget.onTap)) {
            return;
          }

          _raiseCalendarTapCallback(widget,
              date: DateTime(startDate.year, startDate.month, startDate.day),
              appointments: null,
              element: _isMonthLabel
                  ? CalendarElement.header
                  : CalendarElement.viewHeader);
        },
        onLongPressStart: (LongPressStartDetails details) {
          if (!_shouldRaiseCalendarLongPressCallback(widget.onLongPress)) {
            return;
          }

          _raiseCalendarLongPressCallback(widget,
              date: DateTime(startDate.year, startDate.month, startDate.day),
              appointments: null,
              element: _isMonthLabel
                  ? CalendarElement.header
                  : CalendarElement.viewHeader);
        });
  }

  void _raiseCallbackForScheduleView(
      DateTime selectedDate,
      Offset offset,
      List<Appointment> appointments,
      double viewHeaderWidth,
      double padding,
      bool isTapCallback) {
    /// Check the touch position on day label
    if (viewHeaderWidth >= offset.dx) {
      final List<Appointment> _currentAppointments = <Appointment>[];
      for (int i = 0; i < appointments.length; i++) {
        final Appointment _appointment = appointments[i];
        _currentAppointments.add(_appointment);
      }

      if (isTapCallback) {
        _raiseCalendarTapCallback(widget,
            date: DateTime(
                selectedDate.year, selectedDate.month, selectedDate.day),
            appointments: widget.dataSource != null &&
                    !_isCalendarAppointment(widget.dataSource.appointments)
                ? _getCustomAppointments(_currentAppointments)
                : _currentAppointments,
            element: CalendarElement.viewHeader);
      } else {
        _raiseCalendarLongPressCallback(widget,
            date: DateTime(
                selectedDate.year, selectedDate.month, selectedDate.day),
            appointments: widget.dataSource != null &&
                    !_isCalendarAppointment(widget.dataSource.appointments)
                ? _getCustomAppointments(_currentAppointments)
                : _currentAppointments,
            element: CalendarElement.viewHeader);
      }
    } else {
      /// Calculate the touch position appointment from its collection.
      double currentYPosition = padding;
      final double itemHeight =
          _getScheduleAppointmentHeight(null, widget.scheduleViewSettings);
      final double allDayItemHeight = _getScheduleAllDayAppointmentHeight(
          null, widget.scheduleViewSettings);
      for (int k = 0; k < appointments.length; k++) {
        final Appointment appointment = appointments[k];
        final double currentAppointmentHeight =
            (_isAllDayAppointmentView(appointment)
                    ? allDayItemHeight
                    : itemHeight) +
                padding;
        if (currentYPosition <= offset.dy &&
            currentYPosition + currentAppointmentHeight > offset.dy) {
          final List<Appointment> _selectedAppointment = <Appointment>[]
            ..add(appointment);
          if (isTapCallback) {
            _raiseCalendarTapCallback(widget,
                date: DateTime(
                    selectedDate.year, selectedDate.month, selectedDate.day),
                appointments: widget.dataSource != null &&
                        !_isCalendarAppointment(widget.dataSource.appointments)
                    ? _getCustomAppointments(_selectedAppointment)
                    : _selectedAppointment,
                element: CalendarElement.appointment);
          } else {
            _raiseCalendarLongPressCallback(widget,
                date: DateTime(
                    selectedDate.year, selectedDate.month, selectedDate.day),
                appointments: widget.dataSource != null &&
                        !_isCalendarAppointment(widget.dataSource.appointments)
                    ? _getCustomAppointments(_selectedAppointment)
                    : _selectedAppointment,
                element: CalendarElement.appointment);
          }
          break;
        }

        currentYPosition += currentAppointmentHeight;
      }
    }
  }

  Widget addAgenda(double _top, double _height, bool _isRTL) {
    final bool _hideEmptyAgendaDays =
        widget.scheduleViewSettings.hideEmptyScheduleWeek ||
            (kIsWeb && _minWidth > _kMobileViewWidth);

    /// return empty view when [hideEmptyAgendaDays] enabled and
    /// the appointments as empty.
    if (_hideEmptyAgendaDays &&
        (_appointments == null || _appointments.isEmpty)) {
      return Container();
    }

    /// Get the minimum date of schedule view when it value as null
    /// It return min date user assigned when the [hideEmptyAgendaDays]
    /// in [ScheduleViewSettings] disabled else it return min
    /// start date of the appointment collection.
    _minDate = _getMinAppointmentDate(_appointments, widget.timeZone,
        widget.minDate, widget.scheduleViewSettings, _minWidth);
    _minDate = _minDate.isBefore(widget.minDate) ? widget.minDate : _minDate;

    final DateTime _viewMinDate =
        _minDate.add(Duration(days: -_minDate.weekday));

    /// Get the maximum date of schedule view when it value as null
    /// It return max date user assigned when the [hideEmptyAgendaDays]
    /// in [ScheduleViewSettings] disabled else it return max
    /// end date of the appointment collection.
    _maxDate = _getMaxAppointmentDate(_appointments, widget.timeZone,
        widget.maxDate, widget.scheduleViewSettings, _minWidth);
    _maxDate = _maxDate.isAfter(widget.maxDate) ? widget.maxDate : _maxDate;

    final double _appointmentHeight =
        _getScheduleAppointmentHeight(null, widget.scheduleViewSettings);
    final double _allDayAppointmentHeight =
        _getScheduleAllDayAppointmentHeight(null, widget.scheduleViewSettings);

    /// Get the view first date based on specified
    /// display date  and first day of week.
    const int numberOfWeekDays = 7;
    int value = -_controller.displayDate.weekday +
        widget.firstDayOfWeek -
        numberOfWeekDays;
    if (value.abs() >= numberOfWeekDays) {
      value += numberOfWeekDays;
    }

    if (previousDates.isEmpty) {
      /// Calculate the start date from display date if next view dates
      /// collection as empty.
      DateTime date = nextDates.isNotEmpty
          ? nextDates[0]
          : _controller.displayDate.add(Duration(days: value));
      int count = 0;

      /// Using while for calculate dates because if [hideEmptyAgendaDays] as
      /// enabled, then it hides the weeks when it does not have appointments.
      while (count < 50) {
        for (int i = 1; i <= 100; i++) {
          final DateTime _date = addDuration(date, Duration(days: -i * 7));

          /// Skip week dates before min date
          if (!isSameOrAfterDate(_viewMinDate, _date)) {
            count = 50;
            break;
          }

          /// Skip the week date when it does not have appointments
          /// when [hideEmptyAgendaDays] as enabled.
          if (_hideEmptyAgendaDays &&
              !_isAppointmentBetweenDates(
                  _appointments,
                  _date,
                  addDuration(_date, const Duration(days: 6)),
                  widget.timeZone)) {
            continue;
          }

          bool _isSameDate = false;

          /// Check the date placed in next dates collection, when
          /// previous dates collection is empty.
          /// Eg., if [hideEmptyAgendaDays] property enabled but after the
          /// display date does not have a appointment then the previous
          /// dates collection initial dates added to next dates.
          if (previousDates.isEmpty) {
            for (int i = 0; i < nextDates.length; i++) {
              final DateTime date = nextDates[i];
              if (isSameDate(date, _currentDate)) {
                _isSameDate = true;
                break;
              }
            }
          }

          if (_isSameDate) {
            continue;
          }

          previousDates.add(_date);
          count++;
        }

        date = addDuration(date, const Duration(days: -700));
      }
    }

    if (nextDates.isEmpty) {
      /// Calculate the start date from display date
      DateTime date = _controller.displayDate.add(Duration(days: value));
      int count = 0;

      /// Using while for calculate dates because if [hideEmptyAgendaDays] as
      /// enabled, then it hides the weeks when it does not have appointments.
      while (count < 50) {
        for (int i = 0; i < 100; i++) {
          final DateTime _date = addDuration(date, Duration(days: i * 7));

          /// Skip week date after max date
          if (!isSameOrBeforeDate(_maxDate, _date)) {
            count = 50;
            break;
          }

          /// Skip the week date when it does not have appointments
          /// when [hideEmptyAgendaDays] as enabled.
          if (_hideEmptyAgendaDays &&
              !_isAppointmentBetweenDates(
                  _appointments,
                  _date,
                  addDuration(_date, const Duration(days: 6)),
                  widget.timeZone)) {
            continue;
          }

          nextDates.add(_date);
          count++;
        }

        date = addDuration(date, const Duration(days: 700));
      }
    }

    const double padding = 5;

    /// Calculate the next views dates when [hideEmptyAgendaDays] property
    /// enabled but after the display date does not have a appointment to the
    /// viewport then the previous dates collection initial dates added to next dates.
    if (nextDates.length < 10 && previousDates.isNotEmpty) {
      double totalHeight = 0;

      /// This boolean variable is used to check whether the previous dates
      /// collection dates added to next dates collection or not.
      bool _isNewDatesAdded = false;

      /// Add the previous view dates start date to next dates collection and
      /// remove the date from previous dates collection when next dates as empty.
      if (nextDates.isEmpty) {
        _isNewDatesAdded = true;
        nextDates.add(previousDates[0]);
        previousDates.removeAt(0);
      }

      /// Calculate the next dates collection appointments height to check
      /// the appointments fill the view port or not, if not then add another
      /// previous view dates and calculate the same until the next view dates
      /// appointment fills the view port.
      DateTime viewStartDate = nextDates[0];
      DateTime viewEndDate =
          addDuration(nextDates[nextDates.length - 1], const Duration(days: 6));
      List<Appointment> appointmentCollection = _getVisibleAppointments(
          viewStartDate,
          isSameOrBeforeDate(_maxDate, viewEndDate) ? viewEndDate : _maxDate,
          _appointments,
          widget.timeZone,
          false);

      Map<DateTime, List<Appointment>> dateAppointments =
          _getAppointmentCollectionOnDateBasis(
              appointmentCollection, viewStartDate, viewEndDate);
      List<DateTime> _dateAppointmentKeys = dateAppointments.keys.toList();

      double _labelHeight = 0;
      if (!kIsWeb || _minWidth < _kMobileViewWidth) {
        DateTime previousDate =
            addDuration(viewStartDate, const Duration(days: -1));
        for (int i = 0; i < nextDates.length; i++) {
          final DateTime _nextDate = nextDates[i];
          if (previousDate.month != _nextDate.month) {
            _labelHeight +=
                widget.scheduleViewSettings.monthHeaderSettings.height +
                    padding;
          }

          previousDate = _nextDate;
          _labelHeight += widget.scheduleViewSettings.weekHeaderSettings.height;
        }
      }

      int allDayCount = 0;
      int numberOfEvents = 0;
      for (int i = 0; i < _dateAppointmentKeys.length; i++) {
        final List<Appointment> _currentDateAppointment =
            dateAppointments[_dateAppointmentKeys[i]];
        if (!kIsWeb || (kIsWeb && _minWidth < _kMobileViewWidth)) {
          allDayCount += _getAllDayCount(_currentDateAppointment);
        }

        numberOfEvents += _currentDateAppointment.length;
      }

      /// Check the next dates collection appointments height fills the view port
      /// or not, if not then add another previous view dates and calculate
      /// the same until the next view dates appointments fills the view port.
      while (totalHeight < _height &&
          (previousDates.isNotEmpty || totalHeight == 0)) {
        /// Initially appointment height as 0 and check the existing dates
        /// appointment fills the view port or not. if not then add
        /// another previous view dates
        if (totalHeight != 0) {
          final DateTime _currentDate = previousDates[0];
          nextDates.insert(0, _currentDate);
          previousDates.removeAt(0);
          _isNewDatesAdded = true;

          viewStartDate = _currentDate;
          viewEndDate = addDuration(_currentDate, const Duration(days: 6));

          /// Calculate the newly added date appointment height and add
          /// the height to existing appointments height.
          appointmentCollection = _getVisibleAppointments(
              viewStartDate,
              isSameOrBeforeDate(_maxDate, viewEndDate)
                  ? viewEndDate
                  : _maxDate,
              _appointments,
              widget.timeZone,
              false);

          if (!kIsWeb || _minWidth < _kMobileViewWidth) {
            final DateTime nextDate = nextDates[1];
            if (nextDate.month != viewStartDate.month) {
              _labelHeight +=
                  widget.scheduleViewSettings.monthHeaderSettings.height +
                      padding;
            }

            _labelHeight +=
                widget.scheduleViewSettings.weekHeaderSettings.height;
          }

          dateAppointments = _getAppointmentCollectionOnDateBasis(
              appointmentCollection, viewStartDate, viewEndDate);
          _dateAppointmentKeys = dateAppointments.keys.toList();
          for (int i = 0; i < _dateAppointmentKeys.length; i++) {
            final List<Appointment> _currentDateAppointment =
                dateAppointments[_dateAppointmentKeys[i]];
            if (!kIsWeb || (kIsWeb && _minWidth < _kMobileViewWidth)) {
              allDayCount += _getAllDayCount(_currentDateAppointment);
            }

            numberOfEvents += _currentDateAppointment.length;
          }
        }

        totalHeight = ((numberOfEvents + 1) * padding) +
            ((numberOfEvents - allDayCount) * _appointmentHeight) +
            (allDayCount * _allDayAppointmentHeight) +
            _labelHeight;
      }

      /// Update the header date because the next dates insert the previous view
      /// dates at initial position.
      if (nextDates.isNotEmpty && _isNewDatesAdded) {
        _headerUpdateNotifier.value = nextDates[0];
      }
    }

    /// Return empty view when appointment not loaded.
    if (!_timeZoneLoaded && (_appointments == null || _appointments.isEmpty)) {
      return Container();
    }

    /// The below codes used to scroll the view to current display date.
    /// If display date as May 29, 2020 then its week day as friday but first
    /// day of week as sunday then May 23, 2020 as shown, calculate the
    /// in between space between the May 23 to May 28 and assign the value to
    /// scroll controller initial scroll position
    if (nextDates.isNotEmpty &&
        _scheduleViewController.initialScrollOffset == 0 &&
        !_scheduleViewController.hasClients) {
      final DateTime viewStartDate = nextDates[0];
      final DateTime viewEndDate =
          addDuration(viewStartDate, const Duration(days: 6));
      if (viewStartDate.isBefore(_controller.displayDate) &&
          !isSameDate(viewStartDate, _controller.displayDate) &&
          isSameOrBeforeDate(viewEndDate, _controller.displayDate)) {
        final DateTime viewEndDate =
            addDuration(_controller.displayDate, const Duration(days: -1));

        /// Calculate the appointment between the week start date and
        /// previous date of display date to calculate the scrolling position.
        final List<Appointment> appointmentCollection = _getVisibleAppointments(
            viewStartDate, viewEndDate, _appointments, widget.timeZone, false);

        /// Skip the scrolling when the previous week dates of display date
        /// does not have a appointment.
        if (appointmentCollection.isNotEmpty) {
          final Map<DateTime, List<Appointment>> dateAppointments =
              _getAppointmentCollectionOnDateBasis(
                  appointmentCollection, viewStartDate, viewEndDate);
          final List<DateTime> _dateAppointmentKeys =
              dateAppointments.keys.toList();
          double appointmentHeight = 0;
          for (int i = 0; i < _dateAppointmentKeys.length; i++) {
            final List<Appointment> _currentDateAppointment =
                dateAppointments[_dateAppointmentKeys[i]];
            final int eventsCount = _currentDateAppointment.length;
            int allDayEventCount = 0;

            /// Web view does not differentiate all day and normal appointment.
            if (!kIsWeb || (kIsWeb && _minWidth < _kMobileViewWidth)) {
              allDayEventCount = _getAllDayCount(_currentDateAppointment);
            }

            double panelHeight =
                ((eventsCount - allDayEventCount) * _appointmentHeight) +
                    (allDayEventCount * _allDayAppointmentHeight);
            panelHeight = panelHeight > _appointmentHeight
                ? panelHeight
                : _appointmentHeight;

            /// event count + 1 denotes the appointment padding and end padding.
            appointmentHeight += panelHeight + ((eventsCount + 1) * padding);
          }

          final double scrolledPosition = appointmentHeight +

              /// Add the divider height when it render on web.
              (kIsWeb && _minWidth > _kMobileViewWidth
                  ? _dateAppointmentKeys.length
                  : 0) +
              (kIsWeb && _minWidth > _kMobileViewWidth
                  ? 0
                  : widget.scheduleViewSettings.weekHeaderSettings.height) +
              (viewStartDate.month == _controller.displayDate.month &&
                      viewStartDate.day != 1
                  ? 0
                  : (kIsWeb && _minWidth > _kMobileViewWidth
                      ? 0
                      : widget.scheduleViewSettings.monthHeaderSettings.height +
                          padding));
          _scheduleViewController.removeListener(_handleScheduleViewScrolled);
          _scheduleViewController =
              ScrollController(initialScrollOffset: scrolledPosition)
                ..addListener(_handleScheduleViewScrolled);
        } else if (viewStartDate.month != _controller.displayDate.month &&
            (!kIsWeb || (kIsWeb && _minWidth < _kMobileViewWidth))) {
          _scheduleViewController.removeListener(_handleScheduleViewScrolled);
          _scheduleViewController = ScrollController(
              initialScrollOffset:
                  widget.scheduleViewSettings.weekHeaderSettings.height +
                      padding)
            ..addListener(_handleScheduleViewScrolled);
        }
      }
    }

    return Stack(children: <Widget>[
      Positioned(
          top: 0,
          right: 0,
          left: 0,
          height: widget.headerHeight,
          child: GestureDetector(
            child: Container(
                color: widget.headerStyle.backgroundColor ??
                    _calendarTheme.headerBackgroundColor,
                child: MouseRegion(
                  onEnter: _pointerEnterEvent,
                  onExit: _pointerExitEvent,
                  onHover: _pointerHoverEvent,
                  child: CustomPaint(
                    // Returns the header view  as a child for the calendar.
                    painter: _HeaderViewPainter(
                        _currentViewVisibleDates,
                        widget.headerStyle,
                        null,
                        widget.view,
                        widget.monthViewSettings.numberOfWeeksInView,
                        _calendarTheme,
                        _isRTL,
                        _locale,
                        _headerUpdateNotifier,
                        _headerHoverNotifier.value),
                  ),
                )),
            onTapUp: (TapUpDetails details) {
              _handleOnTapForHeader();
            },
            onLongPressStart: (LongPressStartDetails details) {
              _handleOnLongPressForHeader();
            },
          )),
      Positioned(
          top: widget.headerHeight,
          left: 0,
          right: 0,
          height: _height,
          child: CustomScrollView(
            key: _scrollKey,
            physics: const AlwaysScrollableScrollPhysics(),
            controller: _scheduleViewController,
            center: _scheduleViewKey,
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  if (previousDates.length <= index) {
                    return null;
                  }

                  /// Send negative index value to differentiate the
                  /// backward view from forward view.
                  return _getItem(context, -(index + 1), _isRTL);
                }),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  if (nextDates.length <= index) {
                    return null;
                  }

                  return _getItem(context, index, _isRTL);
                }),
                key: _scheduleViewKey,
              ),
            ],
          )),
    ]);
  }

  Widget _addChildren(double _top, double agendaHeight, double _height,
      double _width, bool _isRTL) {
    final DateTime _currentViewDate = _currentViewVisibleDates[
        (_currentViewVisibleDates.length / 2).truncate()];
    return Stack(children: <Widget>[
      Positioned(
        top: 0,
        right: 0,
        left: 0,
        height: widget.headerHeight,
        child: Container(
            color: widget.headerStyle.backgroundColor ??
                _calendarTheme.headerBackgroundColor,
            child: _CalendarHeaderView(
                _currentViewVisibleDates,
                widget.headerStyle,
                _currentViewDate,
                widget.view,
                widget.monthViewSettings.numberOfWeeksInView,
                _calendarTheme,
                _isRTL,
                _locale,
                widget.showNavigationArrow,
                _controller,
                widget.maxDate,
                widget.minDate,
                _width,
                widget.headerHeight,
                widget.timeSlotViewSettings.nonWorkingDays,
                widget.monthViewSettings.navigationDirection,
                _handleOnTapForHeader,
                _handleOnLongPressForHeader)),
      ),
      Positioned(
        top: _top,
        left: 0,
        right: 0,
        height: _height - agendaHeight,
        child: _CustomScrollView(
          widget,
          _width,
          _height - agendaHeight,
          _visibleAppointments,
          _agendaSelectedDate,
          _isRTL,
          _locale,
          _calendarTheme,
          _timeZoneLoaded ? widget.specialRegions : null,
          _controller,
          updateCalendarState: (_UpdateCalendarStateDetails details) {
            _updateCalendarState(details);
          },
        ),
      ),
      _addAgendaView(
          agendaHeight, _top + _height - agendaHeight, _width, _isRTL),
    ]);
  }

  dynamic _updateCalendarState(_UpdateCalendarStateDetails details) {
    if (details._currentDate != null) {
      _currentDate = details._currentDate;
      _controller.displayDate = details._currentDate;
    }

    details._currentDate = details._currentDate ?? _currentDate;

    if (details._currentViewVisibleDates != null &&
        _currentViewVisibleDates != details._currentViewVisibleDates) {
      _currentViewVisibleDates = details._currentViewVisibleDates;
      _updateVisibleAppointments();
      if (_shouldRaiseViewChangedCallback(widget.onViewChanged)) {
        _raiseViewChangedCallback(widget,
            visibleDates: _currentViewVisibleDates);
      }
    }

    if (details._selectedDate == null) {
      details._selectedDate = _selectedDate;
    } else if (details._selectedDate != _selectedDate) {
      _selectedDate = details._selectedDate;
      _controller.selectedDate = details._selectedDate;
    }

    if (details._allDayPanelHeight == null ||
        details._allDayPanelHeight != _allDayPanelHeight) {
      details._allDayPanelHeight = _allDayPanelHeight;
    }

    if (details._allDayAppointmentViewCollection == null ||
        details._allDayAppointmentViewCollection !=
            _allDayAppointmentViewCollection) {
      details._allDayAppointmentViewCollection =
          _allDayAppointmentViewCollection;
    }

    if (details._appointments == null ||
        details._appointments != _appointments) {
      details._appointments = _appointments;
    }
  }

  //// Handles the on tap callback for  header
  void _handleOnTapForHeader() {
    if (!_shouldRaiseCalendarTapCallback(widget.onTap)) {
      return;
    }

    _raiseCalendarTapCallback(widget,
        date: _getTappedHeaderDate(), element: CalendarElement.header);
  }

  //// Handles the on long press callback for  header
  void _handleOnLongPressForHeader() {
    if (!_shouldRaiseCalendarLongPressCallback(widget.onLongPress)) {
      return;
    }

    _raiseCalendarLongPressCallback(widget,
        date: _getTappedHeaderDate(), element: CalendarElement.header);
  }

  DateTime _getTappedHeaderDate() {
    if (widget.view == CalendarView.month) {
      return DateTime(_currentDate.year, _currentDate.month, 01, 0, 0, 0);
    } else {
      return DateTime(
          _currentViewVisibleDates[0].year,
          _currentViewVisibleDates[0].month,
          _currentViewVisibleDates[0].day,
          0,
          0,
          0);
    }
  }

  //// Handles the onTap callback for agenda view.
  void _handleTapForAgenda(TapUpDetails details) {
    if (!_shouldRaiseCalendarTapCallback(widget.onTap)) {
      return;
    }

    final List<dynamic> _selectedAppointments =
        _getSelectedAppointments(details);

    _raiseCalendarTapCallback(widget,
        date: _selectedDate,
        appointments: _selectedAppointments,
        element:
            _selectedAppointments != null && _selectedAppointments.isNotEmpty
                ? CalendarElement.appointment
                : CalendarElement.agenda);
  }

  //// Handles the onLongPress callback for agenda view.
  void _handleLongPressForAgenda(LongPressStartDetails details) {
    if (!_shouldRaiseCalendarLongPressCallback(widget.onLongPress)) {
      return;
    }

    final List<dynamic> _selectedAppointments =
        _getSelectedAppointments(details);

    _raiseCalendarLongPressCallback(widget,
        date: _selectedDate,
        appointments: _selectedAppointments,
        element:
            _selectedAppointments != null && _selectedAppointments.isNotEmpty
                ? CalendarElement.appointment
                : CalendarElement.agenda);
  }

  List<dynamic> _getSelectedAppointments(dynamic details) {
    List<Appointment> _agendaAppointments;
    if (_selectedDate != null) {
      _agendaAppointments = _getSelectedDateAppointments(
          _appointments, widget.timeZone, _selectedDate);

      final double dateTimeWidth = _minWidth * 0.15;
      if (details.localPosition.dx > dateTimeWidth &&
          _agendaAppointments != null &&
          _agendaAppointments.isNotEmpty) {
        _agendaAppointments.sort((dynamic app1, dynamic app2) =>
            app1._actualStartTime.compareTo(app2._actualStartTime));
        _agendaAppointments.sort((dynamic app1, dynamic app2) =>
            _orderAppointmentsAscending(app1.isAllDay, app2.isAllDay));

        int index = -1;
        //// Agenda appointment view top padding as 5.
        const double padding = 5;
        double xPosition = 0;
        final double tappedYPosition =
            _agendaScrollController.offset + details.localPosition.dy;
        final double actualAppointmentHeight =
            _getScheduleAppointmentHeight(widget.monthViewSettings, null);
        final double allDayAppointmentHeight =
            _getScheduleAllDayAppointmentHeight(widget.monthViewSettings, null);
        for (int i = 0; i < _agendaAppointments.length; i++) {
          final Appointment _appointment = _agendaAppointments[i];
          final double appointmentHeight =
              _isAllDayAppointmentView(_appointment)
                  ? allDayAppointmentHeight
                  : actualAppointmentHeight;
          if (tappedYPosition >= xPosition &&
              tappedYPosition < xPosition + appointmentHeight + padding) {
            index = i;
            break;
          }

          xPosition += appointmentHeight + padding;
        }

        if (index > _agendaAppointments.length || index == -1) {
          _agendaAppointments = null;
        } else {
          _agendaAppointments = <Appointment>[_agendaAppointments[index]];
        }
      } else {
        _agendaAppointments = null;
      }
    }

    List<Object> _selectedAppointments = <Object>[];
    if (_agendaAppointments != null && _agendaAppointments.isNotEmpty) {
      for (int i = 0; i < _agendaAppointments.length; i++) {
        _selectedAppointments.add(_agendaAppointments[i]);
      }
    }

    if (widget.dataSource != null &&
        !_isCalendarAppointment(widget.dataSource.appointments)) {
      _selectedAppointments = _getCustomAppointments(_agendaAppointments);
    }

    return _selectedAppointments;
  }

  // Returns the agenda view  as a child for the calendar.
  Widget _addAgendaView(
      double height, double startPosition, double _width, bool _isRTL) {
    if (widget.view != CalendarView.month ||
        !widget.monthViewSettings.showAgenda) {
      return Positioned(
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        child: Container(),
      );
    }

    List<Appointment> _agendaAppointments;
    if (_selectedDate != null) {
      _agendaAppointments = _getSelectedDateAppointments(
          _appointments, widget.timeZone, _selectedDate);
    }

    const double topPadding = 5;
    const double bottomPadding = 5;
    final double appointmentHeight =
        _getScheduleAppointmentHeight(widget.monthViewSettings, null);
    final double allDayAppointmentHeight =
        _getScheduleAllDayAppointmentHeight(widget.monthViewSettings, null);
    double painterHeight = height;
    if (_agendaAppointments != null && _agendaAppointments.isNotEmpty) {
      final int count = _getAllDayCount(_agendaAppointments);
      painterHeight = (((count * (allDayAppointmentHeight + topPadding)) +
                  ((_agendaAppointments.length - count) *
                      (appointmentHeight + topPadding)))
              .toDouble()) +
          bottomPadding;
    }

    double dateTimeWidth = _width * 0.15;
    if (_selectedDate == null) {
      dateTimeWidth = 0;
    }

    if (dateTimeWidth > 60 && kIsWeb) {
      dateTimeWidth = 60;
    }
    return Positioned(
        top: startPosition,
        right: 0,
        left: 0,
        height: height,
        child: Container(
            color: widget.monthViewSettings.agendaStyle.backgroundColor ??
                _calendarTheme.agendaBackgroundColor,
            child: MouseRegion(
                onEnter: (PointerEnterEvent event) {
                  _pointerEnterEvent(event, _isRTL);
                },
                onExit: _pointerExitEvent,
                onHover: (PointerHoverEvent event) {
                  _pointerHoverEvent(event, _isRTL);
                },
                child: GestureDetector(
                  child: Stack(
                    children: <Widget>[
                      CustomPaint(
                        painter: _AgendaDateTimePainter(
                            _selectedDate,
                            widget.monthViewSettings,
                            null,
                            widget.todayHighlightColor ??
                                _calendarTheme.todayHighlightColor,
                            _locale,
                            _calendarTheme,
                            _agendaDateNotifier,
                            _minWidth,
                            _isRTL),
                        size: Size(dateTimeWidth, height),
                      ),
                      Positioned(
                        top: 0,
                        left: _isRTL ? 0 : dateTimeWidth,
                        right: _isRTL ? dateTimeWidth : 0,
                        bottom: 0,
                        child: ListView(
                          padding: const EdgeInsets.all(0.0),
                          controller: _agendaScrollController,
                          children: <Widget>[
                            CustomPaint(
                              painter: _AgendaViewPainter(
                                  widget.monthViewSettings,
                                  null,
                                  _selectedDate,
                                  _agendaAppointments,
                                  _isRTL,
                                  _locale,
                                  _localizations,
                                  _calendarTheme,
                                  widget.appointmentTimeTextFormat,
                                  _agendaViewNotifier,
                                  dateTimeWidth),
                              size: Size(_width - dateTimeWidth, painterHeight),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  onTapUp: (TapUpDetails details) {
                    _handleTapForAgenda(details);
                  },
                  onLongPressStart: (LongPressStartDetails details) {
                    _handleLongPressForAgenda(details);
                  },
                ))));
  }
}
