part of calendar;

class _AgendaViewPainter extends CustomPainter {
  _AgendaViewPainter(
      this._monthViewSettings,
      this._scheduleViewSettings,
      this._selectedDate,
      this._appointments,
      this._isRTL,
      this._locale,
      this._localizations,
      this._calendarTheme,
      this.appointmentTimeTextFormat,
      this._agendaViewNotifier,
      this._timeLabelWidth)
      : super(repaint: _agendaViewNotifier);

  final MonthViewSettings _monthViewSettings;
  final ScheduleViewSettings _scheduleViewSettings;
  final DateTime _selectedDate;
  final List<Appointment> _appointments;
  final bool _isRTL;
  final String _locale;
  final SfCalendarThemeData _calendarTheme;
  final String appointmentTimeTextFormat;
  final ValueNotifier<_ScheduleViewHoveringDetails> _agendaViewNotifier;
  Paint _rectPainter;
  TextPainter _textPainter;
  final SfLocalizations _localizations;
  final double _timeLabelWidth;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    _rectPainter = _rectPainter ?? Paint();
    _rectPainter.isAntiAlias = true;
    double yPosition = 5;
    double xPosition = 5;
    const double padding = 5;

    if (_selectedDate == null ||
        _appointments == null ||
        _appointments.isEmpty) {
      final TextSpan span = TextSpan(
        text: _selectedDate == null
            ? _localizations.noSelectedDateCalendarLabel
            : _localizations.noEventsCalendarLabel,
        style: const TextStyle(
            color: Colors.grey, fontSize: 15, fontFamily: 'Roboto'),
      );
      _textPainter = _textPainter ?? TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textAlign = TextAlign.left;
      _textPainter.textWidthBasis = TextWidthBasis.longestLine;

      _textPainter.layout(minWidth: 0, maxWidth: size.width - 10);
      if (_isRTL) {
        xPosition = size.width - _textPainter.width;
      }
      _textPainter.paint(canvas, Offset(xPosition, yPosition + padding));
      return;
    }

    final bool _isScheduleWebUI = _scheduleViewSettings != null &&
            (kIsWeb && (size.width + _timeLabelWidth) > _kMobileViewWidth)
        ? true
        : false;

    _appointments.sort((Appointment app1, Appointment app2) =>
        app1._actualStartTime.compareTo(app2._actualStartTime));
    _appointments.sort((Appointment app1, Appointment app2) =>
        _orderAppointmentsAscending(app1.isAllDay, app2.isAllDay));
    _appointments.sort((Appointment app1, Appointment app2) =>
        _orderAppointmentsAscending(app1._isSpanned, app2._isSpanned));
    final TextStyle appointmentTextStyle = _monthViewSettings != null
        ? _monthViewSettings.agendaStyle.appointmentTextStyle ??
            const TextStyle(
                color: Colors.white, fontSize: 13, fontFamily: 'Roboto')
        : _scheduleViewSettings.appointmentTextStyle ??
            TextStyle(
                color: _isScheduleWebUI &&
                        _calendarTheme.brightness == Brightness.light
                    ? Colors.black87
                    : Colors.white,
                fontSize: 13,
                fontFamily: 'Roboto');
    final double agendaItemHeight = _getScheduleAppointmentHeight(
        _monthViewSettings, _scheduleViewSettings);
    final double agendaAllDayItemHeight = _getScheduleAllDayAppointmentHeight(
        _monthViewSettings, _scheduleViewSettings);
    final double _centerYPosition = agendaItemHeight / 2;
    final double circleRadius =
        _centerYPosition > padding ? padding : _centerYPosition - 2;

    //// Draw Appointments
    for (int i = 0; i < _appointments.length; i++) {
      final Appointment _appointment = _appointments[i];
      xPosition = 5;
      _rectPainter.color = _appointment.color;
      final bool _isSpanned = _appointment._actualEndTime.day !=
              _appointment._actualStartTime.day ||
          _appointment._isSpanned;
      final double appointmentHeight =
          (_appointment.isAllDay || _isSpanned) && !_isScheduleWebUI
              ? agendaAllDayItemHeight
              : agendaItemHeight;
      final Rect rect = Rect.fromLTWH(xPosition, yPosition,
          size.width - xPosition - padding, appointmentHeight);
      final Radius cornerRadius = Radius.circular(
          (appointmentHeight * 0.1) > 5 ? 5 : (appointmentHeight * 0.1));

      /// Web view does not highlighted by background
      if (!_isScheduleWebUI) {
        canvas.drawRRect(
            RRect.fromRectAndRadius(rect, cornerRadius), _rectPainter);
      }

      TextSpan span =
          TextSpan(text: _appointment.subject, style: appointmentTextStyle);
      _textPainter ??= TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textAlign = TextAlign.left;
      _textPainter.textWidthBasis = TextWidthBasis.longestLine;
      double _timeWidth =
          _isScheduleWebUI ? (size.width - (2 * padding)) * 0.3 : 0;
      _timeWidth = _timeWidth > 200 ? 200 : _timeWidth;
      xPosition += _timeWidth;

      /// Draw web schedule view.
      if (_isScheduleWebUI) {
        final double _circleStartPosition = 3 * circleRadius;
        canvas.drawCircle(
            Offset(
                _isRTL
                    ? size.width - _circleStartPosition
                    : _circleStartPosition,
                yPosition + _centerYPosition),
            circleRadius,
            _rectPainter);
        final double _circleWidth = 5 * circleRadius;
        xPosition += _circleWidth;
        _textPainter.layout(
            minWidth: 0, maxWidth: size.width - (2 * padding) - xPosition);

        if (_isRTL) {
          xPosition = size.width -
              _textPainter.width -
              (3 * padding) -
              _timeWidth -
              _circleWidth;
        }

        _textPainter.paint(
            canvas,
            Offset(xPosition + padding,
                yPosition + ((appointmentHeight - _textPainter.height) / 2)));
        final DateFormat format =
            DateFormat(appointmentTimeTextFormat ?? 'hh:mm a', _locale);
        span = TextSpan(
            text: _appointment.isAllDay || _appointment._isSpanned
                ? 'All Day'
                : format.format(_appointment._actualStartTime) +
                    ' - ' +
                    format.format(_appointment._actualEndTime),
            style: appointmentTextStyle);
        _textPainter.text = span;
        _textPainter.layout(minWidth: 0, maxWidth: _timeWidth - padding);
        xPosition = padding + _circleWidth;
        if (_isRTL) {
          xPosition =
              size.width - _textPainter.width - (3 * padding) - _circleWidth;
        }
        _textPainter.paint(
            canvas,
            Offset(xPosition + padding,
                yPosition + ((appointmentHeight - _textPainter.height) / 2)));
      } else {
        /// Draws spanning appointment UI for schedule view.
        if (_isSpanned) {
          final String totalDays = (_convertToEndTime(
                          _appointment._actualEndTime)
                      .difference(
                          _convertToStartTime(_appointment._actualStartTime))
                      .inDays +
                  1)
              .toString();
          final String currentDate = (_convertToEndTime(_selectedDate)
                      .difference(
                          _convertToStartTime(_appointment._actualStartTime))
                      .inDays +
                  1)
              .toString();

          span = TextSpan(
              text: _appointment.subject +
                  ' ( Day ' +
                  currentDate +
                  ' / ' +
                  totalDays +
                  ')',
              style: appointmentTextStyle);

          _textPainter.text = span;
          _textPainter.maxLines = 1;
          _textPainter.textDirection = TextDirection.ltr;
          _textPainter.textAlign = TextAlign.left;
          _textPainter.textWidthBasis = TextWidthBasis.longestLine;

          _textPainter.layout(minWidth: 0, maxWidth: size.width - 10 - padding);
          if (_isRTL) {
            xPosition = size.width - _textPainter.width - (padding * 3);
          }
          _textPainter.paint(
              canvas,
              Offset(xPosition + padding,
                  yPosition + ((appointmentHeight - _textPainter.height) / 2)));
        }
        //// Draw Appointments except All day appointment
        else if (!_appointment.isAllDay) {
          _textPainter.layout(
              minWidth: 0, maxWidth: size.width - (2 * padding) - xPosition);

          if (_isRTL) {
            xPosition =
                size.width - _textPainter.width - (3 * padding) - _timeWidth;
          }

          _textPainter.paint(
              canvas,
              Offset(xPosition + padding,
                  yPosition + (appointmentHeight / 2) - _textPainter.height));

          final String format = appointmentTimeTextFormat ??
              (isSameDate(_appointment._actualStartTime,
                      _appointment._actualEndTime)
                  ? 'hh:mm a'
                  : 'MMM dd, hh:mm a');
          span = TextSpan(
              text: DateFormat(format, _locale)
                      .format(_appointment._actualStartTime) +
                  ' - ' +
                  DateFormat(format, _locale)
                      .format(_appointment._actualEndTime),
              style: appointmentTextStyle);
          _textPainter.text = span;

          _textPainter.layout(
              minWidth: 0, maxWidth: size.width - (2 * padding) - padding);
          if (_isRTL) {
            xPosition = size.width - _textPainter.width - (3 * padding);
          }
          _textPainter.paint(canvas,
              Offset(xPosition + padding, yPosition + (appointmentHeight / 2)));
        } else {
          //// Draw All day appointment
          _textPainter.layout(minWidth: 0, maxWidth: size.width - 10 - padding);
          if (_isRTL) {
            xPosition = size.width - _textPainter.width - (padding * 3);
          }
          _textPainter.paint(
              canvas,
              Offset(xPosition + 5,
                  yPosition + ((appointmentHeight - _textPainter.height) / 2)));
        }
      }

      if (_appointment.recurrenceRule != null &&
          _appointment.recurrenceRule.isNotEmpty) {
        // The default font size if none is specified.
        // The value taken from framework, for text style when there is no font
        // size given they have used 14 as the default font size.
        const double _defaultFontSize = 14;
        double textSize = kIsWeb
            ? appointmentTextStyle.fontSize != null
                ? appointmentTextStyle.fontSize * 1.5
                : _defaultFontSize * 1.5
            : appointmentTextStyle.fontSize ?? _defaultFontSize;
        if (rect.width < textSize || rect.height < textSize) {
          textSize = rect.width > rect.height ? rect.height : rect.width;
        }

        final TextSpan icon =
            _getRecurrenceIcon(appointmentTextStyle.color, textSize);
        _textPainter.text = icon;
        _textPainter.layout(
            minWidth: 0, maxWidth: size.width - (2 * padding) - padding);
        if (!_isScheduleWebUI) {
          if (_isRTL) {
            canvas.drawRRect(
                RRect.fromRectAndRadius(
                    Rect.fromLTRB(8, rect.top, rect.left, rect.bottom),
                    cornerRadius),
                _rectPainter);
          } else {
            canvas.drawRRect(
                RRect.fromRectAndRadius(
                    Rect.fromLTRB(rect.right - textSize - 8, rect.top,
                        rect.right, rect.bottom),
                    cornerRadius),
                _rectPainter);
          }
        }

        // Value 8 added as a right side padding for the recurrence icon in the agenda view
        if (_isRTL) {
          _textPainter.paint(canvas,
              Offset(8, rect.top + (rect.height - _textPainter.height) / 2));
        } else {
          _textPainter.paint(
              canvas,
              Offset(rect.right - textSize - 8,
                  rect.top + (rect.height - _textPainter.height) / 2));
        }
      }

      if (_agendaViewNotifier.value != null &&
          isSameDate(_agendaViewNotifier.value._hoveringDate, _selectedDate)) {
        _rectPainter ??= Paint();
        if (rect.left < _agendaViewNotifier.value._hoveringOffset.dx &&
            rect.right > _agendaViewNotifier.value._hoveringOffset.dx &&
            rect.top < _agendaViewNotifier.value._hoveringOffset.dy &&
            rect.bottom > _agendaViewNotifier.value._hoveringOffset.dy) {
          if (_isScheduleWebUI) {
            _rectPainter.color = Colors.grey.withOpacity(0.1);
            canvas.drawRRect(
                RRect.fromRectAndRadius(
                    Rect.fromLTWH(0, rect.top + padding, size.width - 2,
                        rect.height - (2 * padding)),
                    Radius.circular(rect.height / 2)),
                _rectPainter);
          } else {
            _rectPainter.color =
                _calendarTheme.selectionBorderColor.withOpacity(0.4);
            _rectPainter.style = PaintingStyle.stroke;
            _rectPainter.strokeWidth = 2;
            canvas.drawRect(rect, _rectPainter);
            _rectPainter.style = PaintingStyle.fill;
          }
        }
      }

      yPosition += appointmentHeight + padding;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      return _getSemanticsBuilder(size);
    };
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    return true;
  }

  List<CustomPainterSemantics> _getSemanticsBuilder(Size size) {
    final List<CustomPainterSemantics> _semanticsBuilder =
        <CustomPainterSemantics>[];
    double _height;
    const double _left = 5.0;
    double _top = 5.0;
    const double _padding = 5.0;
    final double agendaItemHeight = _getScheduleAppointmentHeight(
        _monthViewSettings, _scheduleViewSettings);
    final double agendaAllDayItemHeight = _getScheduleAllDayAppointmentHeight(
        _monthViewSettings, _scheduleViewSettings);
    if (_selectedDate == null) {
      _semanticsBuilder.add(CustomPainterSemantics(
        rect: Offset.zero & size,
        properties: const SemanticsProperties(
          label: 'No selected date',
          textDirection: TextDirection.ltr,
        ),
      ));
    } else if (_selectedDate != null &&
        (_appointments == null || _appointments.isEmpty)) {
      _semanticsBuilder.add(CustomPainterSemantics(
        rect: Offset.zero & size,
        properties: SemanticsProperties(
          label: DateFormat('EEEEE').format(_selectedDate).toString() +
              DateFormat('dd/MMMM/yyyy').format(_selectedDate).toString() +
              ', '
                  'No events',
          textDirection: TextDirection.ltr,
        ),
      ));
    } else if (_selectedDate != null &&
        _appointments != null &&
        _appointments.isNotEmpty) {
      final bool _isScheduleWebUI =
          _scheduleViewSettings != null && kIsWeb ? true : false;
      for (int i = 0; i < _appointments.length; i++) {
        final Appointment _currentAppointment = _appointments[i];
        _height = (_currentAppointment.isAllDay ||
                    _currentAppointment._actualEndTime.day !=
                        _currentAppointment._actualStartTime.day ||
                    _currentAppointment._isSpanned) &&
                !_isScheduleWebUI
            ? agendaAllDayItemHeight
            : agendaItemHeight;
        _semanticsBuilder.add(CustomPainterSemantics(
          rect: Rect.fromLTWH(
              _left, _top, size.width - _left - _padding, _height),
          properties: SemanticsProperties(
            label: _getAppointmentText(_currentAppointment),
            textDirection: TextDirection.ltr,
          ),
        ));
        _top += _height + _padding;
      }
    }

    return _semanticsBuilder;
  }
}

class _AgendaDateTimePainter extends CustomPainter {
  _AgendaDateTimePainter(
      this._selectedDate,
      this._monthViewSettings,
      this._scheduleViewSettings,
      this._todayHighlightColor,
      this._locale,
      this._calendarTheme,
      this._agendaDateNotifier,
      this._viewWidth,
      this._isRTL)
      : super(repaint: _agendaDateNotifier);

  final DateTime _selectedDate;
  final MonthViewSettings _monthViewSettings;
  final ScheduleViewSettings _scheduleViewSettings;
  final Color _todayHighlightColor;
  final String _locale;
  final ValueNotifier<_ScheduleViewHoveringDetails> _agendaDateNotifier;
  Paint _linePainter;
  final SfCalendarThemeData _calendarTheme;
  TextPainter _textPainter;
  final double _viewWidth;
  final bool _isRTL;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    _linePainter = _linePainter ?? Paint();
    _linePainter.isAntiAlias = true;
    const double padding = 5;
    if (_selectedDate == null) {
      return;
    }

    final bool isToday = isSameDate(_selectedDate, DateTime.now());
    TextStyle dateTextStyle, dayTextStyle;
    if (_monthViewSettings != null) {
      dayTextStyle = _monthViewSettings.agendaStyle.dayTextStyle ??
          _calendarTheme.agendaDayTextStyle;
      dateTextStyle = _monthViewSettings.agendaStyle.dateTextStyle ??
          _calendarTheme.agendaDateTextStyle;
    } else {
      dayTextStyle = _scheduleViewSettings.dayHeaderSettings.dayTextStyle ??
          ((kIsWeb && _viewWidth > _kMobileViewWidth)
              ? TextStyle(
                  color: _calendarTheme.agendaDayTextStyle.color,
                  fontSize: 13,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.normal)
              : _calendarTheme.agendaDayTextStyle);
      dateTextStyle = _scheduleViewSettings.dayHeaderSettings.dateTextStyle ??
          ((kIsWeb && _viewWidth > _kMobileViewWidth)
              ? TextStyle(
                  color: _calendarTheme.agendaDateTextStyle.color,
                  fontSize: 15,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.normal)
              : _calendarTheme.agendaDateTextStyle);
    }

    final Color selectedDayTextColor = isToday
        ? _todayHighlightColor ?? _calendarTheme.todayHighlightColor
        : dayTextStyle != null
            ? dayTextStyle.color
            : _calendarTheme.agendaDayTextStyle.color;
    final Color selectedDateTextColor = isToday
        ? _calendarTheme.todayTextStyle.color
        : dateTextStyle != null
            ? dateTextStyle.color
            : _calendarTheme.agendaDateTextStyle.color;
    dayTextStyle = dayTextStyle.copyWith(color: selectedDayTextColor);
    dateTextStyle = dateTextStyle.copyWith(color: selectedDateTextColor);

    /// Draw day label other than web schedule view.
    if (_scheduleViewSettings == null ||
        (!kIsWeb || (kIsWeb && _viewWidth < _kMobileViewWidth))) {
      //// Draw Weekday
      final String dayTextFormat = _scheduleViewSettings != null
          ? _scheduleViewSettings.dayHeaderSettings.dayFormat
          : 'EEE';
      TextSpan span = TextSpan(
          text: DateFormat(dayTextFormat, _locale)
              .format(_selectedDate)
              .toUpperCase()
              .toString(),
          style: dayTextStyle);
      _textPainter = _textPainter ?? TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textAlign = TextAlign.left;
      _textPainter.textWidthBasis = TextWidthBasis.parent;

      _textPainter.layout(minWidth: 0, maxWidth: size.width);
      _textPainter.paint(
          canvas,
          Offset(
              padding + ((size.width - (2 * padding) - _textPainter.width) / 2),
              padding));

      final double weekDayHeight = padding + _textPainter.height;
      //// Draw Date
      span = TextSpan(text: _selectedDate.day.toString(), style: dateTextStyle);
      _textPainter = _textPainter ?? TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textAlign = TextAlign.left;
      _textPainter.textWidthBasis = TextWidthBasis.parent;

      _textPainter.layout(minWidth: 0, maxWidth: size.width);
      final double xPosition =
          padding + ((size.width - (2 * padding) - _textPainter.width) / 2);
      double yPosition = weekDayHeight;
      if (isToday) {
        yPosition = weekDayHeight + padding;
        _linePainter.color = _todayHighlightColor;
        _drawTodayCircle(canvas, xPosition, yPosition, padding);
      }

      /// padding added between date and day labels in web, to avoid the hovering
      /// effect overlapping issue.
      if (kIsWeb && !isToday) {
        yPosition = weekDayHeight + padding;
      }
      if (_agendaDateNotifier.value != null &&
          isSameDate(_agendaDateNotifier.value._hoveringDate, _selectedDate)) {
        if (xPosition < _agendaDateNotifier.value._hoveringOffset.dx &&
            xPosition + _textPainter.width >
                _agendaDateNotifier.value._hoveringOffset.dx &&
            yPosition < _agendaDateNotifier.value._hoveringOffset.dy &&
            yPosition + _textPainter.height >
                _agendaDateNotifier.value._hoveringOffset.dy) {
          _linePainter.color = isToday
              ? Colors.black.withOpacity(0.1)
              : Colors.grey.withOpacity(0.3);
          _drawTodayCircle(canvas, xPosition, yPosition, padding);
        }
      }

      _textPainter.paint(canvas, Offset(xPosition, yPosition));
    } else {
      /// Draw day label on web schedule view.
      final String dateText = _selectedDate.day.toString();
      final String dayText = DateFormat(
              _isRTL
                  ? _scheduleViewSettings.dayHeaderSettings.dayFormat + ', MMM'
                  : 'MMM, ' + _scheduleViewSettings.dayHeaderSettings.dayFormat,
              _locale)
          .format(_selectedDate)
          .toUpperCase()
          .toString();

      //// Draw Weekday
      TextSpan span = TextSpan(text: dateText + dayText, style: dateTextStyle);
      _textPainter ??= TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textAlign = TextAlign.left;
      _textPainter.textWidthBasis = TextWidthBasis.parent;

      _textPainter.layout(minWidth: 0, maxWidth: size.width);
      double _startXPosition = (size.width - _textPainter.width) / 2;
      _startXPosition = _isRTL ? size.width - _startXPosition : _startXPosition;
      double _dateHeight = size.height;
      if (kIsWeb && _viewWidth > _kMobileViewWidth) {
        _dateHeight =
            _getScheduleAppointmentHeight(null, _scheduleViewSettings);
      }
      final double _yPosition = (_dateHeight - _textPainter.height) / 2;

      span = TextSpan(text: dateText, style: dateTextStyle);
      _textPainter.text = span;
      _textPainter.layout(minWidth: 0, maxWidth: size.width);
      if (isToday) {
        _linePainter.color = _todayHighlightColor;
        _drawTodayCircle(canvas, _startXPosition, _yPosition, padding);
      }

      if (_agendaDateNotifier.value != null &&
          isSameDate(_agendaDateNotifier.value._hoveringDate, _selectedDate)) {
        if (_startXPosition <
                (_isRTL
                    ? size.width - _agendaDateNotifier.value._hoveringOffset.dx
                    : _agendaDateNotifier.value._hoveringOffset.dx) &&
            (_startXPosition + _textPainter.width) >
                (_isRTL
                    ? size.width - _agendaDateNotifier.value._hoveringOffset.dx
                    : _agendaDateNotifier.value._hoveringOffset.dx) &&
            0 < _agendaDateNotifier.value._hoveringOffset.dy &&
            (_yPosition + _textPainter.height) >
                _agendaDateNotifier.value._hoveringOffset.dy) {
          _linePainter.color = isToday
              ? Colors.black.withOpacity(0.1)
              : Colors.grey.withOpacity(0.3);
          _drawTodayCircle(canvas, _startXPosition, _yPosition, padding);
        }
      }

      _textPainter.paint(canvas,
          Offset(_startXPosition, (_dateHeight - _textPainter.height) / 2));

      //// Draw Date
      final double painterWidth = _textPainter.width;
      span = TextSpan(text: dayText, style: dayTextStyle);
      _textPainter.text = span;
      if (_isRTL) {
        _textPainter.layout(minWidth: 0, maxWidth: _startXPosition);
        _startXPosition -= _textPainter.width + (3 * padding);
        if (_startXPosition > 0) {
          _textPainter.paint(canvas,
              Offset(_startXPosition, (_dateHeight - _textPainter.height) / 2));
        }
      } else {
        _startXPosition += painterWidth + (3 * padding);
        if (_startXPosition > size.width) {
          return;
        }
        _textPainter.layout(
            minWidth: 0, maxWidth: size.width - _startXPosition);
        _textPainter.paint(canvas,
            Offset(_startXPosition, (_dateHeight - _textPainter.height) / 2));
      }
    }
  }

  void _drawTodayCircle(
      Canvas canvas, double xPosition, double yPosition, double padding) {
    canvas.drawCircle(
        Offset(xPosition + (_textPainter.width / 2),
            yPosition + (_textPainter.height / 2)),
        _textPainter.width > _textPainter.height
            ? (_textPainter.width / 2) + padding
            : (_textPainter.height / 2) + padding,
        _linePainter);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      return _getSemanticsBuilder(size);
    };
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    return true;
  }

  List<CustomPainterSemantics> _getSemanticsBuilder(Size size) {
    final List<CustomPainterSemantics> _semanticsBuilder =
        <CustomPainterSemantics>[];
    if (_selectedDate == null) {
      return _semanticsBuilder;
    } else if (_selectedDate != null) {
      _semanticsBuilder.add(CustomPainterSemantics(
        rect: Offset.zero & size,
        properties: SemanticsProperties(
          label: DateFormat('EEEEE').format(_selectedDate).toString() +
              DateFormat('dd/MMMM/yyyy').format(_selectedDate).toString(),
          textDirection: TextDirection.ltr,
        ),
      ));
    }

    return _semanticsBuilder;
  }
}
