part of calendar;

/// The dates that visible on the view changes in [SfCalendar].
///
/// Details for [ViewChangedCallback], such as [visibleDates].
class ViewChangedDetails {
  /// Creates details for [ViewChangedCallback].
  const ViewChangedDetails(this.visibleDates);

  /// The date collection that visible on current view.
  final List<DateTime> visibleDates;
}

/// The element that tapped on view in [SfCalendar]
///
/// Details for [CalendarTapCallback], such as [appointments], [date], and
/// [targetElement].
///
/// See also:
/// [CalendarTapCallback]
class CalendarTapDetails extends CalendarTouchDetails {
  /// Creates details for [CalendarTapCallback].
  const CalendarTapDetails(
      List<dynamic> appointments, DateTime date, CalendarElement element)
      : super(appointments, date, element);
}

/// The element that long pressed on view in [SfCalendar]
///
/// Details for [CalendarLongPressCallback], such as [appointments], [date] and
/// [targetElement].
///
/// See also:
/// [CalendarLongPressCallback]
class CalendarLongPressDetails extends CalendarTouchDetails {
  /// Creates details for [CalendarLongPressCallback]
  const CalendarLongPressDetails(
      List<dynamic> appointments, DateTime date, CalendarElement element)
      : super(appointments, date, element);
}

/// The element that tapped or long pressed on view in [SfCalendar].
///
/// Base class for [CalendarTapDetails] and [CalendarLongPressDetails].
///
/// See also:
/// [CalendarTapDetails]
/// [CalendarLongPressDetails]
/// [CalendarTapCallback]
/// [CalendarLongPressCallback]
class CalendarTouchDetails {
  /// Creates details for [CalendarTapCallback] and [CalendarLongPressCallback].
  const CalendarTouchDetails(this.appointments, this.date, this.targetElement);

  /// The collection of appointments that tapped or falls inside the selected date.
  final List<dynamic> appointments;

  /// The date cell that tapped on view.
  final DateTime date;

  /// The element that tapped on view.
  final CalendarElement targetElement;
}

/// args to update the required properties from calendar state to it's children's
class _UpdateCalendarStateDetails {
  DateTime _currentDate;
  List<DateTime> _currentViewVisibleDates;
  List<dynamic> _visibleAppointments;
  DateTime _selectedDate;
  double _allDayPanelHeight;
  List<_AppointmentView> _allDayAppointmentViewCollection;
  List<dynamic> _appointments;

  // ignore: prefer_final_fields
  bool _isAppointmentTapped = false;
}
