part of calendar;

class _HeaderViewPainter extends CustomPainter {
  _HeaderViewPainter(
      this._visibleDates,
      this._headerStyle,
      this._currentDate,
      this._view,
      this._numberOfWeeksInView,
      this._calendarTheme,
      this._isRTL,
      this._locale,
      this._notifier,
      this._mouseHoverPosition)
      : super(repaint: _notifier);

  final List<DateTime> _visibleDates;
  final CalendarHeaderStyle _headerStyle;
  final SfCalendarThemeData _calendarTheme;
  final DateTime _currentDate;
  final CalendarView _view;
  final int _numberOfWeeksInView;
  final bool _isRTL;
  final String _locale;
  final Offset _mouseHoverPosition;
  final ValueNotifier<DateTime> _notifier;
  String _headerText;
  TextPainter _textPainter;
  Paint _hoverPainter;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    const double padding = 5;
    double _xPosition = 5.0;
    _textPainter = _textPainter ?? TextPainter();
    _textPainter.textDirection = TextDirection.ltr;
    _textPainter.textWidthBasis = TextWidthBasis.longestLine;
    String _format = 'MMM';
    if (_view == CalendarView.schedule) {
      _format = 'MMMM';
      _headerText =
          DateFormat(_format, _locale).format(_notifier.value).toString() +
              ' ' +
              _notifier.value.year.toString();
    } else if (_view == CalendarView.month &&
        _numberOfWeeksInView != 6 &&
        _visibleDates[0].month !=
            _visibleDates[_visibleDates.length - 1].month) {
      _headerText =
          DateFormat(_format, _locale).format(_visibleDates[0]).toString() +
              ' ' +
              _visibleDates[0].year.toString() +
              ' - ' +
              DateFormat(_format, _locale)
                  .format(_visibleDates[_visibleDates.length - 1])
                  .toString() +
              ' ' +
              _visibleDates[_visibleDates.length - 1].year.toString();
    } else if (!_isTimelineView(_view)) {
      final DateTime _headerDate =
          _view == CalendarView.month ? _currentDate : _visibleDates[0];
      _format = 'MMMM';
      _headerText =
          DateFormat(_format, _locale).format(_headerDate).toString() +
              ' ' +
              _headerDate.year.toString();
    } else {
      _format = 'MMM';
      final DateTime startDate = _visibleDates[0];
      final DateTime endDate = _visibleDates[_visibleDates.length - 1];
      String startText = '';
      String endText = '';
      startText = DateFormat(_format, _locale).format(startDate).toString();
      if (_view != CalendarView.timelineDay) {
        startText = startDate.day.toString() + ' ' + startText + ' - ';
        endText = endDate.day.toString() +
            ' ' +
            DateFormat(_format, _locale).format(endDate).toString() +
            ' ' +
            endDate.year.toString();
      } else {
        endText = ' ' + startDate.year.toString();
      }

      _headerText = startText + endText;
    }

    TextStyle style = _headerStyle.textStyle;
    style ??= _calendarTheme.headerTextStyle;

    final TextSpan span = TextSpan(text: _headerText, style: style);
    _textPainter.text = span;

    if (_headerStyle.textAlign == TextAlign.justify) {
      _textPainter.textAlign = _headerStyle.textAlign;
    }

    _textPainter.layout(minWidth: 0, maxWidth: size.width - _xPosition);

    if (_headerStyle.textAlign == TextAlign.right ||
        _headerStyle.textAlign == TextAlign.end) {
      _xPosition = size.width - _textPainter.width;
    } else if (_headerStyle.textAlign == TextAlign.center) {
      _xPosition = size.width / 2 - _textPainter.width / 2;
    }

    if (_isRTL) {
      _xPosition = size.width - _textPainter.width - padding;
      if (_headerStyle.textAlign == TextAlign.left ||
          _headerStyle.textAlign == TextAlign.end) {
        _xPosition = 5.0;
      } else if (_headerStyle.textAlign == TextAlign.center) {
        _xPosition = size.width / 2 - _textPainter.width / 2;
      }
    }

    if (_mouseHoverPosition != null) {
      _hoverPainter ??= Paint();
      if (_xPosition <= _mouseHoverPosition.dx &&
          _xPosition + _textPainter.width >= _mouseHoverPosition.dx) {
        _hoverPainter.color = Colors.grey.withOpacity(0.3);
        canvas.drawRect(
            Rect.fromLTWH(_xPosition - padding, 0,
                _textPainter.width + (padding * 2), size.height),
            _hoverPainter);
      }
    }

    _textPainter.paint(
        canvas, Offset(_xPosition, size.height / 2 - _textPainter.height / 2));
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      final dynamic rect = Offset.zero & size;
      return <CustomPainterSemantics>[
        CustomPainterSemantics(
          rect: rect,
          properties: SemanticsProperties(
            label: _headerText,
            textDirection: TextDirection.ltr,
          ),
        ),
      ];
    };
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    final _HeaderViewPainter oldWidget = oldDelegate;
    return oldWidget._headerText != _headerText;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final _HeaderViewPainter oldWidget = oldDelegate;
    return oldWidget._visibleDates != _visibleDates ||
        oldWidget._headerStyle != _headerStyle ||
        oldWidget._currentDate != _currentDate ||
        oldWidget._calendarTheme != _calendarTheme ||
        oldWidget._isRTL != _isRTL ||
        oldWidget._locale != _locale ||
        oldWidget._mouseHoverPosition != _mouseHoverPosition;
  }
}

class _CalendarHeaderView extends StatefulWidget {
  const _CalendarHeaderView(
      this.visibleDates,
      this.headerStyle,
      this.currentDate,
      this.view,
      this.numberOfWeeksInView,
      this.calendarTheme,
      this.isRTL,
      this.locale,
      this.showNavigationArrow,
      this.controller,
      this.maxDate,
      this.minDate,
      this.width,
      this.height,
      this.nonWorkingDays,
      this.navigationDirection,
      this._headerTapCallback,
      this._headerLongPressCallback);

  final List<DateTime> visibleDates;
  final CalendarHeaderStyle headerStyle;
  final SfCalendarThemeData calendarTheme;
  final DateTime currentDate;
  final CalendarView view;
  final int numberOfWeeksInView;
  final bool isRTL;
  final String locale;
  final bool showNavigationArrow;
  final CalendarController controller;
  final DateTime maxDate;
  final DateTime minDate;
  final double width;
  final double height;
  final List<int> nonWorkingDays;
  final MonthNavigationDirection navigationDirection;
  final VoidCallback _headerTapCallback;
  final VoidCallback _headerLongPressCallback;

  @override
  _CalendarHeaderViewState createState() => _CalendarHeaderViewState();
}

class _CalendarHeaderViewState extends State<_CalendarHeaderView> {
  Offset _mouseHoverPosition;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _updateMouseHoveringPosition(
      dynamic event, double _arrowWidth, double _headerWidth) {
    final RenderBox box = context.findRenderObject();
    final Offset _localPosition = box.globalToLocal(event.position);
    double _xPosition = _localPosition.dx;

    if (widget.isRTL) {
      if (widget.headerStyle.textAlign == TextAlign.left ||
          widget.headerStyle.textAlign == TextAlign.end) {
        _xPosition = _localPosition.dx - (_arrowWidth * 2);
      }
    }

    if (!widget.isRTL &&
        (widget.headerStyle.textAlign == null ||
            widget.headerStyle.textAlign == TextAlign.left ||
            widget.headerStyle.textAlign == TextAlign.start)) {
      _xPosition = _localPosition.dx - (_arrowWidth * 2);
    } else if (widget.headerStyle.textAlign == TextAlign.center ||
        widget.headerStyle.textAlign == TextAlign.justify) {
      _headerWidth = _headerWidth > 200 ? 200 : _headerWidth;
      final double _headerStartPosition =
          (widget.width - (_headerWidth + (_arrowWidth * 2))) / 2;
      _xPosition = _localPosition.dx - _headerStartPosition - _arrowWidth;
    }

    setState(() {
      _mouseHoverPosition = Offset(_xPosition, _localPosition.dy);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _arrowWidth = 0;
    double _headerWidth = widget.width;
    if (widget.showNavigationArrow) {
      _arrowWidth = widget.width / 6;
      _arrowWidth = _arrowWidth > 50 ? 50 : _arrowWidth;
      _headerWidth = widget.width - (_arrowWidth * 2);
    }

    Color arrowColor = widget.headerStyle.textStyle != null
        ? widget.headerStyle.textStyle.color
        : (widget.calendarTheme.headerTextStyle.color);
    arrowColor ??= Colors.black87;
    arrowColor = arrowColor.withOpacity(arrowColor.opacity * 0.6);
    Color _prevArrowColor = arrowColor;
    Color _nextArrowColor = arrowColor;
    final List<DateTime> _dates = widget.visibleDates;
    if (!_canMoveToNextView(widget.view, widget.numberOfWeeksInView,
        widget.minDate, widget.maxDate, _dates, widget.nonWorkingDays)) {
      _nextArrowColor =
          _nextArrowColor.withOpacity(_nextArrowColor.opacity * 0.5);
    }

    if (!_canMoveToPreviousView(widget.view, widget.numberOfWeeksInView,
        widget.minDate, widget.maxDate, _dates, widget.nonWorkingDays)) {
      _prevArrowColor =
          _prevArrowColor.withOpacity(_prevArrowColor.opacity * 0.5);
    }

    double _arrowSize = widget.height * 0.6;
    _arrowSize = _arrowSize > 25 ? 25 : _arrowSize;
    final bool _isCenterAlignment = kIsWeb &&
        widget.showNavigationArrow &&
        widget.headerStyle.textAlign != null &&
        (widget.headerStyle.textAlign == TextAlign.center ||
            widget.headerStyle.textAlign == TextAlign.justify);
    final Widget _headerText = GestureDetector(
        child: MouseRegion(
            onEnter: (PointerEnterEvent event) {
              _updateMouseHoveringPosition(event, _arrowWidth, _headerWidth);
            },
            onHover: (PointerHoverEvent event) {
              _updateMouseHoveringPosition(event, _arrowWidth, _headerWidth);
            },
            onExit: (PointerExitEvent event) {
              setState(() {
                _mouseHoverPosition = null;
              });
            },
            child: RepaintBoundary(
                child: CustomPaint(
              painter: _HeaderViewPainter(
                  _dates,
                  widget.headerStyle,
                  widget.currentDate,
                  widget.view,
                  widget.numberOfWeeksInView,
                  widget.calendarTheme,
                  widget.isRTL,
                  widget.locale,
                  null,
                  _mouseHoverPosition),
              size: _isCenterAlignment
                  ? Size(_headerWidth > 200 ? 200 : _headerWidth, widget.height)
                  : Size(_headerWidth, widget.height),
            ))),
        onTapUp: (TapUpDetails details) {
          widget._headerTapCallback();
        },
        onLongPressStart: (LongPressStartDetails details) {
          widget._headerLongPressCallback();
        });

    final Container _leftArrow = Container(
      alignment: Alignment.center,
      color: widget.headerStyle.backgroundColor ??
          widget.calendarTheme.headerBackgroundColor,
      width: _arrowWidth,
      padding: const EdgeInsets.all(0),
      child: FlatButton(
        //// set splash color as transparent when arrow reaches min date(disabled)
        splashColor: _prevArrowColor != arrowColor ? Colors.transparent : null,
        highlightColor:
            _prevArrowColor != arrowColor ? Colors.transparent : null,
        hoverColor: _prevArrowColor != arrowColor ? Colors.transparent : null,
        color: widget.headerStyle.backgroundColor ??
            widget.calendarTheme.headerBackgroundColor,
        onPressed: widget.controller.backward,
        padding: const EdgeInsets.all(0),
        child: Semantics(
          label: 'Backward',
          child: Icon(
            widget.navigationDirection == MonthNavigationDirection.horizontal
                ? Icons.chevron_left
                : Icons.keyboard_arrow_up,
            color: _prevArrowColor,
            size: _arrowSize,
          ),
        ),
      ),
    );

    final Container _rightArrow = Container(
      alignment: Alignment.center,
      color: widget.headerStyle.backgroundColor ??
          widget.calendarTheme.headerBackgroundColor,
      width: _arrowWidth,
      padding: const EdgeInsets.all(0),
      child: FlatButton(
        //// set splash color as transparent when arrow reaches max date(disabled)
        splashColor: _nextArrowColor != arrowColor ? Colors.transparent : null,
        highlightColor:
            _nextArrowColor != arrowColor ? Colors.transparent : null,
        hoverColor: _nextArrowColor != arrowColor ? Colors.transparent : null,
        color: widget.headerStyle.backgroundColor ??
            widget.calendarTheme.headerBackgroundColor,
        onPressed: widget.controller.forward,
        padding: const EdgeInsets.all(0),
        child: Semantics(
          label: 'Forward',
          child: Icon(
            widget.navigationDirection == MonthNavigationDirection.horizontal
                ? Icons.chevron_right
                : Icons.keyboard_arrow_down,
            color: _nextArrowColor,
            size: _arrowSize,
          ),
        ),
      ),
    );

    if (widget.isRTL) {
      if (widget.headerStyle.textAlign == TextAlign.left) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: !kIsWeb
                ? <Widget>[
                    _leftArrow,
                    _rightArrow,
                    _headerText,
                  ]
                : <Widget>[
                    _headerText,
                    _leftArrow,
                    _rightArrow,
                  ]);
      } else if (widget.headerStyle.textAlign == null ||
          widget.headerStyle.textAlign == TextAlign.right) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: !kIsWeb
                ? <Widget>[
                    _headerText,
                    _leftArrow,
                    _rightArrow,
                  ]
                : <Widget>[
                    _leftArrow,
                    _rightArrow,
                    _headerText,
                  ]);
      }
    }

    if (widget.headerStyle.textAlign == null ||
        widget.headerStyle.textAlign == TextAlign.left ||
        widget.headerStyle.textAlign == TextAlign.start) {
      return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: kIsWeb
              ? <Widget>[
                  _leftArrow,
                  _rightArrow,
                  _headerText,
                ]
              : <Widget>[
                  _headerText,
                  _leftArrow,
                  _rightArrow,
                ]);
    } else if (widget.headerStyle.textAlign == TextAlign.right ||
        widget.headerStyle.textAlign == TextAlign.end) {
      return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: kIsWeb
              ? <Widget>[
                  _headerText,
                  _leftArrow,
                  _rightArrow,
                ]
              : <Widget>[
                  _leftArrow,
                  _rightArrow,
                  _headerText,
                ]);
    } else {
      return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _leftArrow,
            _headerText,
            _rightArrow,
          ]);
    }
  }
}
