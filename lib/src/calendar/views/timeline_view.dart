part of calendar;

class _TimelineView extends CustomPainter {
  _TimelineView(
      this._horizontalLinesCountPerView,
      this._visibleDates,
      this._timeLabelWidth,
      this._timeSlotViewSettings,
      this._timeIntervalHeight,
      this._cellBorderColor,
      this._isRTL,
      this._locale,
      this._calendarTheme,
      this._calendarCellNotifier,
      this._scrollController,
      this.specialRegion)
      : super(repaint: _calendarCellNotifier);

  final double _horizontalLinesCountPerView;
  final List<DateTime> _visibleDates;
  SfCalendar calendar;
  final double _timeLabelWidth;
  final TimeSlotViewSettings _timeSlotViewSettings;
  final double _timeIntervalHeight;
  final Color _cellBorderColor;
  final SfCalendarThemeData _calendarTheme;
  final String _locale;
  final bool _isRTL;
  final ValueNotifier<Offset> _calendarCellNotifier;
  final ScrollController _scrollController;
  final List<TimeRegion> specialRegion;
  double _x1, _x2, _y1, _y2;
  Paint _linePainter;
  TextPainter _textPainter;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    _x1 = 0;
    _x2 = size.width;
    _y1 = _timeIntervalHeight;
    _y2 = _timeIntervalHeight;
    _linePainter = _linePainter ?? Paint();
    final double _minuteHeight =
        _timeIntervalHeight / _getTimeInterval(_timeSlotViewSettings);
    if (specialRegion != null) {
      final DateTime startDate = _convertToStartTime(_visibleDates[0]);
      final DateTime endDate =
          _convertToEndTime(_visibleDates[_visibleDates.length - 1]);
      final TextPainter painter = TextPainter(
          textDirection: TextDirection.ltr,
          maxLines: 1,
          textAlign: _isRTL ? TextAlign.right : TextAlign.left,
          textWidthBasis: TextWidthBasis.longestLine);

      _linePainter.style = PaintingStyle.fill;
      final double _viewWidth = size.width / _visibleDates.length;
      for (int i = 0; i < specialRegion.length; i++) {
        final TimeRegion region = specialRegion[i];
        _linePainter.color = region.color ?? Colors.grey.withOpacity(0.2);
        final DateTime regionStartTime = region._actualStartTime;
        final DateTime regionEndTime = region._actualEndTime;

        /// Check the start date and end date as same.
        if (regionStartTime.year == regionEndTime.year &&
            regionStartTime.month == regionEndTime.month &&
            regionStartTime.day == regionEndTime.day &&
            regionStartTime.hour == regionEndTime.hour &&
            regionStartTime.minute == regionEndTime.minute) {
          continue;
        }

        /// Check the visible regions holds the region or not
        if (!((regionStartTime.isAfter(startDate) &&
                    regionStartTime.isBefore(endDate)) ||
                (regionEndTime.isAfter(startDate) &&
                    regionEndTime.isBefore(endDate))) &&
            !(regionStartTime.isBefore(startDate) &&
                regionEndTime.isAfter(endDate))) {
          continue;
        }

        int startIndex = _getVisibleDateIndex(_visibleDates, regionStartTime);
        int endIndex = _getVisibleDateIndex(_visibleDates, regionEndTime);

        double startXPosition = _getTimeToPosition(
            Duration(
                hours: regionStartTime.hour, minutes: regionStartTime.minute),
            _timeSlotViewSettings,
            _minuteHeight);
        if (startIndex == -1) {
          if (startDate.isAfter(regionStartTime)) {
            /// Set index as 0 when the region start date before the visible start date
            startIndex = 0;
          } else {
            /// Find the next index when the start date as non working date.
            for (int k = 1; k < _visibleDates.length; k++) {
              final DateTime _currentDate = _visibleDates[k];
              if (_currentDate.isBefore(regionStartTime)) {
                continue;
              }

              startIndex = k;
              break;
            }

            if (startIndex == -1) {
              startIndex = 0;
            }
          }

          /// Start date as non working day and its index as next date index.
          /// so assign the position value as 0
          startXPosition = 0;
        }

        double endXPosition = _getTimeToPosition(
            Duration(hours: regionEndTime.hour, minutes: regionEndTime.minute),
            _timeSlotViewSettings,
            _minuteHeight);
        if (endIndex == -1) {
          /// Find the previous index when the end date as non working date.
          if (endDate.isAfter(regionEndTime)) {
            for (int k = _visibleDates.length - 2; k >= 0; k--) {
              final DateTime _currentDate = _visibleDates[k];
              if (_currentDate.isAfter(regionEndTime)) {
                continue;
              }

              endIndex = k;
              break;
            }

            if (endIndex == -1) {
              endIndex = _visibleDates.length - 1;
            }
          } else {
            /// Set index as visible date end date index when the
            /// region end date before the visible end date
            endIndex = _visibleDates.length - 1;
          }

          /// End date as non working day and its index as previous date index.
          /// so assign the position value as view width
          endXPosition = _viewWidth;
        }

        double startPosition = (startIndex * _viewWidth) + startXPosition;
        double endPosition = (endIndex * _viewWidth) + endXPosition;

        /// Check the start and end position not between the visible hours
        /// position(not between start and end hour)
        if ((startPosition <= 0 && endPosition <= 0) ||
            (startPosition >= size.width && endPosition >= size.width) ||
            (startPosition == endPosition)) {
          continue;
        }

        if (_isRTL) {
          startPosition = size.width - startPosition;
          endPosition = size.width - endPosition;
        }

        Rect rect;
        if (_isRTL) {
          rect = Rect.fromLTRB(
              endPosition, _timeLabelWidth, startPosition, size.height);
        } else {
          rect = Rect.fromLTRB(
              startPosition, _timeLabelWidth, endPosition, size.height);
        }
        canvas.drawRect(rect, _linePainter);
        if ((region.text == null || region.text.isEmpty) &&
            region.iconData == null) {
          continue;
        }

        final TextStyle textStyle = region.textStyle ??
            TextStyle(
                color: _calendarTheme.brightness != null &&
                        _calendarTheme.brightness == Brightness.dark
                    ? Colors.white54
                    : Colors.black45);
        painter.textDirection = TextDirection.ltr;
        painter.textAlign = _isRTL ? TextAlign.right : TextAlign.left;
        if (region.iconData == null) {
          painter.text = TextSpan(text: region.text, style: textStyle);
          painter.ellipsis = '..';
        } else {
          painter.text = TextSpan(
              text: String.fromCharCode(region.iconData.codePoint),
              style:
                  textStyle.copyWith(fontFamily: region.iconData.fontFamily));
        }

        painter.layout(minWidth: 0, maxWidth: rect.width - 4);
        painter.paint(canvas, Offset(rect.left + 3, rect.top + 3));
      }
    }

    _linePainter.strokeWidth = 0.5;
    _linePainter.strokeCap = StrokeCap.round;
    _linePainter.color = _cellBorderColor != null
        ? _cellBorderColor
        : _calendarTheme.cellBorderColor;
    _x1 = 0;
    _x2 = size.width;
    _y1 = _timeLabelWidth;
    _y2 = _timeLabelWidth;

    final Offset _start = Offset(_x1, _y1);
    final Offset _end = Offset(_x2, _y2);
    canvas.drawLine(_start, _end, _linePainter);

    _x1 = 0;
    _x2 = 0;
    _y2 = size.height;
    if (_isRTL) {
      _x1 = size.width;
      _x2 = size.width;
    }
    final List<Offset> points = <Offset>[];
    for (int i = 0;
        i < _horizontalLinesCountPerView * _visibleDates.length;
        i++) {
      _y1 = _timeLabelWidth;
      if (i % _horizontalLinesCountPerView == 0) {
        _y1 = 0;
        _drawTimeLabels(canvas, size, _x1);
      }

      if (kIsWeb) {
        canvas.drawLine(Offset(_x1, _y1), Offset(_x2, _y2), _linePainter);
      } else {
        points.add(Offset(_x1, _y1));
        points.add(Offset(_x2, _y2));
      }

      if (_isRTL) {
        _x1 -= _timeIntervalHeight;
        _x2 -= _timeIntervalHeight;
      } else {
        _x1 += _timeIntervalHeight;
        _x2 += _timeIntervalHeight;
      }
    }

    if (!kIsWeb) {
      canvas.drawPoints(PointMode.lines, points, _linePainter);
    }

    if (_calendarCellNotifier.value != null) {
      final double _left =
          (_calendarCellNotifier.value.dx ~/ _timeIntervalHeight) *
              _timeIntervalHeight;
      final double _top = _timeLabelWidth;
      const double _padding = 0.5;
      double width = _timeIntervalHeight;
      double difference = 0;
      if (_isRTL &&
          (size.width - _scrollController.offset) <
              _scrollController.position.viewportDimension) {
        difference = _scrollController.position.viewportDimension - size.width;
      }

      if ((size.width - _scrollController.offset) <
              _scrollController.position.viewportDimension &&
          (_left + _timeIntervalHeight).round() == size.width.round()) {
        width -= _padding;
      }

      _linePainter.style = PaintingStyle.stroke;
      _linePainter.strokeWidth = 2;
      _linePainter.color = _calendarTheme.selectionBorderColor.withOpacity(0.4);
      canvas.drawRect(
          Rect.fromLTWH(
              _left == 0 ? _left - difference + _padding : _left - difference,
              _top,
              width,
              size.height),
          _linePainter);
    }
  }

  void _drawTimeLabels(Canvas canvas, Size size, double xPosition) {
    final double _startHour = _timeSlotViewSettings.startHour;
    final int _timeInterval = _getTimeInterval(_timeSlotViewSettings);
    final String _timeFormat = _timeSlotViewSettings.timeFormat;

    _textPainter = _textPainter ?? TextPainter();
    _textPainter.textDirection = TextDirection.ltr;
    _textPainter.textAlign = TextAlign.left;
    _textPainter.textWidthBasis = TextWidthBasis.parent;
    TextStyle timeTextStyle = _timeSlotViewSettings.timeTextStyle;
    timeTextStyle ??= _calendarTheme.timeTextStyle;

    DateTime date = DateTime.now();

    for (int i = 0; i < _horizontalLinesCountPerView; i++) {
      final dynamic hour = (_startHour - _startHour.toInt()) * 60;
      final dynamic minute = (i * _timeInterval) + hour;
      date = DateTime(
          date.day, date.month, date.year, _startHour.toInt(), minute.toInt());
      final dynamic _time =
          DateFormat(_timeFormat, _locale).format(date).toString();
      final TextSpan span = TextSpan(
        text: _time,
        style: timeTextStyle,
      );

      _textPainter.text = span;
      _textPainter.layout(minWidth: 0, maxWidth: _timeIntervalHeight);
      if (_textPainter.height > _timeLabelWidth) {
        return;
      }

      _textPainter.paint(
          canvas,
          Offset(_isRTL ? xPosition - _textPainter.width : xPosition,
              _timeLabelWidth / 2 - _textPainter.height / 2));
      if (_isRTL) {
        xPosition -= _timeIntervalHeight;
      } else {
        xPosition += _timeIntervalHeight;
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final _TimelineView oldWidget = oldDelegate;
    return oldWidget._horizontalLinesCountPerView !=
            _horizontalLinesCountPerView ||
        oldWidget._timeLabelWidth != _timeLabelWidth ||
        oldWidget._visibleDates != _visibleDates ||
        oldWidget._cellBorderColor != _cellBorderColor ||
        oldWidget._calendarTheme != _calendarTheme ||
        oldWidget._timeSlotViewSettings != _timeSlotViewSettings ||
        oldWidget._isRTL != _isRTL ||
        oldWidget._locale != _locale ||
        oldWidget._calendarCellNotifier.value != _calendarCellNotifier.value ||
        oldWidget.specialRegion != specialRegion;
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      return _getSemanticsBuilder(size);
    };
  }

  List<CustomPainterSemantics> _getSemanticsBuilder(Size size) {
    final List<CustomPainterSemantics> _semanticsBuilder =
        <CustomPainterSemantics>[];
    double _left, _top, _cellWidth, _cellHeight;
    _top = _timeLabelWidth;
    _cellWidth = _timeIntervalHeight;
    _cellHeight = size.height;
    _left = _isRTL ? size.width - _cellWidth : 0;
    for (int j = 0; j < _visibleDates.length; j++) {
      DateTime date = _visibleDates[j];
      final dynamic hour = (_timeSlotViewSettings.startHour -
              _timeSlotViewSettings.startHour.toInt()) *
          60;
      for (int i = 0; i < _horizontalLinesCountPerView; i++) {
        final dynamic minute =
            (i * _getTimeInterval(_timeSlotViewSettings)) + hour;
        date = DateTime(date.year, date.month, date.day,
            _timeSlotViewSettings.startHour.toInt(), minute.toInt());
        _semanticsBuilder.add(CustomPainterSemantics(
          rect: Rect.fromLTWH(_left, _top, _cellWidth, _cellHeight),
          properties: SemanticsProperties(
            label: DateFormat('h a, dd/MMMM/yyyy').format(date).toString(),
            textDirection: TextDirection.ltr,
          ),
        ));
        if (_isRTL) {
          _left -= _cellWidth;
        } else {
          _left += _cellWidth;
        }
      }
    }

    return _semanticsBuilder;
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    final _TimelineView _oldWidget = oldDelegate;
    return _oldWidget._visibleDates != _visibleDates;
  }
}

class _TimelineViewHeaderView extends CustomPainter {
  _TimelineViewHeaderView(
      this._visibleDates,
      this._calendarViewState,
      this._repaintNotifier,
      this._viewHeaderStyle,
      this._timeSlotViewSettings,
      this._viewHeaderHeight,
      this._isRTL,
      this._todayHighlightColor,
      this._locale,
      this._calendarTheme,
      this._minDate,
      this._maxDate,
      this._viewHeaderNotifier)
      : super(repaint: _repaintNotifier);

  final List<DateTime> _visibleDates;
  final ViewHeaderStyle _viewHeaderStyle;
  final TimeSlotViewSettings _timeSlotViewSettings;
  final double _viewHeaderHeight;
  final Color _todayHighlightColor;
  final double _padding = 5;
  final ValueNotifier<bool> _repaintNotifier;
  final _CalendarViewState _calendarViewState;
  final SfCalendarThemeData _calendarTheme;
  final bool _isRTL;
  final String _locale;
  final DateTime _minDate;
  final DateTime _maxDate;
  final ValueNotifier<Offset> _viewHeaderNotifier;
  double _xPosition = 0;
  TextPainter dayTextPainter, dateTextPainter;
  Paint _hoverPainter;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    final DateTime _today = DateTime.now();
    final double childWidth = size.width / _visibleDates.length;
    final double scrolledPosition =
        _calendarViewState._timelineViewHeaderScrollController.offset;
    final int index = scrolledPosition ~/ childWidth;
    _xPosition = scrolledPosition;

    TextStyle viewHeaderDateTextStyle =
        _calendarTheme.brightness == Brightness.light
            ? const TextStyle(
                color: Colors.black87,
                fontSize: 18,
                fontWeight: FontWeight.w400,
                fontFamily: 'Roboto')
            : const TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w400,
                fontFamily: 'Roboto');
    TextStyle viewHeaderDayTextStyle =
        _calendarTheme.brightness == Brightness.light
            ? const TextStyle(
                color: Colors.black87,
                fontSize: 11,
                fontWeight: FontWeight.w400,
                fontFamily: 'Roboto')
            : const TextStyle(
                color: Colors.white,
                fontSize: 11,
                fontWeight: FontWeight.w400,
                fontFamily: 'Roboto');

    TextStyle _viewHeaderDayStyle = _viewHeaderStyle.dayTextStyle;
    TextStyle _viewHeaderDateStyle = _viewHeaderStyle.dateTextStyle;

    if (viewHeaderDayTextStyle == _calendarTheme.viewHeaderDayTextStyle) {
      viewHeaderDayTextStyle = viewHeaderDayTextStyle.copyWith(fontSize: 18);
    }

    if (viewHeaderDateTextStyle == _calendarTheme.viewHeaderDateTextStyle) {
      viewHeaderDateTextStyle = viewHeaderDateTextStyle.copyWith(fontSize: 18);
    }

    _viewHeaderDayStyle ??= viewHeaderDayTextStyle;
    _viewHeaderDateStyle ??= viewHeaderDateTextStyle;

    TextStyle _dayTextStyle = _viewHeaderDayStyle;
    TextStyle _dateTextStyle = _viewHeaderDateStyle;

    for (int i = 0; i < _visibleDates.length; i++) {
      if (i < index) {
        continue;
      }

      final DateTime _currentDate = _visibleDates[i];
      String dayFormat = 'EE';
      dayFormat = dayFormat == _timeSlotViewSettings.dayFormat
          ? 'EEEE'
          : _timeSlotViewSettings.dayFormat;

      final String dayText =
          DateFormat(dayFormat, _locale).format(_currentDate).toString();
      final String dateText = DateFormat(_timeSlotViewSettings.dateFormat)
          .format(_currentDate)
          .toString();

      if (isSameDate(_currentDate, _today)) {
        _dayTextStyle =
            _viewHeaderDayStyle.copyWith(color: _todayHighlightColor);
        _dateTextStyle =
            _viewHeaderDateStyle.copyWith(color: _todayHighlightColor);
      } else {
        _dateTextStyle = _viewHeaderDateStyle;
        _dayTextStyle = _viewHeaderDayStyle;
      }

      if (!isDateWithInDateRange(_minDate, _maxDate, _currentDate)) {
        if (_calendarTheme.brightness == Brightness.light) {
          _dayTextStyle = _dayTextStyle.copyWith(color: Colors.black26);
          _dateTextStyle = _dateTextStyle.copyWith(color: Colors.black26);
        } else {
          _dayTextStyle = _dayTextStyle.copyWith(color: Colors.white38);
          _dateTextStyle = _dateTextStyle.copyWith(color: Colors.white38);
        }
      }

      final TextSpan dayTextSpan =
          TextSpan(text: dayText, style: _dayTextStyle);

      dayTextPainter = dayTextPainter ?? TextPainter();
      dayTextPainter.text = dayTextSpan;
      dayTextPainter.textDirection = TextDirection.ltr;
      dayTextPainter.textAlign = TextAlign.left;
      dayTextPainter.textWidthBasis = TextWidthBasis.longestLine;

      final TextSpan dateTextSpan =
          TextSpan(text: dateText, style: _dateTextStyle);

      dateTextPainter = dateTextPainter ?? TextPainter();
      dateTextPainter.text = dateTextSpan;
      dateTextPainter.textDirection = TextDirection.ltr;
      dateTextPainter.textAlign = TextAlign.left;
      dateTextPainter.textWidthBasis = TextWidthBasis.longestLine;

      dayTextPainter.layout(minWidth: 0, maxWidth: childWidth);
      dateTextPainter.layout(minWidth: 0, maxWidth: childWidth);
      if (dateTextPainter.width +
              _xPosition +
              (_padding * 2) +
              dayTextPainter.width >
          (i + 1) * childWidth) {
        _xPosition = ((i + 1) * childWidth) -
            (dateTextPainter.width + (_padding * 2) + dayTextPainter.width);
      }

      if (_viewHeaderNotifier.value != null) {
        _hoverPainter ??= Paint();
        double difference = 0;
        if (_isRTL &&
            (size.width -
                    _calendarViewState
                        ._timelineViewHeaderScrollController.offset) <
                _calendarViewState._timelineViewHeaderScrollController.position
                    .viewportDimension) {
          difference = _calendarViewState._timelineViewHeaderScrollController
                  .position.viewportDimension -
              size.width;
        }
        final double leftPosition = _isRTL
            ? size.width -
                _xPosition -
                (_padding * 2) -
                dayTextPainter.width -
                dateTextPainter.width -
                _padding
            : _xPosition;
        final double rightPosition = _isRTL
            ? size.width - _xPosition
            : _xPosition +
                dayTextPainter.width +
                dateTextPainter.width +
                (2 * _padding);
        if (leftPosition + difference <= _viewHeaderNotifier.value.dx &&
            rightPosition + difference >= _viewHeaderNotifier.value.dx &&
            (size.height) - _padding >= _viewHeaderNotifier.value.dy) {
          _hoverPainter.color = Colors.grey.withOpacity(0.2);
          canvas.drawRect(
              Rect.fromLTRB(leftPosition, 0, rightPosition + _padding,
                  dateTextPainter.height + _padding),
              _hoverPainter);
        }
      }

      if (_isRTL) {
        dateTextPainter.paint(
            canvas,
            Offset(
                size.width -
                    _xPosition -
                    (_padding * 2) -
                    dayTextPainter.width -
                    dateTextPainter.width,
                _viewHeaderHeight / 2 - dateTextPainter.height / 2));
        dayTextPainter.paint(
            canvas,
            Offset(size.width - _xPosition - _padding - dayTextPainter.width,
                _viewHeaderHeight / 2 - dayTextPainter.height / 2));
      } else {
        dateTextPainter.paint(
            canvas,
            Offset(_padding + _xPosition,
                _viewHeaderHeight / 2 - dateTextPainter.height / 2));
        dayTextPainter.paint(
            canvas,
            Offset(dateTextPainter.width + _xPosition + (_padding * 2),
                _viewHeaderHeight / 2 - dayTextPainter.height / 2));
      }

      if (index == i) {
        _xPosition = (i + 1) * childWidth;
      } else {
        _xPosition += childWidth;
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final _TimelineViewHeaderView oldWidget = oldDelegate;
    return oldWidget._visibleDates != _visibleDates ||
        oldWidget._xPosition != _xPosition ||
        oldWidget._viewHeaderStyle != _viewHeaderStyle ||
        oldWidget._timeSlotViewSettings != _timeSlotViewSettings ||
        oldWidget._viewHeaderHeight != _viewHeaderHeight ||
        oldWidget._todayHighlightColor != _todayHighlightColor ||
        oldWidget._isRTL != _isRTL ||
        oldWidget._locale != _locale ||
        oldWidget._viewHeaderNotifier.value != _viewHeaderNotifier.value;
  }

  List<CustomPainterSemantics> _getSemanticsBuilder(Size size) {
    final List<CustomPainterSemantics> _semanticsBuilder =
        <CustomPainterSemantics>[];
    double _left, _top, _cellWidth;
    _top = 0;
    _cellWidth = size.width / _visibleDates.length;
    _left = _isRTL ? size.width - _cellWidth : 0;
    for (int i = 0; i < _visibleDates.length; i++) {
      _semanticsBuilder.add(CustomPainterSemantics(
        rect: Rect.fromLTWH(_left, _top, _cellWidth, size.height),
        properties: SemanticsProperties(
          label: _getAccessibilityText(_visibleDates[i]),
          textDirection: TextDirection.ltr,
        ),
      ));
      if (_isRTL) {
        _left -= _cellWidth;
      } else {
        _left += _cellWidth;
      }
    }

    return _semanticsBuilder;
  }

  String _getAccessibilityText(DateTime date) {
    if (!isDateWithInDateRange(_minDate, _maxDate, date)) {
      return DateFormat('EEEEE').format(date).toString() +
          DateFormat('dd/MMMM/yyyy').format(date).toString() +
          ', Disabled date';
    }

    return DateFormat('EEEEE').format(date).toString() +
        DateFormat('dd/MMMM/yyyy').format(date).toString();
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      return _getSemanticsBuilder(size);
    };
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    final _TimelineViewHeaderView _oldWidget = oldDelegate;
    return _oldWidget._visibleDates != _visibleDates ||
        _oldWidget._calendarTheme != _calendarTheme;
  }
}
