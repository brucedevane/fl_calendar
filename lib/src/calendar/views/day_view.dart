part of calendar;

class _TimeSlotView extends CustomPainter {
  _TimeSlotView(
      this._visibleDates,
      this._horizontalLinesCount,
      this._timeIntervalHeight,
      this._timeLabelWidth,
      this._cellBorderColor,
      this._calendarTheme,
      this._timeSlotViewSettings,
      this._isRTL,
      this.specialRegion,
      this._calendarCellNotifier)
      : super(repaint: _calendarCellNotifier);

  final List<DateTime> _visibleDates;
  final double _horizontalLinesCount;
  final double _timeIntervalHeight;
  final double _timeLabelWidth;
  final Color _cellBorderColor;
  final SfCalendarThemeData _calendarTheme;
  final TimeSlotViewSettings _timeSlotViewSettings;
  final bool _isRTL;
  final ValueNotifier<Offset> _calendarCellNotifier;
  final List<TimeRegion> specialRegion;
  double _cellWidth;
  Paint _linePainter;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    final double _width = size.width - _timeLabelWidth;
    _cellWidth = _width / _visibleDates.length;
    _linePainter = _linePainter ?? Paint();

    final double _minuteHeight =
        _timeIntervalHeight / _getTimeInterval(_timeSlotViewSettings);
    if (specialRegion != null) {
      final DateTime startDate = _convertToStartTime(_visibleDates[0]);
      final DateTime endDate =
          _convertToEndTime(_visibleDates[_visibleDates.length - 1]);
      final TextPainter painter = TextPainter(
          textDirection: TextDirection.ltr,
          maxLines: 1,
          textAlign: _isRTL ? TextAlign.right : TextAlign.left,
          textWidthBasis: TextWidthBasis.longestLine);

      _linePainter.style = PaintingStyle.fill;
      for (int i = 0; i < specialRegion.length; i++) {
        final TimeRegion region = specialRegion[i];
        _linePainter.color = region.color ?? Colors.grey.withOpacity(0.2);
        final DateTime regionStartTime = region._actualStartTime;
        final DateTime regionEndTime = region._actualEndTime;

        /// Check the start date and end date as same.
        if (regionStartTime.year == regionEndTime.year &&
            regionStartTime.month == regionEndTime.month &&
            regionStartTime.day == regionEndTime.day &&
            regionStartTime.hour == regionEndTime.hour &&
            regionStartTime.minute == regionEndTime.minute) {
          continue;
        }

        /// Check the visible regions holds the region or not
        if (!((regionStartTime.isAfter(startDate) &&
                    regionStartTime.isBefore(endDate)) ||
                (regionEndTime.isAfter(startDate) &&
                    regionEndTime.isBefore(endDate))) &&
            !(regionStartTime.isBefore(startDate) &&
                regionEndTime.isAfter(endDate))) {
          continue;
        }

        int startIndex = _getVisibleDateIndex(_visibleDates, regionStartTime);
        int endIndex = _getVisibleDateIndex(_visibleDates, regionEndTime);

        double startYPosition = _getTimeToPosition(
            Duration(
                hours: regionStartTime.hour, minutes: regionStartTime.minute),
            _timeSlotViewSettings,
            _minuteHeight);
        if (startIndex == -1) {
          if (startDate.isAfter(regionStartTime)) {
            /// Set index as 0 when the region start date before the visible start date
            startIndex = 0;
          } else {
            /// Find the next index when the start date as non working date.
            for (int k = 1; k < _visibleDates.length; k++) {
              final DateTime _currentDate = _visibleDates[k];
              if (_currentDate.isBefore(regionStartTime)) {
                continue;
              }

              startIndex = k;
              break;
            }

            if (startIndex == -1) {
              startIndex = 0;
            }
          }

          /// Start date as non working day and its index as next date index.
          /// so assign the position value as 0
          startYPosition = 0;
        }

        double endYPosition = _getTimeToPosition(
            Duration(hours: regionEndTime.hour, minutes: regionEndTime.minute),
            _timeSlotViewSettings,
            _minuteHeight);
        if (endIndex == -1) {
          /// Find the previous index when the end date as non working date.
          if (endDate.isAfter(regionEndTime)) {
            for (int k = _visibleDates.length - 2; k >= 0; k--) {
              final DateTime _currentDate = _visibleDates[k];
              if (_currentDate.isAfter(regionEndTime)) {
                continue;
              }

              endIndex = k;
              break;
            }

            if (endIndex == -1) {
              endIndex = _visibleDates.length - 1;
            }
          } else {
            /// Set index as visible date end date index when the
            /// region end date before the visible end date
            endIndex = _visibleDates.length - 1;
          }

          /// End date as non working day and its index as previous date index.
          /// so assign the position value as view height
          endYPosition = size.height;
        }

        final TextStyle textStyle = region.textStyle ??
            TextStyle(
                color: _calendarTheme.brightness != null &&
                        _calendarTheme.brightness == Brightness.dark
                    ? Colors.white54
                    : Colors.black45);
        for (int j = startIndex; j <= endIndex; j++) {
          final double startPosition = j == startIndex ? startYPosition : 0;
          final double endPosition = j == endIndex ? endYPosition : size.height;

          /// Check the start and end position not between the visible hours
          /// position(not between start and end hour)
          if ((startPosition <= 0 && endPosition <= 0) ||
              (startPosition >= size.height && endPosition >= size.height) ||
              (startPosition == endPosition)) {
            continue;
          }

          double startXPosition = _timeLabelWidth + (j * _cellWidth);
          if (_isRTL) {
            startXPosition = size.width - (startXPosition + _cellWidth);
          }

          final Rect rect = Rect.fromLTRB(startXPosition, startPosition,
              startXPosition + _cellWidth, endPosition);
          canvas.drawRect(rect, _linePainter);
          if ((region.text == null || region.text.isEmpty) &&
              region.iconData == null) {
            continue;
          }

          painter.textDirection = TextDirection.ltr;
          painter.textAlign = _isRTL ? TextAlign.right : TextAlign.left;
          if (region.iconData == null) {
            painter.text = TextSpan(text: region.text, style: textStyle);
            painter.ellipsis = '..';
          } else {
            painter.text = TextSpan(
                text: String.fromCharCode(region.iconData.codePoint),
                style:
                    textStyle.copyWith(fontFamily: region.iconData.fontFamily));
          }

          painter.layout(minWidth: 0, maxWidth: rect.width - 4);
          painter.paint(canvas, Offset(rect.left + 3, rect.top + 3));
        }
      }
    }

    double x, y;
    y = _timeIntervalHeight;
    _linePainter.style = PaintingStyle.stroke;
    _linePainter.strokeWidth = 0.5;
    _linePainter.strokeCap = StrokeCap.round;
    _linePainter.color = _cellBorderColor ?? _calendarTheme.cellBorderColor;

    for (int i = 1; i <= _horizontalLinesCount; i++) {
      final Offset _start = Offset(_isRTL ? 0 : _timeLabelWidth, y);
      final Offset _end =
          Offset(_isRTL ? size.width - _timeLabelWidth : size.width, y);
      canvas.drawLine(_start, _end, _linePainter);

      y += _timeIntervalHeight;
      if (y == size.height) {
        break;
      }
    }

    if (_isRTL) {
      x = _cellWidth;
    } else {
      x = _timeLabelWidth + _cellWidth;
    }

    for (int i = 0; i < _visibleDates.length - 1; i++) {
      final Offset _start = Offset(x, 0);
      final Offset _end = Offset(x, size.height);
      canvas.drawLine(_start, _end, _linePainter);
      x += _cellWidth;
    }

    if (_calendarCellNotifier.value != null) {
      final double _left =
          (_calendarCellNotifier.value.dx ~/ _cellWidth) * _cellWidth;
      final double _top =
          (_calendarCellNotifier.value.dy ~/ _timeIntervalHeight) *
              _timeIntervalHeight;
      _linePainter.style = PaintingStyle.stroke;
      _linePainter.strokeWidth = 2;
      _linePainter.color = _calendarTheme.selectionBorderColor.withOpacity(0.4);
      canvas.drawRect(
          Rect.fromLTWH(_left + (_isRTL ? 0 : _timeLabelWidth), _top,
              _cellWidth, _timeIntervalHeight),
          _linePainter);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final _TimeSlotView oldWidget = oldDelegate;
    return oldWidget._visibleDates != _visibleDates ||
        oldWidget._timeIntervalHeight != _timeIntervalHeight ||
        oldWidget._timeLabelWidth != _timeLabelWidth ||
        oldWidget._cellBorderColor != _cellBorderColor ||
        oldWidget._horizontalLinesCount != _horizontalLinesCount ||
        oldWidget._calendarTheme != _calendarTheme ||
        oldWidget.specialRegion != specialRegion ||
        oldWidget._isRTL != _isRTL;
  }

  List<CustomPainterSemantics> _getSemanticsBuilder(Size size) {
    final List<CustomPainterSemantics> _semanticsBuilder =
        <CustomPainterSemantics>[];
    double _left, _top, _cellWidth, _cellHeight;
    _top = 0;
    _cellWidth = (size.width - _timeLabelWidth) / _visibleDates.length;
    _left =
        _isRTL ? (size.width - _timeLabelWidth) - _cellWidth : _timeLabelWidth;
    _cellHeight = _timeIntervalHeight;
    final dynamic hour = (_timeSlotViewSettings.startHour -
            _timeSlotViewSettings.startHour.toInt()) *
        60;
    for (int j = 0; j < _visibleDates.length; j++) {
      DateTime date = _visibleDates[j];
      for (int i = 0; i < _horizontalLinesCount; i++) {
        final dynamic minute =
            (i * _getTimeInterval(_timeSlotViewSettings)) + hour;
        date = DateTime(date.year, date.month, date.day,
            _timeSlotViewSettings.startHour.toInt(), minute.toInt());
        _semanticsBuilder.add(CustomPainterSemantics(
          rect: Rect.fromLTWH(_left, _top, _cellWidth, _cellHeight),
          properties: SemanticsProperties(
            label: DateFormat('h a, dd/MMMM/yyyy').format(date).toString(),
            textDirection: TextDirection.ltr,
          ),
        ));
        _top += _cellHeight;
      }

      if (_isRTL) {
        if (_left.round() == _cellWidth.round()) {
          _left = 0;
        } else {
          _left -= _cellWidth;
        }
        _top = 0;
        if (_left < 0) {
          _left = (size.width - _timeLabelWidth) - _cellWidth;
        }
      } else {
        _left += _cellWidth;
        _top = 0;
        if (_left.round() == size.width.round()) {
          _left = _timeLabelWidth;
        }
      }
    }

    return _semanticsBuilder;
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      return _getSemanticsBuilder(size);
    };
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    final _TimeSlotView _oldWidget = oldDelegate;
    return _oldWidget._visibleDates != _visibleDates;
  }
}
