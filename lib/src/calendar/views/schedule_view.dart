part of calendar;

/// Used to store the height and intersection point of scroll view item.
/// intersection point used to identify the view does not have same month dates.
class _ScheduleViewDetails {
  double _height;
  double _intersectPoint;
}

class _ScheduleViewHoveringDetails {
  _ScheduleViewHoveringDetails(this._hoveringDate, this._hoveringOffset);

  final DateTime _hoveringDate;
  final Offset _hoveringOffset;
}

//// Extra small devices (phones, 600px and down)
//// @media only screen and (max-width: 600px) {...}
////
//// Small devices (portrait tablets and large phones, 600px and up)
//// @media only screen and (min-width: 600px) {...}
////
//// Medium devices (landscape tablets, 768px and up)
//// media only screen and (min-width: 768px) {...}
////
//// Large devices (laptops/desktops, 992px and up)
//// media only screen and (min-width: 992px) {...}
////
//// Extra large devices (large laptops and desktops, 1200px and up)
//// media only screen and (min-width: 1200px) {...}
//// Default width to render the mobile UI in web, if the device width exceeds
//// the given width agenda view will render the web UI.
const double _kMobileViewWidth = 767;

/// It is used to generate the week and month label of schedule calendar view.
class _ScheduleLabelPainter extends CustomPainter {
  _ScheduleLabelPainter(
    this._startDate,
    this._endDate,
    this._scheduleViewSettings,
    this._isMonthLabel,
    this._isRTL,
    this._locale,
  );

  final DateTime _startDate;
  final DateTime _endDate;
  final bool _isMonthLabel;
  final bool _isRTL;
  final String _locale;
  final ScheduleViewSettings _scheduleViewSettings;
  TextPainter _textPainter;
  Paint _backgroundPainter;

  @override
  void paint(Canvas canvas, Size size) {
    double xPosition = 5;
    const double yPosition = 0;

    /// Draw the week label.
    if (!_isMonthLabel) {
      final String startDateFormat =
          _scheduleViewSettings.weekHeaderSettings.startDateFormat ?? 'MMM dd';
      String endDateFormat =
          _scheduleViewSettings.weekHeaderSettings.endDateFormat;
      if (_startDate.month == _endDate.month && endDateFormat == null) {
        endDateFormat = 'dd';
      }

      endDateFormat ??= 'MMM dd';
      final String firstDate =
          DateFormat(startDateFormat, _locale).format(_startDate).toString();
      final String endDate =
          DateFormat(endDateFormat, _locale).format(_endDate).toString();
      final TextSpan span = TextSpan(
        text: firstDate + ' - ' + endDate,
        style: _scheduleViewSettings.weekHeaderSettings.weekTextStyle ??
            const TextStyle(
                color: Colors.grey, fontSize: 15, fontFamily: 'Roboto'),
      );
      _backgroundPainter ??= Paint();
      _backgroundPainter.color =
          _scheduleViewSettings.weekHeaderSettings.backgroundColor;

      /// Draw week label background.
      canvas.drawRect(
          Rect.fromLTWH(0, yPosition, size.width,
              _scheduleViewSettings.weekHeaderSettings.height),
          _backgroundPainter);
      _textPainter ??= TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textWidthBasis = TextWidthBasis.longestLine;

      _textPainter.layout(
          minWidth: 0, maxWidth: size.width - 10 > 0 ? size.width - 10 : 0);

      if (_scheduleViewSettings.weekHeaderSettings.textAlign ==
              TextAlign.right ||
          _scheduleViewSettings.weekHeaderSettings.textAlign == TextAlign.end) {
        xPosition = size.width - _textPainter.width;
      } else if (_scheduleViewSettings.weekHeaderSettings.textAlign ==
          TextAlign.center) {
        xPosition = size.width / 2 - _textPainter.width / 2;
      }

      if (_isRTL) {
        xPosition = size.width - _textPainter.width - xPosition;
        if (_scheduleViewSettings.weekHeaderSettings.textAlign ==
                TextAlign.left ||
            _scheduleViewSettings.weekHeaderSettings.textAlign ==
                TextAlign.end) {
          xPosition = 5.0;
        } else if (_scheduleViewSettings.weekHeaderSettings.textAlign ==
            TextAlign.center) {
          xPosition = size.width / 2 - _textPainter.width / 2;
        }
      }

      /// Draw week label text
      _textPainter.paint(
          canvas,
          Offset(
              xPosition,
              yPosition +
                  (_scheduleViewSettings.weekHeaderSettings.height / 2 -
                      _textPainter.height / 2)));
    } else {
      /// Draw the month label
      final String monthFormat =
          _scheduleViewSettings.monthHeaderSettings.monthFormat;
      final TextSpan span = TextSpan(
        text: DateFormat(monthFormat, _locale).format(_startDate).toString(),
        style: _scheduleViewSettings.monthHeaderSettings.monthTextStyle ??
            const TextStyle(
                color: Colors.white, fontSize: 20, fontFamily: 'Roboto'),
      );
      _backgroundPainter ??= Paint();
      _backgroundPainter.shader = null;
      _backgroundPainter.color =
          _scheduleViewSettings.monthHeaderSettings.backgroundColor;
      final Rect rect = Rect.fromLTWH(0, yPosition, size.width,
          _scheduleViewSettings.monthHeaderSettings.height);

      /// Draw month label background.
      canvas.drawRect(rect, _backgroundPainter);
      _textPainter ??= TextPainter();
      _textPainter.text = span;
      _textPainter.maxLines = 1;
      _textPainter.textDirection = TextDirection.ltr;
      _textPainter.textWidthBasis = TextWidthBasis.longestLine;

      _textPainter.layout(
          minWidth: 0, maxWidth: size.width - 10 > 0 ? size.width - 10 : 0);

      final double _viewPadding = size.width * 0.15;
      xPosition = _viewPadding + 5;
      if (_scheduleViewSettings.monthHeaderSettings.textAlign ==
              TextAlign.right ||
          _scheduleViewSettings.monthHeaderSettings.textAlign ==
              TextAlign.end) {
        xPosition = size.width - _textPainter.width;
      } else if (_scheduleViewSettings.monthHeaderSettings.textAlign ==
          TextAlign.center) {
        xPosition = size.width / 2 - _textPainter.width / 2;
      }

      if (_isRTL) {
        xPosition = size.width - _textPainter.width - xPosition;
        if (_scheduleViewSettings.monthHeaderSettings.textAlign ==
                TextAlign.left ||
            _scheduleViewSettings.monthHeaderSettings.textAlign ==
                TextAlign.end) {
          xPosition = 5.0;
        } else if (_scheduleViewSettings.monthHeaderSettings.textAlign ==
            TextAlign.center) {
          xPosition = size.width / 2 - _textPainter.width / 2;
        }
      }

      /// Draw month label text.
      _textPainter.paint(canvas, Offset(xPosition, _textPainter.height));
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  List<CustomPainterSemantics> _getSemanticsBuilder(Size size) {
    final List<CustomPainterSemantics> _semanticsBuilder =
        <CustomPainterSemantics>[];
    double _left, _top, _cellHeight;
    _top = 0;
    _left = 0;
    _cellHeight = 0;
    String accessibilityText;
    if (!_isMonthLabel) {
      _cellHeight = _scheduleViewSettings.weekHeaderSettings.height;
      accessibilityText =
          DateFormat('dd', _locale).format(_startDate).toString() +
              'to' +
              DateFormat('dd MMM', _locale)
                  .format(_endDate.add(const Duration(days: 6)))
                  .toString();
    } else {
      _cellHeight = _scheduleViewSettings.monthHeaderSettings.height;
      accessibilityText =
          DateFormat('MMMM yyyy', _locale).format(_startDate).toString();
    }
    _semanticsBuilder.add(CustomPainterSemantics(
      rect: Rect.fromLTWH(_left, _top, size.width, _cellHeight),
      properties: SemanticsProperties(
        label: accessibilityText,
        textDirection: TextDirection.ltr,
      ),
    ));

    return _semanticsBuilder;
  }

  /// overrides this property to build the semantics information which uses to
  /// return the required information for accessibility, need to return the list
  /// of custom painter semantics which contains the rect area and the semantics
  /// properties for accessibility
  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      return _getSemanticsBuilder(size);
    };
  }

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) {
    return true;
  }
}

/// Used to implement the sticky header in schedule calendar view
/// based on its header and content widget.
class _ScheduleAppointmentView extends Stack {
  _ScheduleAppointmentView({
    Widget content,
    Widget header,
    AlignmentDirectional alignment,
    Key key,
  }) : super(
          key: key,
          children: <Widget>[
            RepaintBoundary(child: content),
            RepaintBoundary(child: header)
          ],
          alignment: alignment ?? AlignmentDirectional.topStart,
          overflow: Overflow.clip,
        );

  @override
  RenderStack createRenderObject(BuildContext context) =>
      _AppointmentViewHeaderRenderObject(
        scrollableState: Scrollable.of(context),
        alignment: alignment,
        textDirection: textDirection ?? Directionality.of(context),
        fit: fit,
      );

  @override
  @mustCallSuper
  void updateRenderObject(BuildContext context, RenderStack renderObject) {
    super.updateRenderObject(context, renderObject);

    if (renderObject is _AppointmentViewHeaderRenderObject) {
      renderObject..scrollableState = Scrollable.of(context);
    }
  }
}

/// Render object of the schedule calendar view item.
class _AppointmentViewHeaderRenderObject extends RenderStack {
  _AppointmentViewHeaderRenderObject({
    ScrollableState scrollableState,
    AlignmentGeometry alignment,
    TextDirection textDirection,
    StackFit fit,
  })  : _scrollableState = scrollableState,
        super(
          alignment: alignment,
          textDirection: textDirection,
          fit: fit,
        );

  /// Used to update the child position when it scroll changed.
  ScrollableState _scrollableState;

  /// Current view port.
  RenderAbstractViewport get _stackViewPort => RenderAbstractViewport.of(this);

  ScrollableState get scrollableState => _scrollableState;

  set scrollableState(ScrollableState newScrollable) {
    final ScrollableState oldScrollable = _scrollableState;
    _scrollableState = newScrollable;

    markNeedsPaint();
    if (attached) {
      oldScrollable.position.removeListener(markNeedsPaint);
      newScrollable.position.addListener(markNeedsPaint);
    }
  }

  /// attach will called when the render object rendered in view.
  @override
  void attach(PipelineOwner owner) {
    super.attach(owner);
    scrollableState.position.addListener(markNeedsPaint);
  }

  /// attach will called when the render object removed from view.
  @override
  void detach() {
    scrollableState.position.removeListener(markNeedsPaint);
    super.detach();
  }

  @override
  void paint(PaintingContext context, Offset paintOffset) {
    /// Update the child position.
    updateHeaderOffset();
    paintStack(context, paintOffset);
  }

  void updateHeaderOffset() {
    /// Content widget height
    final double contentSize = firstChild.size.height;
    final RenderBox _headerView = lastChild;

    /// Header view height
    final double headerSize = _headerView.size.height;

    /// Current view position on scroll view.
    final double viewPosition =
        _stackViewPort.getOffsetToReveal(this, 0).offset;

    /// Calculate the current view offset by view position on scroll view, scrolled position and
    /// scroll view view port.
    final double currentViewOffset =
        viewPosition - _scrollableState.position.pixels - _scrollableHeight;

    /// Check current header offset exits content size, if exist then place the
    /// header at content size.
    final double offset = _getCurrentOffset(currentViewOffset, contentSize);
    final StackParentData headerParentData = _headerView.parentData;
    final double _headerYOffset =
        _getHeaderOffset(contentSize, offset, headerSize);

    /// Update the header start y position.
    if (_headerYOffset != headerParentData.offset.dy) {
      headerParentData.offset =
          Offset(headerParentData.offset.dx, _headerYOffset);
    }
  }

  /// Return the view port height.
  double get _scrollableHeight {
    final dynamic _viewPort = _stackViewPort;
    double viewPortHeight;

    if (_viewPort is RenderBox) {
      viewPortHeight = _viewPort.size.height;
    }

    double anchor = 0;
    if (_viewPort is RenderViewport) {
      anchor = _viewPort.anchor;
    }

    return -viewPortHeight * anchor;
  }

  /// Check current header offset exits content size, if exist then place the
  /// header at content size.
  double _getCurrentOffset(double currentOffset, double contentSize) {
    final double currentHeaderPosition =
        -currentOffset > contentSize ? contentSize : -currentOffset;
    return currentHeaderPosition > 0 ? currentHeaderPosition : 0;
  }

  /// Return current offset value from header size and content size.
  double _getHeaderOffset(
    double contentSize,
    double offset,
    double headerSize,
  ) {
    return headerSize + offset < contentSize
        ? offset
        : contentSize - headerSize;
  }
}
