part of calendar;

class _CalendarView extends StatefulWidget {
  const _CalendarView(
      this._calendar,
      this._visibleDates,
      this._width,
      this._height,
      this._agendaSelectedDate,
      this._locale,
      this._calendarTheme,
      this._regions,
      this._focusNode,
      {Key key,
      this.updateCalendarState})
      : super(key: key);

  final List<DateTime> _visibleDates;
  final List<TimeRegion> _regions;
  final SfCalendar _calendar;
  final double _width;
  final SfCalendarThemeData _calendarTheme;
  final double _height;
  final String _locale;
  final ValueNotifier<DateTime> _agendaSelectedDate;
  final _UpdateCalendarState updateCalendarState;
  final FocusNode _focusNode;

  @override
  _CalendarViewState createState() => _CalendarViewState();
}

class _CalendarViewState extends State<_CalendarView>
    with TickerProviderStateMixin {
  // line count is the total time slot lines to be drawn in the view
  // line count per view is for time line view which contains the time slot count for per view
  double _horizontalLinesCount;

  //// all day scroll controller is used to identify the scrollposition for draw all day selection.
  ScrollController _scrollController, _timelineViewHeaderScrollController;

  // scroll physics to handle the scrolling for time line view
  ScrollPhysics _scrollPhysics;
  _AppointmentPainter _appointmentPainter;
  AnimationController _timelineViewAnimationController;
  Animation<double> _timelineViewAnimation;
  Tween<double> _timelineViewTween;

  //// timeline header is used to implement the sticky view header in horizontal calendar view mode.
  _TimelineViewHeaderView _timelineViewHeader;
  _SelectionPainter _selectionPainter;
  double _allDayHeight = 0;
  int numberOfDaysInWeek;
  double _timeIntervalHeight;
  _UpdateCalendarStateDetails _updateCalendarStateDetails;
  ValueNotifier<_SelectionDetails> _allDaySelectionNotifier;
  ValueNotifier<Offset> _viewHeaderNotifier,
      _calendarCellNotifier,
      _allDayNotifier,
      _appointmentHoverNotifier;
  bool _isRTL;

  bool isExpanded = false;
  DateTime _hoveringDate;
  AnimationController _animationController;
  Animation<double> _heightAnimation;
  Animation<double> _allDayExpanderAnimation;
  AnimationController _expanderAnimationController;

  @override
  void initState() {
    isExpanded = false;
    _hoveringDate = DateTime.now();
    _viewHeaderNotifier = ValueNotifier<Offset>(null)
      ..addListener(_timelineViewHoveringUpdate);
    _calendarCellNotifier = ValueNotifier<Offset>(null)
      ..addListener(_timelineViewHoveringUpdate);
    _allDayNotifier = ValueNotifier<Offset>(null)
      ..addListener(_allDayPanelHoveringUpdate);
    _appointmentHoverNotifier = ValueNotifier<Offset>(null)
      ..addListener(_appointmentHoverUpdate);
    _allDaySelectionNotifier = ValueNotifier<_SelectionDetails>(null);
    if (!_isTimelineView(widget._calendar.view) &&
        widget._calendar.view != CalendarView.month) {
      _animationController = AnimationController(
          duration: const Duration(milliseconds: 200), vsync: this);
      _heightAnimation =
          CurveTween(curve: Curves.easeIn).animate(_animationController)
            ..addListener(() {
              setState(() {});
            });

      _expanderAnimationController = AnimationController(
          duration: const Duration(milliseconds: 100), vsync: this);
      _allDayExpanderAnimation =
          CurveTween(curve: Curves.easeIn).animate(_expanderAnimationController)
            ..addListener(() {
              setState(() {});
            });
    }

    _updateCalendarStateDetails = _UpdateCalendarStateDetails();
    numberOfDaysInWeek = 7;
    _timeIntervalHeight = _getTimeIntervalHeight(
        widget._calendar,
        widget._width,
        widget._height,
        widget._visibleDates.length,
        _allDayHeight);
    if (widget._calendar.view != CalendarView.month) {
      _horizontalLinesCount =
          _getHorizontalLinesCount(widget._calendar.timeSlotViewSettings);
      _scrollController =
          ScrollController(initialScrollOffset: 0, keepScrollOffset: true)
            ..addListener(_scrollListener);
      if (_isTimelineView(widget._calendar.view)) {
        _scrollPhysics = const NeverScrollableScrollPhysics();
        _timelineViewHeaderScrollController =
            ScrollController(initialScrollOffset: 0, keepScrollOffset: true);
        _timelineViewAnimationController = AnimationController(
            duration: const Duration(milliseconds: 300),
            vsync: this,
            animationBehavior: AnimationBehavior.normal);
        _timelineViewTween = Tween<double>(begin: 0.0, end: 0.1);
        _timelineViewAnimation = _timelineViewTween
            .animate(_timelineViewAnimationController)
              ..addListener(_scrollAnimationListener);
      }
    }

    if (widget._calendar.view != CalendarView.month) {
      _scrollToPosition();
    }

    super.initState();
  }

  void _timelineViewHoveringUpdate() {
    if (!_isTimelineView(widget._calendar.view) && mounted) {
      return;
    }

    setState(() {});
  }

  void _appointmentHoverUpdate() {
    if (widget._calendar.view == CalendarView.month) {
      return;
    }

    setState(() {});
  }

  void _allDayPanelHoveringUpdate() {
    if (!_isTimelineView(widget._calendar.view) && mounted) {
      setState(() {});
    }
  }

  void _scrollAnimationListener() {
    _scrollController.jumpTo(_timelineViewAnimation.value);
  }

  void _scrollToPosition() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (widget._calendar.view == CalendarView.month) {
        return;
      }
      _updateCalendarStateDetails._currentDate = null;
      widget.updateCalendarState(_updateCalendarStateDetails);
      double timeToPosition = 0;
      if (isDateWithInDateRange(
          widget._visibleDates[0],
          widget._visibleDates[widget._visibleDates.length - 1],
          _updateCalendarStateDetails._currentDate)) {
        if (!_isTimelineView(widget._calendar.view)) {
          timeToPosition = _timeToPosition(widget._calendar,
              _updateCalendarStateDetails._currentDate, _timeIntervalHeight);
        } else {
          for (int i = 0; i < widget._visibleDates.length; i++) {
            if (isSameDate(_updateCalendarStateDetails._currentDate,
                widget._visibleDates[i])) {
              timeToPosition = (_getSingleViewWidthForTimeLineView(this) * i) +
                  _timeToPosition(
                      widget._calendar,
                      _updateCalendarStateDetails._currentDate,
                      _timeIntervalHeight);
              break;
            }
          }
        }

        if (timeToPosition > _scrollController.position.maxScrollExtent)
          timeToPosition = _scrollController.position.maxScrollExtent;
        else if (timeToPosition < _scrollController.position.minScrollExtent)
          timeToPosition = _scrollController.position.minScrollExtent;

        _scrollController.jumpTo(timeToPosition);
      }
      _updateCalendarStateDetails._currentDate = null;
    });
  }

  void _expandOrCollapseAllDay() {
    isExpanded = !isExpanded;
    if (isExpanded) {
      _expanderAnimationController.forward();
    } else {
      _expanderAnimationController.reverse();
    }
  }

  @override
  void dispose() {
    if (_isTimelineView(widget._calendar.view) &&
        _timelineViewAnimationController != null)
      _timelineViewAnimationController.dispose();
    if (_scrollController != null) {
      _scrollController.dispose();
    }
    if (_timelineViewHeaderScrollController != null)
      _timelineViewHeaderScrollController.dispose();
    if (_animationController != null) {
      _animationController.dispose();
      _animationController = null;
    }

    if (_expanderAnimationController != null) {
      _expanderAnimationController.dispose();
      _expanderAnimationController = null;
    }

    super.dispose();
  }

  void _scrollListener() {
    if (_isTimelineView(widget._calendar.view)) {
      _updateCalendarStateDetails._selectedDate = null;
      _updateCalendarStateDetails._currentViewVisibleDates = null;
      widget.updateCalendarState(_updateCalendarStateDetails);
      if (_scrollController.position.atEdge &&
          _updateCalendarStateDetails._currentViewVisibleDates ==
              widget._visibleDates) {
        setState(() {
          _scrollPhysics = const NeverScrollableScrollPhysics();
        });
      } else if ((!_scrollController.position.atEdge) &&
          _scrollPhysics.toString() == 'NeverScrollableScrollPhysics' &&
          _updateCalendarStateDetails._currentViewVisibleDates ==
              widget._visibleDates) {
        setState(() {
          _scrollPhysics = const ClampingScrollPhysics();
        });
      }

      if (_timelineViewHeader != null) {
        _timelineViewHeader._repaintNotifier.value =
            !_timelineViewHeader._repaintNotifier.value;
      }

      _timelineViewHeaderScrollController.jumpTo(_scrollController.offset);
    }
  }

  bool _isUpdateTimelineViewScroll(double _initial, double _dx) {
    if (_scrollController.position.maxScrollExtent == 0) {
      return false;
    }

    double _scrollToPosition = 0;

    if (_scrollController.offset < _scrollController.position.maxScrollExtent &&
        _scrollController.offset <=
            _scrollController.position.viewportDimension &&
        (_isRTL ? _initial < _dx : _initial > _dx)) {
      setState(() {
        _scrollPhysics = const ClampingScrollPhysics();
      });

      _scrollToPosition = _isRTL ? _dx - _initial : _initial - _dx;
      _scrollToPosition =
          _scrollToPosition <= _scrollController.position.maxScrollExtent
              ? _scrollToPosition
              : _scrollController.position.maxScrollExtent;

      _scrollController.jumpTo(_scrollToPosition);
      return true;
    } else if (_scrollController.offset >
            _scrollController.position.minScrollExtent &&
        _scrollController.offset != 0 &&
        (_isRTL ? _initial > _dx : _initial < _dx)) {
      setState(() {
        _scrollPhysics = const ClampingScrollPhysics();
      });
      _scrollToPosition = _isRTL
          ? _scrollController.position.maxScrollExtent - (_initial - _dx)
          : _scrollController.position.maxScrollExtent - (_dx - _initial);
      _scrollToPosition =
          _scrollToPosition >= _scrollController.position.minScrollExtent
              ? _scrollToPosition
              : _scrollController.position.minScrollExtent;
      _scrollController.jumpTo(_scrollToPosition);
      return true;
    }

    return false;
  }

  bool _isAnimateTimelineViewScroll(double _position, double _velocity) {
    if (_scrollController.position.maxScrollExtent == 0) {
      return false;
    }

    _velocity =
        _velocity == 0 ? _position.abs() : _velocity / window.devicePixelRatio;
    if (_scrollController.offset < _scrollController.position.maxScrollExtent &&
        _scrollController.offset <=
            _scrollController.position.viewportDimension &&
        (_isRTL ? _position > 0 : _position < 0)) {
      setState(() {
        _scrollPhysics = const ClampingScrollPhysics();
      });

      // animation to animate the scroll view manually in time line view
      _timelineViewTween.begin = _scrollController.offset;
      _timelineViewTween.end =
          _velocity.abs() > _scrollController.position.maxScrollExtent
              ? _scrollController.position.maxScrollExtent
              : _velocity.abs();

      if (_timelineViewAnimationController.isCompleted &&
          _scrollController.offset != _timelineViewTween.end)
        _timelineViewAnimationController.reset();

      _timelineViewAnimationController.forward();
      return true;
    } else if (_scrollController.offset >
            _scrollController.position.minScrollExtent &&
        _scrollController.offset != 0 &&
        (_isRTL ? _position < 0 : _position > 0)) {
      setState(() {
        _scrollPhysics = const ClampingScrollPhysics();
      });

      // animation to animate the scroll view manually in time line view
      _timelineViewTween.begin = _scrollController.offset;
      _timelineViewTween.end =
          _scrollController.position.maxScrollExtent - _velocity.abs() <
                  _scrollController.position.minScrollExtent
              ? _scrollController.position.minScrollExtent
              : _scrollController.position.maxScrollExtent - _velocity.abs();

      if (_timelineViewAnimationController.isCompleted &&
          _scrollController.offset != _timelineViewTween.end)
        _timelineViewAnimationController.reset();

      _timelineViewAnimationController.forward();
      return true;
    }

    return false;
  }

  @override
  void didUpdateWidget(_CalendarView oldWidget) {
    if (widget._calendar.view != CalendarView.month) {
      _allDaySelectionNotifier ??= ValueNotifier<_SelectionDetails>(null);
      if (!_isTimelineView(widget._calendar.view)) {
        _animationController ??= AnimationController(
            duration: const Duration(milliseconds: 200), vsync: this);
        _heightAnimation ??=
            CurveTween(curve: Curves.easeIn).animate(_animationController)
              ..addListener(() {
                setState(() {});
              });

        _updateCalendarStateDetails ??= _UpdateCalendarStateDetails();
        _expanderAnimationController ??= AnimationController(
            duration: const Duration(milliseconds: 100), vsync: this);
        _allDayExpanderAnimation ??= CurveTween(curve: Curves.easeIn)
            .animate(_expanderAnimationController)
              ..addListener(() {
                setState(() {});
              });

        if (widget._calendar.view != CalendarView.day && _allDayHeight == 0) {
          if (_animationController.status == AnimationStatus.completed) {
            _animationController.reset();
          }

          _animationController.forward();
        }
      }

      if (widget._calendar.timeSlotViewSettings.startHour !=
              oldWidget._calendar.timeSlotViewSettings.startHour ||
          widget._calendar.timeSlotViewSettings.endHour !=
              oldWidget._calendar.timeSlotViewSettings.endHour ||
          _getTimeInterval(widget._calendar.timeSlotViewSettings) !=
              _getTimeInterval(oldWidget._calendar.timeSlotViewSettings))
        _horizontalLinesCount =
            _getHorizontalLinesCount(widget._calendar.timeSlotViewSettings);
      else if (oldWidget._calendar.view == CalendarView.month)
        _horizontalLinesCount =
            _getHorizontalLinesCount(widget._calendar.timeSlotViewSettings);
      else
        _horizontalLinesCount = _horizontalLinesCount ??
            _getHorizontalLinesCount(widget._calendar.timeSlotViewSettings);

      _scrollController = _scrollController ??
          ScrollController(initialScrollOffset: 0, keepScrollOffset: true)
        ..addListener(_scrollListener);

      if (_isTimelineView(widget._calendar.view)) {
        _scrollPhysics = const NeverScrollableScrollPhysics();

        _timelineViewAnimationController = _timelineViewAnimationController ??
            AnimationController(
                duration: const Duration(milliseconds: 300),
                vsync: this,
                animationBehavior: AnimationBehavior.normal);
        _timelineViewTween =
            _timelineViewTween ?? Tween<double>(begin: 0.0, end: 0.1);

        _timelineViewAnimation = _timelineViewAnimation ??
            _timelineViewTween.animate(_timelineViewAnimationController)
          ..addListener(_scrollAnimationListener);

        _timelineViewHeaderScrollController =
            _timelineViewHeaderScrollController ??
                ScrollController(
                    initialScrollOffset: 0, keepScrollOffset: true);
      }
    }

    /// Update the scroll position with following scenarios
    /// 1. View changed from month or schedule view.
    /// 2. View changed from timeline view(timeline day, timeline week,
    /// timeline work week) to timeslot view(day, week, work week).
    /// 3. View changed from timeslot view(day, week, work week) to
    /// timeline view(timeline day, timeline week, timeline work week).
    ///
    /// This condition used to restrict the following scenarios
    /// 1. View changed to month view.
    /// 2. View changed with in the day, week, work week
    /// (eg., view changed to week from day).
    /// 3. View changed with in the timeline day, timeline week, timeline
    /// work week(eg., view changed to timeline week from timeline day).
    if ((oldWidget._calendar.view == CalendarView.month ||
            oldWidget._calendar.view == CalendarView.schedule ||
            (!_isTimelineView(oldWidget._calendar.view) &&
                _isTimelineView(widget._calendar.view)) ||
            (_isTimelineView(oldWidget._calendar.view) &&
                !_isTimelineView(widget._calendar.view))) &&
        widget._calendar.view != CalendarView.month) {
      _scrollToPosition();
    }

    _timeIntervalHeight = _getTimeIntervalHeight(
        widget._calendar,
        widget._width,
        widget._height,
        widget._visibleDates.length,
        _allDayHeight);

    if (oldWidget._calendar.view != widget._calendar.view &&
        widget._calendar.view != CalendarView.month &&
        _selectionPainter._appointmentView != null) {
      _updateSelection();
    }

    super.didUpdateWidget(oldWidget);
  }

  /// Retains the selection of an appointment when switching between views.
  void _updateSelection() {
    final _AppointmentView _selectedAppointmentView =
        _selectionPainter._appointmentView;
    _selectionPainter._appointmentView = null;
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (widget._visibleDates !=
          _updateCalendarStateDetails._currentViewVisibleDates) {
        return;
      }
      for (int i = 0;
          i < _appointmentPainter._appointmentCollection.length;
          i++) {
        if (_selectedAppointmentView.appointment ==
            _appointmentPainter._appointmentCollection[i].appointment) {
          _selectionPainter._appointmentView =
              _appointmentPainter._appointmentCollection[i];
          break;
        }
      }
    });
  }

  dynamic _updatePainterProperties(_UpdateCalendarStateDetails details) {
    _updateCalendarStateDetails._allDayAppointmentViewCollection = null;
    _updateCalendarStateDetails._currentViewVisibleDates = null;
    _updateCalendarStateDetails._visibleAppointments = null;
    _updateCalendarStateDetails._selectedDate = null;
    widget.updateCalendarState(_updateCalendarStateDetails);

    details._allDayAppointmentViewCollection =
        _updateCalendarStateDetails._allDayAppointmentViewCollection;
    details._currentViewVisibleDates =
        _updateCalendarStateDetails._currentViewVisibleDates;
    details._visibleAppointments =
        _updateCalendarStateDetails._visibleAppointments;
    details._selectedDate = _updateCalendarStateDetails._selectedDate;
  }

  Widget _addAllDayAppointmentPanel(SfCalendarThemeData _calendarTheme) {
    _updateCalendarStateDetails._currentViewVisibleDates = null;
    _updateCalendarStateDetails._visibleAppointments = null;
    _updateCalendarStateDetails._allDayPanelHeight = null;
    _updateCalendarStateDetails._allDayAppointmentViewCollection = null;
    widget.updateCalendarState(_updateCalendarStateDetails);
    final Color borderColor =
        widget._calendar.cellBorderColor ?? _calendarTheme.cellBorderColor;
    final Widget _shadowView = Divider(
      height: 1,
      thickness: 1,
      color: borderColor.withOpacity(borderColor.opacity * 0.5),
    );

    final double timeLabelWidth = _getTimeLabelWidth(
        widget._calendar.timeSlotViewSettings.timeRulerSize,
        widget._calendar.view);
    double topPosition = _getViewHeaderHeight(
        widget._calendar.viewHeaderHeight, widget._calendar.view);
    if (widget._calendar.view == CalendarView.day) {
      topPosition = _allDayHeight;
    }

    if (_allDayHeight == 0 ||
        (widget._calendar.view != CalendarView.day &&
            widget._visibleDates !=
                _updateCalendarStateDetails._currentViewVisibleDates)) {
      return Positioned(
          left: 0, right: 0, top: topPosition, height: 1, child: _shadowView);
    }

    double _leftPosition = timeLabelWidth;
    if (widget._calendar.view == CalendarView.day) {
      //// Default minimum view header width in day view as 50,so set 50
      //// when view header width less than 50.
      _leftPosition = _leftPosition < 50 ? 50 : _leftPosition;
      topPosition = 0;
    }

    double _panelHeight =
        _updateCalendarStateDetails._allDayPanelHeight - _allDayHeight;
    if (_panelHeight < 0) {
      _panelHeight = 0;
    }

    final double _alldayExpanderHeight =
        _allDayHeight + (_panelHeight * _allDayExpanderAnimation.value);
    return Positioned(
      left: 0,
      top: topPosition,
      right: 0,
      height: _alldayExpanderHeight,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 0,
            top: 0,
            right: 0,
            height: isExpanded ? _alldayExpanderHeight : _allDayHeight,
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              padding: const EdgeInsets.all(0.0),
              children: <Widget>[
                CustomPaint(
                  painter: _AllDayAppointmentPainter(
                      widget._calendar,
                      widget._visibleDates,
                      widget._visibleDates ==
                              _updateCalendarStateDetails
                                  ._currentViewVisibleDates
                          ? _updateCalendarStateDetails._visibleAppointments
                          : null,
                      timeLabelWidth,
                      _alldayExpanderHeight,
                      _updateCalendarStateDetails._allDayPanelHeight >
                              _allDayHeight &&
                          (_heightAnimation.value == 1 ||
                              widget._calendar.view == CalendarView.day),
                      _allDayExpanderAnimation.value != 0.0 &&
                          _allDayExpanderAnimation.value != 1,
                      _isRTL,
                      widget._calendarTheme,
                      _allDaySelectionNotifier,
                      _allDayNotifier.value, updateCalendarState:
                          (_UpdateCalendarStateDetails details) {
                    _updatePainterProperties(details);
                  }),
                  size: Size(
                      widget._width,
                      widget._calendar.view == CalendarView.day &&
                              _updateCalendarStateDetails._allDayPanelHeight <
                                  _allDayHeight
                          ? _allDayHeight
                          : _updateCalendarStateDetails._allDayPanelHeight),
                ),
              ],
            ),
          ),
          Positioned(
              left: 0,
              top: _alldayExpanderHeight - 1,
              right: 0,
              height: 1,
              child: _shadowView),
        ],
      ),
    );
  }

  _AppointmentPainter _addAppointmentPainter() {
    _updateCalendarStateDetails._currentViewVisibleDates = null;
    _updateCalendarStateDetails._visibleAppointments = null;
    widget.updateCalendarState(_updateCalendarStateDetails);
    _appointmentPainter = _AppointmentPainter(
        widget._calendar,
        widget._visibleDates,
        widget._visibleDates ==
                _updateCalendarStateDetails._currentViewVisibleDates
            ? _updateCalendarStateDetails._visibleAppointments
            : null,
        _timeIntervalHeight,
        ValueNotifier<bool>(false),
        widget._calendarTheme,
        _isRTL,
        _appointmentHoverNotifier.value,
        updateCalendarState: (_UpdateCalendarStateDetails details) {
      _updatePainterProperties(details);
    });

    return _appointmentPainter;
  }

  // Returns the month view  as a child for the calendar view.
  Widget _addMonthView(bool _isRTL, String _locale) {
    final double viewHeaderHeight = _getViewHeaderHeight(
        widget._calendar.viewHeaderHeight, widget._calendar.view);
    final double height = widget._height - viewHeaderHeight;
    return Stack(
      children: <Widget>[
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          height: viewHeaderHeight,
          child: Container(
            color: widget._calendar.viewHeaderStyle.backgroundColor ??
                widget._calendarTheme.viewHeaderBackgroundColor,
            child: RepaintBoundary(
              child: CustomPaint(
                painter: _ViewHeaderViewPainter(
                    widget._visibleDates,
                    widget._calendar.view,
                    widget._calendar.viewHeaderStyle,
                    widget._calendar.timeSlotViewSettings,
                    _getTimeLabelWidth(
                        widget._calendar.timeSlotViewSettings.timeRulerSize,
                        widget._calendar.view),
                    _getViewHeaderHeight(widget._calendar.viewHeaderHeight,
                        widget._calendar.view),
                    widget._calendar.monthViewSettings,
                    _isRTL,
                    widget._locale,
                    widget._calendarTheme,
                    widget._calendar.todayHighlightColor ??
                        widget._calendarTheme.todayHighlightColor,
                    widget._calendar.cellBorderColor,
                    widget._calendar.minDate,
                    widget._calendar.maxDate,
                    _viewHeaderNotifier),
              ),
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: viewHeaderHeight,
          right: 0,
          bottom: 0,
          child: RepaintBoundary(
            child: CustomPaint(
              painter: _MonthCellPainter(
                  widget._visibleDates,
                  widget._calendar.monthViewSettings.numberOfWeeksInView,
                  widget._calendar.monthViewSettings.monthCellStyle,
                  _isRTL,
                  widget._calendar.todayHighlightColor ??
                      widget._calendarTheme.todayHighlightColor,
                  widget._calendar.cellBorderColor,
                  widget._calendarTheme,
                  _calendarCellNotifier,
                  widget._calendar.minDate,
                  widget._calendar.maxDate),
              size: Size(widget._width, height),
              foregroundPainter: _addAppointmentPainter(),
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: viewHeaderHeight,
          right: 0,
          bottom: 0,
          child: RepaintBoundary(
            child: CustomPaint(
              painter: _addSelectionView(),
              size: Size(widget._width, height),
            ),
          ),
        ),
      ],
    );
  }

  // Returns the day view as a child for the calendar view.
  Widget _addDayView(double width, double height, bool _isRTL, String _locale) {
    double viewHeaderWidth = widget._width;
    double viewHeaderHeight = _getViewHeaderHeight(
        widget._calendar.viewHeaderHeight, widget._calendar.view);
    final double timeLabelWidth = _getTimeLabelWidth(
        widget._calendar.timeSlotViewSettings.timeRulerSize,
        widget._calendar.view);
    if (widget._calendar.view == null ||
        widget._calendar.view == CalendarView.day) {
      viewHeaderWidth = timeLabelWidth < 50 ? 50 : timeLabelWidth;
      viewHeaderHeight =
          _allDayHeight > viewHeaderHeight ? _allDayHeight : viewHeaderHeight;
    }

    double _panelHeight =
        _updateCalendarStateDetails._allDayPanelHeight - _allDayHeight;
    if (_panelHeight < 0) {
      _panelHeight = 0;
    }

    final double _alldayExpanderHeight =
        _panelHeight * _allDayExpanderAnimation.value;
    return Stack(
      children: <Widget>[
        Positioned(
          left: _isRTL ? widget._width - viewHeaderWidth : 0,
          top: 0,
          right: _isRTL ? 0 : widget._width - viewHeaderWidth,
          height: _getViewHeaderHeight(
              widget._calendar.viewHeaderHeight, widget._calendar.view),
          child: Container(
            color: widget._calendar.viewHeaderStyle.backgroundColor ??
                widget._calendarTheme.viewHeaderBackgroundColor,
            child: RepaintBoundary(
              child: CustomPaint(
                painter: _ViewHeaderViewPainter(
                    widget._visibleDates,
                    widget._calendar.view,
                    widget._calendar.viewHeaderStyle,
                    widget._calendar.timeSlotViewSettings,
                    _getTimeLabelWidth(
                        widget._calendar.timeSlotViewSettings.timeRulerSize,
                        widget._calendar.view),
                    _getViewHeaderHeight(widget._calendar.viewHeaderHeight,
                        widget._calendar.view),
                    widget._calendar.monthViewSettings,
                    _isRTL,
                    widget._locale,
                    widget._calendarTheme,
                    widget._calendar.todayHighlightColor ??
                        widget._calendarTheme.todayHighlightColor,
                    widget._calendar.cellBorderColor,
                    widget._calendar.minDate,
                    widget._calendar.maxDate,
                    _viewHeaderNotifier),
              ),
            ),
          ),
        ),
        _addAllDayAppointmentPanel(widget._calendarTheme),
        Positioned(
          top: (widget._calendar.view == CalendarView.day)
              ? viewHeaderHeight + _alldayExpanderHeight
              : viewHeaderHeight + _allDayHeight + _alldayExpanderHeight,
          left: 0,
          right: 0,
          bottom: 0,
          child: ListView(
              padding: const EdgeInsets.all(0.0),
              controller: _scrollController,
              scrollDirection: Axis.vertical,
              physics: const ClampingScrollPhysics(),
              children: <Widget>[
                Stack(children: <Widget>[
                  RepaintBoundary(
                    child: CustomPaint(
                      painter: _TimeSlotView(
                          widget._visibleDates,
                          _horizontalLinesCount,
                          _timeIntervalHeight,
                          timeLabelWidth,
                          widget._calendar.cellBorderColor,
                          widget._calendarTheme,
                          widget._calendar.timeSlotViewSettings,
                          _isRTL,
                          widget._regions,
                          _calendarCellNotifier),
                      size: Size(width, height),
                      foregroundPainter: _addAppointmentPainter(),
                    ),
                  ),
                  RepaintBoundary(
                    child: CustomPaint(
                      painter: _TimeRulerView(
                          _horizontalLinesCount,
                          _timeIntervalHeight,
                          widget._calendar.timeSlotViewSettings,
                          widget._calendar.cellBorderColor,
                          _isRTL,
                          widget._locale,
                          widget._calendarTheme),
                      size: Size(timeLabelWidth, height),
                    ),
                  ),
                  RepaintBoundary(
                    child: CustomPaint(
                      painter: _addSelectionView(),
                      size: Size(width, height),
                    ),
                  ),
                ])
              ]),
        ),
      ],
    );
  }

  // Returns the timeline view  as a child for the calendar view.
  Widget _addTimelineView(double width, double height, String _locale) {
    return Stack(children: <Widget>[
      Positioned(
        top: 0,
        left: 0,
        right: 0,
        height: _getViewHeaderHeight(
            widget._calendar.viewHeaderHeight, widget._calendar.view),
        child: Container(
          color: widget._calendar.viewHeaderStyle.backgroundColor ??
              widget._calendarTheme.viewHeaderBackgroundColor,
          child: _getTimelineViewHeader(
              width,
              _getViewHeaderHeight(
                  widget._calendar.viewHeaderHeight, widget._calendar.view),
              widget._locale),
        ),
      ),
      Positioned(
        top: _getViewHeaderHeight(
            widget._calendar.viewHeaderHeight, widget._calendar.view),
        left: 0,
        right: 0,
        bottom: 0,
        child: ListView(
            padding: const EdgeInsets.all(0.0),
            controller: _scrollController,
            scrollDirection: Axis.horizontal,
            physics: _scrollPhysics,
            children: <Widget>[
              Stack(children: <Widget>[
                RepaintBoundary(
                  child: CustomPaint(
                    painter: _TimelineView(
                        _horizontalLinesCount,
                        widget._visibleDates,
                        _getTimeLabelWidth(
                            widget._calendar.timeSlotViewSettings.timeRulerSize,
                            widget._calendar.view),
                        widget._calendar.timeSlotViewSettings,
                        _timeIntervalHeight,
                        widget._calendar.cellBorderColor,
                        _isRTL,
                        widget._locale,
                        widget._calendarTheme,
                        _calendarCellNotifier,
                        _scrollController,
                        widget._regions),
                    size: Size(width, height),
                    foregroundPainter: _addAppointmentPainter(),
                  ),
                ),
                RepaintBoundary(
                  child: CustomPaint(
                    painter: _addSelectionView(),
                    size: Size(width, height),
                  ),
                ),
              ]),
            ]),
      ),
    ]);
  }

  //// Handles the onTap callback for month cells, and view header of month
  void _handleOnTapForMonth(TapUpDetails details) {
    _handleTouchOnMonthView(details, null);
  }

  /// Handles the tap and long press related functions for month view.
  void _handleTouchOnMonthView(
      TapUpDetails tapDetails, LongPressStartDetails longPressDetails) {
    double _xDetails, _yDetails;
    bool _isTapCallback = false;
    if (tapDetails != null) {
      _isTapCallback = true;
      _xDetails = tapDetails.localPosition.dx;
      _yDetails = tapDetails.localPosition.dy;
    } else if (longPressDetails != null) {
      _xDetails = longPressDetails.localPosition.dx;
      _yDetails = longPressDetails.localPosition.dy;
    }

    final double _viewHeaderHeight = _getViewHeaderHeight(
        widget._calendar.viewHeaderHeight, widget._calendar.view);
    if (_yDetails < _viewHeaderHeight) {
      if (_isTapCallback &&
          _shouldRaiseCalendarTapCallback(widget._calendar.onTap)) {
        _handleOnTapForViewHeader(tapDetails, widget._width);
      } else if (!_isTapCallback &&
          _shouldRaiseCalendarLongPressCallback(widget._calendar.onLongPress)) {
        _handleOnLongPressForViewHeader(longPressDetails, widget._width);
      }
    } else if (_yDetails > _viewHeaderHeight) {
      if (!widget._focusNode.hasFocus) {
        widget._focusNode.requestFocus();
      }

      _drawSelection(_xDetails, _yDetails - _viewHeaderHeight, 0);

      _updateCalendarStateDetails._appointments = null;
      widget.updateCalendarState(_updateCalendarStateDetails);

      if ((!_isTapCallback &&
              _shouldRaiseCalendarLongPressCallback(
                  widget._calendar.onLongPress)) ||
          (_isTapCallback &&
              _shouldRaiseCalendarTapCallback(widget._calendar.onTap))) {
        final DateTime _selectedDate =
            _getDateFromPosition(_xDetails, _yDetails - _viewHeaderHeight, 0);

        if (!isDateWithInDateRange(widget._calendar.minDate,
            widget._calendar.maxDate, _selectedDate)) {
          return;
        }

        if (_isTapCallback) {
          _raiseCalendarTapCallback(widget._calendar,
              date: _selectedDate,
              appointments: _getSelectedAppointments(),
              element: CalendarElement.calendarCell);
        } else {
          _raiseCalendarLongPressCallback(widget._calendar,
              date: _selectedDate,
              appointments: _getSelectedAppointments(),
              element: CalendarElement.calendarCell);
        }
      }
    }
  }

  //// Handles the onLongPress callback for month cells, and view header of month.
  void _handleOnLongPressForMonth(LongPressStartDetails details) {
    _handleTouchOnMonthView(null, details);
  }

  //// Handles the onTap callback for timeline view cells, and view header of timeline.
  void _handleOnTapForTimeline(TapUpDetails details) {
    _handleTouchOnTimeline(details, null);
  }

  /// Handles the tap and long press related functions for timeline view.
  void _handleTouchOnTimeline(
      TapUpDetails tapDetails, LongPressStartDetails longPressDetails) {
    double _xDetails, _yDetails;
    bool _isTapCallback = false;
    if (tapDetails != null) {
      _isTapCallback = true;
      _xDetails = tapDetails.localPosition.dx;
      _yDetails = tapDetails.localPosition.dy;
    } else if (longPressDetails != null) {
      _xDetails = longPressDetails.localPosition.dx;
      _yDetails = longPressDetails.localPosition.dy;
    }

    final double _viewHeaderHeight = _getViewHeaderHeight(
        widget._calendar.viewHeaderHeight, widget._calendar.view);

    if (_yDetails < _viewHeaderHeight) {
      if (_isTapCallback &&
          _shouldRaiseCalendarTapCallback(widget._calendar.onTap)) {
        _handleOnTapForViewHeader(tapDetails, widget._width);
      } else if (!_isTapCallback &&
          _shouldRaiseCalendarLongPressCallback(widget._calendar.onLongPress)) {
        _handleOnLongPressForViewHeader(longPressDetails, widget._width);
      }
    } else if (_yDetails > _viewHeaderHeight) {
      if (!widget._focusNode.hasFocus) {
        widget._focusNode.requestFocus();
      }

      _updateCalendarStateDetails._selectedDate = null;
      _updateCalendarStateDetails._currentViewVisibleDates = null;
      _updateCalendarStateDetails._allDayAppointmentViewCollection = null;
      widget.updateCalendarState(_updateCalendarStateDetails);
      DateTime _selectedDate = _updateCalendarStateDetails._selectedDate;

      double xPosition = _scrollController.offset + _xDetails;
      final double yPosition = _yDetails - _viewHeaderHeight;
      final double _timeLabelWidth = _getTimeLabelWidth(
          widget._calendar.timeSlotViewSettings.timeRulerSize,
          widget._calendar.view);

      if (yPosition < _timeLabelWidth) {
        return;
      }

      if (_isRTL) {
        xPosition = _scrollController.offset +
            (_scrollController.position.viewportDimension - _xDetails);
        xPosition = (_scrollController.position.viewportDimension +
                _scrollController.position.maxScrollExtent) -
            xPosition;
      }
      final _AppointmentView _appointmentView = _getAppointmentOnPoint(
          _appointmentPainter._appointmentCollection, xPosition, yPosition);
      if (_appointmentView == null) {
        _drawSelection(
            _xDetails, yPosition - _viewHeaderHeight, _timeLabelWidth);
        _selectedDate = _selectionPainter._selectedDate;
      } else {
        if (_selectedDate != null) {
          _selectedDate = null;
          _selectionPainter._selectedDate = _selectedDate;
          _updateCalendarStateDetails._selectedDate = _selectedDate;
          _updateCalendarStateDetails._isAppointmentTapped = true;
        }

        _selectionPainter._appointmentView = _appointmentView;
        _selectionPainter._repaintNotifier.value =
            !_selectionPainter._repaintNotifier.value;
      }

      _updateCalendarStateDetails._appointments = null;
      widget.updateCalendarState(_updateCalendarStateDetails);

      if ((!_isTapCallback &&
              _shouldRaiseCalendarLongPressCallback(
                  widget._calendar.onLongPress)) ||
          (_isTapCallback &&
              _shouldRaiseCalendarTapCallback(widget._calendar.onTap))) {
        if (_appointmentView == null) {
          final DateTime _selectedDate =
              _getDateFromPosition(_xDetails, _yDetails - _viewHeaderHeight, 0);

          if (!isDateWithInDateRange(widget._calendar.minDate,
              widget._calendar.maxDate, _selectedDate)) {
            return;
          }

          /// Restrict the callback, while selected region as disabled [TimeRegion].
          if (!_isEnabledRegion(_xDetails, _selectedDate)) {
            return;
          }

          if (_isTapCallback) {
            _raiseCalendarTapCallback(widget._calendar,
                date: _selectedDate,
                appointments: null,
                element: CalendarElement.calendarCell);
          } else {
            _raiseCalendarLongPressCallback(widget._calendar,
                date: _selectedDate,
                appointments: null,
                element: CalendarElement.calendarCell);
          }
        } else {
          if (_isTapCallback) {
            _raiseCalendarTapCallback(widget._calendar,
                date: _appointmentView.appointment.startTime,
                appointments: <dynamic>[
                  _appointmentView.appointment._data ??
                      _appointmentView.appointment
                ],
                element: CalendarElement.appointment);
          } else {
            _raiseCalendarLongPressCallback(widget._calendar,
                date: _appointmentView.appointment.startTime,
                appointments: <dynamic>[
                  _appointmentView.appointment._data ??
                      _appointmentView.appointment
                ],
                element: CalendarElement.appointment);
          }
        }
      }
    }
  }

  //// Handles the onLongPress callback for timeline view cells, and view header
  //// of timeline.
  void _handleOnLongPressForTimeline(LongPressStartDetails details) {
    _handleTouchOnTimeline(null, details);
  }

  void _updateAllDaySelection(_AppointmentView view, DateTime date) {
    if (_allDaySelectionNotifier != null &&
        _allDaySelectionNotifier.value != null &&
        view == _allDaySelectionNotifier.value._appointmentView &&
        isSameDate(date, _allDaySelectionNotifier.value._selectedDate)) {
      return;
    }

    _allDaySelectionNotifier?.value = _SelectionDetails(view, date);
  }

  //// Handles the onTap callback for day view cells, all day panel, and view
  //// header of day.
  void _handleOnTapForDay(TapUpDetails details) {
    _handleTouchOnDayView(details, null);
  }

  /// Handles the tap and long press related functions for day, week
  /// work week views.
  void _handleTouchOnDayView(
      TapUpDetails tapDetails, LongPressStartDetails longPressDetails) {
    double _xDetails, _yDetails;
    bool _isTappedCallback = false;
    if (tapDetails != null) {
      _isTappedCallback = true;
      _xDetails = tapDetails.localPosition.dx;
      _yDetails = tapDetails.localPosition.dy;
    } else if (longPressDetails != null) {
      _xDetails = longPressDetails.localPosition.dx;
      _yDetails = longPressDetails.localPosition.dy;
    }
    if (!widget._focusNode.hasFocus) {
      widget._focusNode.requestFocus();
    }
    _updateCalendarStateDetails._selectedDate = null;
    _updateCalendarStateDetails._currentViewVisibleDates = null;
    _updateCalendarStateDetails._allDayAppointmentViewCollection = null;
    widget.updateCalendarState(_updateCalendarStateDetails);
    dynamic _selectedAppointment;
    List<dynamic> _selectedAppointments;
    CalendarElement _targetElement;
    DateTime _selectedDate = _updateCalendarStateDetails._selectedDate;
    final double _timeLabelWidth = _getTimeLabelWidth(
        widget._calendar.timeSlotViewSettings.timeRulerSize,
        widget._calendar.view);

    final double _viewHeaderHeight = widget._calendar.view == CalendarView.day
        ? 0
        : _getViewHeaderHeight(
            widget._calendar.viewHeaderHeight, widget._calendar.view);
    final double allDayHeight = isExpanded
        ? _updateCalendarStateDetails._allDayPanelHeight
        : _allDayHeight;
    if (!_isRTL &&
        _xDetails <= _timeLabelWidth &&
        _yDetails > _viewHeaderHeight + allDayHeight) {
      return;
    }

    if (_isRTL &&
        _xDetails >= widget._width - _timeLabelWidth &&
        _yDetails > _viewHeaderHeight + allDayHeight) {
      return;
    }

    if (_yDetails < _viewHeaderHeight) {
      /// Check the touch position in time ruler view
      /// If RTL, time ruler placed at right side,
      /// else time ruler placed at left side.
      if ((!_isRTL && _xDetails <= _timeLabelWidth) ||
          (_isRTL && widget._width - _xDetails <= _timeLabelWidth)) {
        return;
      }

      if (_isTappedCallback &&
          _shouldRaiseCalendarTapCallback(widget._calendar.onTap)) {
        _handleOnTapForViewHeader(tapDetails, widget._width);
      } else if (!_isTappedCallback &&
          _shouldRaiseCalendarLongPressCallback(widget._calendar.onLongPress)) {
        _handleOnLongPressForViewHeader(longPressDetails, widget._width);
      }

      return;
    } else if (_yDetails < _viewHeaderHeight + allDayHeight) {
      /// Check the touch position in view header when [CalendarView] is day
      /// If RTL, view header placed at right side,
      /// else view header placed at left side.
      if (widget._calendar.view == CalendarView.day &&
          ((!_isRTL && _xDetails <= _timeLabelWidth) ||
              (_isRTL && widget._width - _xDetails <= _timeLabelWidth)) &&
          _yDetails <
              _getViewHeaderHeight(
                  widget._calendar.viewHeaderHeight, widget._calendar.view)) {
        if (_isTappedCallback &&
            _shouldRaiseCalendarTapCallback(widget._calendar.onTap)) {
          _handleOnTapForViewHeader(tapDetails, widget._width);
        } else if (!_isTappedCallback &&
            _shouldRaiseCalendarLongPressCallback(
                widget._calendar.onLongPress)) {
          _handleOnLongPressForViewHeader(longPressDetails, widget._width);
        }

        return;
      } else if ((!_isRTL && _timeLabelWidth >= _xDetails) ||
          (_isRTL && _xDetails > widget._width - _timeLabelWidth)) {
        /// Perform expand or collapse when the touch position on
        /// expander icon in all day panel.
        _expandOrCollapseAllDay();
        return;
      }

      final double yPosition = _yDetails - _viewHeaderHeight;
      final _AppointmentView _appointmentView = _getAppointmentOnPoint(
          _updateCalendarStateDetails._allDayAppointmentViewCollection,
          _xDetails,
          yPosition);
      if (_appointmentView == null &&
          (yPosition < allDayHeight - _kAllDayAppointmentHeight ||
              _updateCalendarStateDetails._allDayPanelHeight <= allDayHeight)) {
        _targetElement = CalendarElement.allDayPanel;
        if (_isTappedCallback) {
          _selectedDate = _getTappedViewHeaderDate(tapDetails, widget._width);
        } else {
          _selectedDate =
              _getTappedViewHeaderDate(longPressDetails, widget._width);
        }
      }

      if (_appointmentView != null &&
          (yPosition < allDayHeight - _kAllDayAppointmentHeight ||
              _updateCalendarStateDetails._allDayPanelHeight <= allDayHeight)) {
        if (!isDateWithInDateRange(
                widget._calendar.minDate,
                widget._calendar.maxDate,
                _appointmentView.appointment._actualStartTime) ||
            !isDateWithInDateRange(
                widget._calendar.minDate,
                widget._calendar.maxDate,
                _appointmentView.appointment._actualEndTime)) {
          return;
        }
        if (_selectedDate != null) {
          _selectedDate = null;
          _selectionPainter._selectedDate = _selectedDate;
          _updateCalendarStateDetails._selectedDate = _selectedDate;
          _updateCalendarStateDetails._isAppointmentTapped = true;
        }

        _selectionPainter._appointmentView = null;
        _selectionPainter._repaintNotifier.value =
            !_selectionPainter._repaintNotifier.value;
        _selectedAppointment = _appointmentView.appointment;
        _selectedAppointments = null;
        _targetElement = CalendarElement.appointment;
        _updateAllDaySelection(_appointmentView, null);
      } else if (_updateCalendarStateDetails._allDayPanelHeight >
              allDayHeight &&
          yPosition > allDayHeight - _kAllDayAppointmentHeight) {
        _expandOrCollapseAllDay();
        return;
      } else if (_appointmentView == null) {
        _updateAllDaySelection(null, _selectedDate);
        _selectionPainter._selectedDate = null;
        _selectionPainter._appointmentView = null;
        _selectionPainter._repaintNotifier.value =
            !_selectionPainter._repaintNotifier.value;
        _updateCalendarStateDetails._selectedDate = null;
        _updateCalendarStateDetails._isAppointmentTapped = true;
      }
    } else {
      final double yPosition = _yDetails -
          _viewHeaderHeight -
          allDayHeight +
          _scrollController.offset;
      final _AppointmentView _appointmentView = _getAppointmentOnPoint(
          _appointmentPainter._appointmentCollection, _xDetails, yPosition);
      _allDaySelectionNotifier?.value = null;
      if (_appointmentView == null) {
        if (_isRTL) {
          _drawSelection(_xDetails,
              _yDetails - _viewHeaderHeight - allDayHeight, _timeLabelWidth);
        } else {
          _drawSelection(_xDetails - _timeLabelWidth,
              _yDetails - _viewHeaderHeight - allDayHeight, _timeLabelWidth);
        }
        _targetElement = CalendarElement.calendarCell;
      } else {
        if (_selectedDate != null) {
          _selectedDate = null;
          _selectionPainter._selectedDate = _selectedDate;
          _updateCalendarStateDetails._selectedDate = _selectedDate;
          _updateCalendarStateDetails._isAppointmentTapped = true;
        }

        _selectionPainter._appointmentView = _appointmentView;
        _selectionPainter._repaintNotifier.value =
            !_selectionPainter._repaintNotifier.value;
        _selectedAppointment = _appointmentView.appointment;
        _targetElement = CalendarElement.appointment;
      }
    }

    _updateCalendarStateDetails._appointments = null;
    widget.updateCalendarState(_updateCalendarStateDetails);
    if ((!_isTappedCallback &&
            _shouldRaiseCalendarLongPressCallback(
                widget._calendar.onLongPress)) ||
        (_isTappedCallback &&
            _shouldRaiseCalendarTapCallback(widget._calendar.onTap))) {
      if (_selectionPainter._selectedDate != null &&
          _targetElement != CalendarElement.allDayPanel) {
        _selectedAppointments = null;

        final double _yPosition = _yDetails - _viewHeaderHeight - allDayHeight;

        /// In LTR, remove the time ruler width value from the
        /// touch x position while calculate the selected date value.
        _selectedDate = _getDateFromPosition(
            !_isRTL ? _xDetails - _timeLabelWidth : _xDetails,
            _yPosition,
            _timeLabelWidth);

        if (!isDateWithInDateRange(widget._calendar.minDate,
            widget._calendar.maxDate, _selectedDate)) {
          return;
        }

        /// Restrict the callback, while selected region as disabled [TimeRegion].
        if (_targetElement == CalendarElement.calendarCell &&
            !_isEnabledRegion(_yPosition, _selectedDate)) {
          return;
        }

        if (_isTappedCallback) {
          _raiseCalendarTapCallback(widget._calendar,
              date: _selectionPainter._selectedDate,
              appointments: _selectedAppointments,
              element: _targetElement);
        } else {
          _raiseCalendarLongPressCallback(widget._calendar,
              date: _selectionPainter._selectedDate,
              appointments: _selectedAppointments,
              element: _targetElement);
        }
      } else if (_selectedAppointment != null) {
        _selectedAppointments = <dynamic>[
          _selectedAppointment._data ?? _selectedAppointment
        ];
        if (_isTappedCallback) {
          _raiseCalendarTapCallback(widget._calendar,
              date: _selectedAppointment.startTime,
              appointments: _selectedAppointments,
              element: CalendarElement.appointment);
        } else {
          _raiseCalendarLongPressCallback(widget._calendar,
              date: _selectedAppointment.startTime,
              appointments: _selectedAppointments,
              element: CalendarElement.appointment);
        }
      } else if (_selectedDate != null &&
          _targetElement == CalendarElement.allDayPanel) {
        if (_isTappedCallback) {
          _raiseCalendarTapCallback(widget._calendar,
              date: _selectedDate, appointments: null, element: _targetElement);
        } else {
          _raiseCalendarLongPressCallback(widget._calendar,
              date: _selectedDate, appointments: null, element: _targetElement);
        }
      }
    }
  }

  /// Check the selected date region as enabled time region or not.
  bool _isEnabledRegion(double y, DateTime selectedDate) {
    if (widget._regions == null ||
        widget._regions.isEmpty ||
        selectedDate == null) {
      return true;
    }

    final double minuteHeight =
        widget._calendar.timeSlotViewSettings.timeIntervalHeight /
            _getTimeInterval(widget._calendar.timeSlotViewSettings);
    final Duration startDuration = Duration(
        hours: widget._calendar.timeSlotViewSettings.startHour.toInt(),
        minutes: ((widget._calendar.timeSlotViewSettings.startHour -
                    widget._calendar.timeSlotViewSettings.startHour.toInt()) *
                60)
            .toInt());
    int minutes;
    if (_isTimelineView(widget._calendar.view)) {
      final double viewWidth = _timeIntervalHeight * _horizontalLinesCount;
      if (_isRTL) {
        minutes = ((_scrollController.offset +
                    (_scrollController.position.viewportDimension - y)) %
                viewWidth) ~/
            minuteHeight;
      } else {
        minutes = ((_scrollController.offset + y) % viewWidth) ~/ minuteHeight;
      }
    } else {
      minutes = (_scrollController.offset + y) ~/ minuteHeight;
    }

    final DateTime date = DateTime(selectedDate.year, selectedDate.month,
        selectedDate.day, 0, minutes + startDuration.inMinutes, 0);
    for (int i = 0; i < widget._regions.length; i++) {
      final TimeRegion region = widget._regions[i];
      if (region.enablePointerInteraction ||
          region._actualStartTime.isAfter(date) ||
          region._actualEndTime.isBefore(date)) {
        continue;
      }

      return false;
    }

    return true;
  }

  //// Handles the onLongPress callback for day view cells, all day panel and
  //// view header  of day.
  void _handleOnLongPressForDay(LongPressStartDetails details) {
    _handleTouchOnDayView(null, details);
  }

  //// Handles the on tap callback for view header
  void _handleOnTapForViewHeader(TapUpDetails details, double _width) {
    _raiseCalendarTapCallback(widget._calendar,
        date: _getTappedViewHeaderDate(details, _width),
        element: CalendarElement.viewHeader);
  }

  //// Handles the on long press callback for view header
  void _handleOnLongPressForViewHeader(
      LongPressStartDetails details, double _width) {
    _raiseCalendarLongPressCallback(widget._calendar,
        date: _getTappedViewHeaderDate(details, _width),
        element: CalendarElement.viewHeader);
  }

  DateTime _getTappedViewHeaderDate(dynamic details, double _width) {
    int index = 0;
    final double _timeLabelViewWidth = _getTimeLabelWidth(
        widget._calendar.timeSlotViewSettings.timeRulerSize,
        widget._calendar.view);
    if (!_isTimelineView(widget._calendar.view)) {
      double cellWidth = 0;
      if (widget._calendar.view != CalendarView.month) {
        cellWidth =
            (_width - _timeLabelViewWidth) / widget._visibleDates.length;

        /// Set index value as 0 when calendar view as day because day view hold
        /// single visible date.
        if (widget._calendar.view == CalendarView.day) {
          index = 0;
        } else {
          index =
              ((details.localPosition.dx - (_isRTL ? 0 : _timeLabelViewWidth)) /
                      cellWidth)
                  .truncate();
        }
      } else {
        /// 7 represents the number of days in week.
        cellWidth = _width / 7;
        index = (details.localPosition.dx / cellWidth).truncate();
      }

      /// Calculate the RTL based value of index when the widget direction as RTL.
      if (_isRTL && widget._calendar.view != CalendarView.month) {
        index = widget._visibleDates.length - index - 1;
      } else if (_isRTL && widget._calendar.view == CalendarView.month) {
        /// 7 represents the number of days in week.
        index = 7 - index - 1;
      }

      if (index < 0 || index >= widget._visibleDates.length) {
        return null;
      }

      return widget._visibleDates[index];
    } else {
      index = ((_scrollController.offset +
                  (_isRTL
                      ? _scrollController.position.viewportDimension -
                          details.localPosition.dx
                      : details.localPosition.dx)) /
              _getSingleViewWidthForTimeLineView(this))
          .truncate();

      if (index < 0 || index >= widget._visibleDates.length) {
        return null;
      }

      return widget._visibleDates[index];
    }
  }

  void _updateHoveringForAppointment(double _xPosition, double _yPosition) {
    if (_viewHeaderNotifier.value != null) {
      _viewHeaderNotifier.value = null;
    }

    if (_calendarCellNotifier.value != null) {
      _calendarCellNotifier.value = null;
    }

    if (_allDayNotifier.value != null) {
      _allDayNotifier.value = null;
    }

    _appointmentHoverNotifier.value = Offset(_xPosition, _yPosition);
  }

  void _updateHoveringForAllDayPanel(double _xPosition, double _yPosition) {
    if (_viewHeaderNotifier.value != null) {
      _viewHeaderNotifier.value = null;
    }

    if (_calendarCellNotifier.value != null) {
      _calendarCellNotifier.value = null;
    }

    if (_appointmentHoverNotifier.value != null) {
      _appointmentHoverNotifier.value = null;
    }

    _allDayNotifier.value = Offset(_xPosition, _yPosition);
  }

  void _updateHoveringForViewHeader(Offset localPosition, double _xPosition,
      double _yPosition, double _viewHeaderHeight) {
    if (_yPosition < 0) {
      if (_hoveringDate != null) {
        _hoveringDate = null;
      }

      if (_viewHeaderNotifier.value != null) {
        _viewHeaderNotifier.value = null;
      }

      if (_calendarCellNotifier.value != null) {
        _calendarCellNotifier.value = null;
      }

      if (_allDayNotifier.value != null) {
        _allDayNotifier.value = null;
      }

      if (_appointmentHoverNotifier.value != null) {
        _appointmentHoverNotifier.value = null;
      }
    }

    final DateTime _hoverDate = _getTappedViewHeaderDate(
        TapUpDetails(
            globalPosition: localPosition,
            localPosition: Offset(
                _isTimelineView(widget._calendar.view)
                    ? localPosition.dx
                    : _xPosition,
                _yPosition)),
        widget._width);

    // Remove the hovering when the position not in cell regions.
    if (_hoverDate == null) {
      if (_hoveringDate != null) {
        _hoveringDate = null;
      }

      if (_viewHeaderNotifier.value != null) {
        _viewHeaderNotifier.value = null;
      }

      return;
    }

    _hoveringDate = _hoverDate;

    if (_calendarCellNotifier.value != null) {
      _calendarCellNotifier.value = null;
    }

    if (_allDayNotifier.value != null) {
      _allDayNotifier.value = null;
    }

    if (_appointmentHoverNotifier.value != null) {
      _appointmentHoverNotifier.value = null;
    }

    _viewHeaderNotifier.value = Offset(_xPosition, _yPosition);
  }

  void _updatePointerHover(dynamic event) {
    final RenderBox box = context.findRenderObject();
    final Offset localPosition = box.globalToLocal(event.position);
    double _viewHeaderHeight = _getViewHeaderHeight(
        widget._calendar.viewHeaderHeight, widget._calendar.view);
    final double _timeLabelWidth = _getTimeLabelWidth(
        widget._calendar.timeSlotViewSettings.timeRulerSize,
        widget._calendar.view);
    double allDayHeight = isExpanded
        ? _updateCalendarStateDetails._allDayPanelHeight
        : _allDayHeight;

    /// All day panel and view header are arranged horizontally,
    /// so get the maximum value from all day height and view header height and
    /// use the value instead of adding of view header height and all day height.
    if (widget._calendar.view == CalendarView.day) {
      if (allDayHeight > _viewHeaderHeight) {
        _viewHeaderHeight = allDayHeight;
      }

      allDayHeight = 0;
    }

    double _xPosition;
    double _yPosition;
    final bool _isTimelineViews = _isTimelineView(widget._calendar.view);
    if (widget._calendar.view != CalendarView.month && !_isTimelineViews) {
      /// In LTR, remove the time ruler width value from the
      /// touch x position while calculate the selected date from position.
      _xPosition =
          _isRTL ? localPosition.dx : localPosition.dx - _timeLabelWidth;

      if (localPosition.dy < _viewHeaderHeight) {
        if (widget._calendar.view == CalendarView.day) {
          _xPosition =
              _isRTL ? widget._width - localPosition.dx : localPosition.dx;
          if ((_isRTL && _xPosition < widget._width - _timeLabelWidth) ||
              (!_isRTL && _xPosition > _timeLabelWidth)) {
            _updateHoveringForAllDayPanel(_xPosition, localPosition.dy);
            return;
          }
          _updateHoveringForViewHeader(
              localPosition, _xPosition, localPosition.dy, _viewHeaderHeight);
          return;
        }

        _updateHoveringForViewHeader(localPosition, localPosition.dx,
            localPosition.dy, _viewHeaderHeight);
        return;
      }

      double _panelHeight =
          _updateCalendarStateDetails._allDayPanelHeight - _allDayHeight;
      if (_panelHeight < 0) {
        _panelHeight = 0;
      }

      final double _alldayExpanderHeight =
          _panelHeight * _allDayExpanderAnimation.value;
      final double _allDayBottom = (widget._calendar.view == CalendarView.day)
          ? _viewHeaderHeight + _alldayExpanderHeight
          : _viewHeaderHeight + _allDayHeight + _alldayExpanderHeight;
      if (localPosition.dy > _viewHeaderHeight &&
          localPosition.dy < _allDayBottom &&
          ((_isRTL && _xPosition < widget._width - _timeLabelWidth) ||
              (!_isRTL && _xPosition > _timeLabelWidth))) {
        _updateHoveringForAllDayPanel(
            _xPosition, localPosition.dy - _viewHeaderHeight);
        return;
      }

      _yPosition = localPosition.dy - (_viewHeaderHeight + allDayHeight);

      final _AppointmentView appointment = _getAppointmentOnPoint(
          _appointmentPainter._appointmentCollection,
          localPosition.dx,
          _yPosition + _scrollController.offset);
      if (appointment != null) {
        _updateHoveringForAppointment(
            localPosition.dx, _yPosition + _scrollController.offset);
        return;
      }
    } else {
      _xPosition = localPosition.dx;

      /// Update the x position value with scroller offset and the value
      /// assigned to mouse hover position.
      /// mouse hover position value used for highlight the position
      /// on all the calendar views.
      if (_isTimelineViews) {
        if (_isRTL) {
          _xPosition = (_getSingleViewWidthForTimeLineView(this) *
                  widget._visibleDates.length) -
              (_scrollController.offset +
                  (_scrollController.position.viewportDimension -
                      localPosition.dx));
        } else {
          _xPosition = localPosition.dx + _scrollController.offset;
        }
      }

      if (localPosition.dy < _viewHeaderHeight) {
        _updateHoveringForViewHeader(
            localPosition, _xPosition, localPosition.dy, _viewHeaderHeight);
        return;
      }

      _yPosition = localPosition.dy - _viewHeaderHeight;

      if (widget._calendar.view != CalendarView.month) {
        final _AppointmentView appointment = _getAppointmentOnPoint(
            _appointmentPainter._appointmentCollection, _xPosition, _yPosition);
        if (appointment != null) {
          _updateHoveringForAppointment(_xPosition, _yPosition);
          return;
        }
      }
    }

    /// Remove the hovering when the position not in cell regions.
    if (_yPosition < 0) {
      if (_hoveringDate != null) {
        _hoveringDate = null;
      }

      if (_calendarCellNotifier.value != null) {
        _calendarCellNotifier.value = null;
      }

      return;
    }

    final DateTime _hoverDate = _getDateFromPosition(
        _isTimelineViews ? localPosition.dx : _xPosition,
        _yPosition,
        _timeLabelWidth);

    /// Remove the hovering when the position not in cell regions or non active cell regions.
    if (_hoverDate == null ||
        !isDateWithInDateRange(
            widget._calendar.minDate, widget._calendar.maxDate, _hoverDate)) {
      if (_hoveringDate != null) {
        _hoveringDate = null;
      }

      if (_calendarCellNotifier.value != null) {
        _calendarCellNotifier.value = null;
      }

      return;
    }

    /// Restrict the hovering, while selected region as disabled [TimeRegion].
    if (((widget._calendar.view == CalendarView.day ||
                widget._calendar.view == CalendarView.week ||
                widget._calendar.view == CalendarView.workWeek) &&
            !_isEnabledRegion(_yPosition, _hoverDate)) ||
        (_isTimelineViews && !_isEnabledRegion(localPosition.dx, _hoverDate))) {
      if (_hoveringDate != null) {
        _hoveringDate = null;
      }

      if (_calendarCellNotifier.value != null) {
        _calendarCellNotifier.value = null;
      }
      return;
    }

    if ((widget._calendar.view == CalendarView.month &&
            isSameDate(_hoveringDate, _hoverDate) &&
            _viewHeaderNotifier.value == null) ||
        (widget._calendar.view != CalendarView.month &&
            _isSameTimeSlot(_hoveringDate, _hoverDate) &&
            _viewHeaderNotifier.value == null)) {
      return;
    }

    _hoveringDate = _hoverDate;

    if (widget._calendar.view == CalendarView.month &&
        isSameDate(_selectionPainter._selectedDate, _hoveringDate)) {
      _calendarCellNotifier.value = null;
      return;
    } else if (widget._calendar.view != CalendarView.month &&
        _isSameTimeSlot(_selectionPainter._selectedDate, _hoveringDate)) {
      _calendarCellNotifier.value = null;
      return;
    }

    if (widget._calendar.view != CalendarView.month && !_isTimelineViews) {
      _yPosition += _scrollController.offset;
    }

    if (_viewHeaderNotifier.value != null) {
      _viewHeaderNotifier.value = null;
    }

    if (_allDayNotifier.value != null) {
      _allDayNotifier.value = null;
    }

    if (_appointmentHoverNotifier.value != null) {
      _appointmentHoverNotifier.value = null;
    }

    _calendarCellNotifier.value = Offset(_xPosition, _yPosition);
  }

  dynamic _pointerEnterEvent(PointerEnterEvent event) {
    _updatePointerHover(event);
  }

  dynamic _pointerHoverEvent(PointerHoverEvent event) {
    _updatePointerHover(event);
  }

  dynamic _pointerExitEvent(PointerExitEvent event) {
    _hoveringDate = null;
    _calendarCellNotifier.value = null;
    _viewHeaderNotifier.value = null;
    _appointmentHoverNotifier.value = null;
    _allDayNotifier.value = null;
  }

  @override
  Widget build(BuildContext context) {
    _isRTL = _isRTLLayout(context);
    _updateCalendarStateDetails._currentViewVisibleDates = null;
    _updateCalendarStateDetails._allDayPanelHeight = null;
    _updateCalendarStateDetails._selectedDate = null;
    _updateCalendarStateDetails._currentDate = null;
    widget.updateCalendarState(_updateCalendarStateDetails);
    if (widget._calendar.view == CalendarView.month) {
      return GestureDetector(
        child: MouseRegion(
          onEnter: _pointerEnterEvent,
          onExit: _pointerExitEvent,
          onHover: _pointerHoverEvent,
          child: _addMonthView(_isRTL, widget._locale),
        ),
        onTapUp: (TapUpDetails details) {
          _handleOnTapForMonth(details);
        },
        onLongPressStart: (LongPressStartDetails details) {
          _handleOnLongPressForMonth(details);
        },
      );
    } else if (!_isTimelineView(widget._calendar.view) &&
        widget._calendar.view != CalendarView.month) {
      _allDayHeight = 0;

      if (widget._calendar.view == CalendarView.day) {
        final double viewHeaderHeight = _getViewHeaderHeight(
            widget._calendar.viewHeaderHeight, widget._calendar.view);
        if (widget._visibleDates ==
            _updateCalendarStateDetails._currentViewVisibleDates) {
          _allDayHeight = _kAllDayLayoutHeight > viewHeaderHeight &&
                  _updateCalendarStateDetails._allDayPanelHeight >
                      viewHeaderHeight
              ? _updateCalendarStateDetails._allDayPanelHeight >
                      _kAllDayLayoutHeight
                  ? _kAllDayLayoutHeight
                  : _updateCalendarStateDetails._allDayPanelHeight
              : viewHeaderHeight;
          if (_allDayHeight < _updateCalendarStateDetails._allDayPanelHeight) {
            _allDayHeight += _kAllDayAppointmentHeight;
          }
        } else {
          _allDayHeight = viewHeaderHeight;
        }
      }

      if (widget._calendar.view != CalendarView.day &&
          widget._visibleDates ==
              _updateCalendarStateDetails._currentViewVisibleDates) {
        _allDayHeight = _updateCalendarStateDetails._allDayPanelHeight >
                _kAllDayLayoutHeight
            ? _kAllDayLayoutHeight
            : _updateCalendarStateDetails._allDayPanelHeight;
        _allDayHeight = _allDayHeight * _heightAnimation.value;
      }

      return GestureDetector(
        child: MouseRegion(
          onEnter: _pointerEnterEvent,
          onHover: _pointerHoverEvent,
          onExit: _pointerExitEvent,
          child: _addDayView(
              widget._width,
              _timeIntervalHeight * _horizontalLinesCount,
              _isRTL,
              widget._locale),
        ),
        onTapUp: (TapUpDetails details) {
          _handleOnTapForDay(details);
        },
        onLongPressStart: (LongPressStartDetails details) {
          _handleOnLongPressForDay(details);
        },
      );
    } else {
      if ((_scrollController.hasClients &&
              !_scrollController.position.atEdge) &&
          _scrollPhysics.toString() == 'NeverScrollableScrollPhysics' &&
          _updateCalendarStateDetails._currentViewVisibleDates ==
              widget._visibleDates) {
        _scrollPhysics = const ClampingScrollPhysics();
      }

      return GestureDetector(
        child: MouseRegion(
          onEnter: _pointerEnterEvent,
          onHover: _pointerHoverEvent,
          onExit: _pointerExitEvent,
          child: _addTimelineView(
              _timeIntervalHeight *
                  (_horizontalLinesCount * widget._visibleDates.length),
              widget._height,
              widget._locale),
        ),
        onTapUp: (TapUpDetails details) {
          _handleOnTapForTimeline(details);
        },
        onLongPressStart: (LongPressStartDetails details) {
          _handleOnLongPressForTimeline(details);
        },
      );
    }
  }

  _AppointmentView _getAppointmentOnPoint(
      List<_AppointmentView> _appointmentCollection, double x, double y) {
    if (_appointmentCollection == null) {
      return null;
    }

    _AppointmentView _appointmentView;
    for (int i = 0; i < _appointmentCollection.length; i++) {
      _appointmentView = _appointmentCollection[i];
      if (_appointmentView.appointment != null &&
          _appointmentView.appointmentRect != null &&
          _appointmentView.appointmentRect.left <= x &&
          _appointmentView.appointmentRect.right >= x &&
          _appointmentView.appointmentRect.top <= y &&
          _appointmentView.appointmentRect.bottom >= y) {
        return _appointmentView;
      }
    }

    return null;
  }

  List<dynamic> _getSelectedAppointments() {
    return (widget._calendar.dataSource != null &&
            !_isCalendarAppointment(widget._calendar.dataSource.appointments))
        ? _getCustomAppointments(_getSelectedDateAppointments(
            _updateCalendarStateDetails._appointments,
            widget._calendar.timeZone,
            _selectionPainter._selectedDate))
        : (_getSelectedDateAppointments(
            _updateCalendarStateDetails._appointments,
            widget._calendar.timeZone,
            _selectionPainter._selectedDate));
  }

  DateTime _getDateFromPosition(double x, double y, double _timeLabelWidth) {
    int rowIndex, columnIndex;
    double cellWidth = 0;
    double cellHeight = 0;
    final double _width = widget._width - _timeLabelWidth;
    int index = 0;
    if (widget._calendar.view == CalendarView.month) {
      if (x > _width || x < 0) {
        return null;
      }

      cellWidth = _width / numberOfDaysInWeek;
      cellHeight = (widget._height -
              _getViewHeaderHeight(
                  widget._calendar.viewHeaderHeight, widget._calendar.view)) /
          widget._calendar.monthViewSettings.numberOfWeeksInView;
      rowIndex = (x / cellWidth).truncate();
      columnIndex = (y / cellHeight).truncate();
      if (_isRTL) {
        index = (columnIndex * numberOfDaysInWeek) +
            (numberOfDaysInWeek - rowIndex) -
            1;
      } else {
        index = (columnIndex * numberOfDaysInWeek) + rowIndex;
      }

      if (index < 0 || index >= widget._visibleDates.length) {
        return null;
      }

      return widget._visibleDates[index];
    } else if (widget._calendar.view == CalendarView.day) {
      if (y >= _timeIntervalHeight * _horizontalLinesCount ||
          x > _width ||
          x < 0) {
        return null;
      }
      cellWidth = _width;
      cellHeight = _timeIntervalHeight;
      columnIndex = ((_scrollController.offset + y) / cellHeight).truncate();
      final dynamic time =
          ((_getTimeInterval(widget._calendar.timeSlotViewSettings) / 60) *
                  columnIndex) +
              widget._calendar.timeSlotViewSettings.startHour;
      final int hour = time.toInt();
      final dynamic minute = ((time - hour) * 60).round();
      return DateTime(
          widget._visibleDates[0].year,
          widget._visibleDates[0].month,
          widget._visibleDates[0].day,
          hour,
          minute);
    } else if (widget._calendar.view == CalendarView.workWeek ||
        widget._calendar.view == CalendarView.week) {
      if (y >= _timeIntervalHeight * _horizontalLinesCount ||
          x > _width ||
          x < 0) {
        return null;
      }
      cellWidth = _width / widget._visibleDates.length;
      cellHeight = _timeIntervalHeight;
      columnIndex = ((_scrollController.offset + y) / cellHeight).truncate();
      final dynamic time =
          ((_getTimeInterval(widget._calendar.timeSlotViewSettings) / 60) *
                  columnIndex) +
              widget._calendar.timeSlotViewSettings.startHour;
      final int hour = time.toInt();
      final dynamic minute = ((time - hour) * 60).round();
      rowIndex = (x / cellWidth).truncate();
      if (_isRTL) {
        rowIndex = (widget._visibleDates.length - rowIndex) - 1;
      }

      if (rowIndex < 0 || rowIndex >= widget._visibleDates.length) {
        return null;
      }

      final DateTime date = widget._visibleDates[rowIndex];

      return DateTime(date.year, date.month, date.day, hour, minute);
    } else {
      final double _viewWidth = _timeIntervalHeight *
          (_horizontalLinesCount * widget._visibleDates.length);
      if ((!_isRTL && x >= _viewWidth) ||
          (_isRTL && x < (widget._width - _viewWidth))) {
        return null;
      }
      cellWidth = _timeIntervalHeight;
      cellHeight = widget._height;
      if (_isRTL) {
        rowIndex = (((_scrollController.offset %
                        _getSingleViewWidthForTimeLineView(this)) +
                    (_scrollController.position.viewportDimension - x)) /
                cellWidth)
            .truncate();
      } else {
        rowIndex = (((_scrollController.offset %
                        _getSingleViewWidthForTimeLineView(this)) +
                    x) /
                cellWidth)
            .truncate();
      }
      columnIndex =
          (_scrollController.offset / _getSingleViewWidthForTimeLineView(this))
              .truncate();
      if (rowIndex >= _horizontalLinesCount) {
        columnIndex += rowIndex ~/ _horizontalLinesCount;
        rowIndex = (rowIndex % _horizontalLinesCount).toInt();
      }
      final dynamic time =
          ((_getTimeInterval(widget._calendar.timeSlotViewSettings) / 60) *
                  rowIndex) +
              widget._calendar.timeSlotViewSettings.startHour;
      final int hour = time.toInt();
      final dynamic minute = ((time - hour) * 60).round();
      if (columnIndex < 0) {
        columnIndex = 0;
      } else if (columnIndex > widget._visibleDates.length) {
        columnIndex = widget._visibleDates.length - 1;
      }

      if (columnIndex < 0 || columnIndex >= widget._visibleDates.length) {
        return null;
      }

      final DateTime date = widget._visibleDates[columnIndex];

      return DateTime(date.year, date.month, date.day, hour, minute);
    }
  }

  void _drawSelection(double x, double y, double _timeLabelWidth) {
    final DateTime _selectedDate = _getDateFromPosition(x, y, _timeLabelWidth);
    if (_selectedDate == null ||
        !isDateWithInDateRange(widget._calendar.minDate,
            widget._calendar.maxDate, _selectedDate)) {
      return;
    }

    /// Restrict the selection update, while selected region as disabled [TimeRegion].
    if (((widget._calendar.view == CalendarView.day ||
                widget._calendar.view == CalendarView.week ||
                widget._calendar.view == CalendarView.workWeek) &&
            !_isEnabledRegion(y, _selectedDate)) ||
        (_isTimelineView(widget._calendar.view) &&
            !_isEnabledRegion(x, _selectedDate))) {
      return;
    }

    if (widget._calendar.view == CalendarView.month) {
      widget._agendaSelectedDate.value = _selectedDate;
    }

    _updateCalendarStateDetails._selectedDate = _selectedDate;
    _updateCalendarStateDetails._isAppointmentTapped = false;

    _selectionPainter._selectedDate = _selectedDate;
    _selectionPainter._appointmentView = null;
    _selectionPainter._repaintNotifier.value =
        !_selectionPainter._repaintNotifier.value;
  }

  _SelectionPainter _addSelectionView() {
    _updateCalendarStateDetails._selectedDate = null;
    _updateCalendarStateDetails._currentViewVisibleDates = null;
    widget.updateCalendarState(_updateCalendarStateDetails);
    _AppointmentView _appointmentView;
    if (_selectionPainter != null &&
        _selectionPainter._appointmentView != null) {
      _appointmentView = _selectionPainter._appointmentView;
    }
    _selectionPainter = _SelectionPainter(
        widget._calendar,
        widget._visibleDates,
        _updateCalendarStateDetails._selectedDate,
        widget._calendar.selectionDecoration,
        _timeIntervalHeight,
        widget._calendarTheme,
        ValueNotifier<bool>(false),
        _isRTL, updateCalendarState: (_UpdateCalendarStateDetails details) {
      _updatePainterProperties(details);
    });

    if (_appointmentView != null) {
      _selectionPainter._appointmentView = _appointmentView;
    }

    return _selectionPainter;
  }

  Widget _getTimelineViewHeader(double width, double height, String locale) {
    _timelineViewHeader = _TimelineViewHeaderView(
        widget._visibleDates,
        this,
        ValueNotifier<bool>(false),
        widget._calendar.viewHeaderStyle,
        widget._calendar.timeSlotViewSettings,
        _getViewHeaderHeight(
            widget._calendar.viewHeaderHeight, widget._calendar.view),
        _isRTL,
        widget._calendar.todayHighlightColor ??
            widget._calendarTheme.todayHighlightColor,
        widget._locale,
        widget._calendarTheme,
        widget._calendar.minDate,
        widget._calendar.maxDate,
        _viewHeaderNotifier);
    return ListView(
        padding: const EdgeInsets.all(0.0),
        controller: _timelineViewHeaderScrollController,
        scrollDirection: Axis.horizontal,
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
          CustomPaint(
            painter: _timelineViewHeader,
            size: Size(width, height),
          )
        ]);
  }
}
